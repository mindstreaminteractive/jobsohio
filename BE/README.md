# jobsohio - Backend

## Setup for development

1. Find all 'jobsohio' and replace with the 'project name'
1. Copy the `.env-sample` as `.env`.
1. Run `make start` to start the Docker containers.
1. Run `make shell`
1. Run `composer install` to install WordPress and its dependencies. Remember to do this from inside the container.

At this point you can open the browser by doing `make open`.

NOTE: If you get the message "Error establishing a database connection" just give it a few seconds so the database gets totally imported.

**Admin user:**  
User: midev  
Pass: 123

