<?php
define('WP_CACHE', true); // Added by WP Rocket
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jobsohio_beta');

/** MySQL database username */
define('DB_USER', 'jobsohio_wpadmin');

/** MySQL database password */
define('DB_PASSWORD', 'JoBsOH2018#!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DISALLOW_FILE_EDIT', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UcL _os-~Pz8+@]Pjl,FnwawIQt|01<tEJZ(.FutN#tEha=0A#9!%qs,kA268K%T');
define('SECURE_AUTH_KEY',  '1*t%;xQqx:~,w+gnk6.F<vFgV!HxL2Z#,,pcF^|-h.Qc:.oD$D*^pEhgb0#YPf|Q');
define('LOGGED_IN_KEY',    '.^m `wE9T7?i2+*7H:aZf?#(D*^my?)e*ua=d?u u-#on8c1;Z[*5edZG,~<7*3Z');
define('NONCE_KEY',        'uKE_by.z3tlB[]d}9XF,*{%4$isYN4&f7vm6_K{v;z{pN+wyaqEu[%fdk(GftiDI');
define('AUTH_SALT',        'U>*T[Kw)8?)tGo@-[=dY90d[1eQ$V|]K:vgjwuHVT0@DvXNLXG+x~t(%3s_I+3Qh');
define('SECURE_AUTH_SALT', '}QPu]Qt^^jsbi77k>sVarSPWM%>._*_y%rKL*Ti`}Wkrp:<w0!Wbxe|A20n>fHs[');
define('LOGGED_IN_SALT',   'OJ}ig1N@/_:<X?s@@Ey,-jvhKmdG)d]-hIm~$*R`u2&hY-t0#[VzpScS^;0NBT))');
define('NONCE_SALT',       '-)sc(=bIGi^RG^f7@Abz`IF1]]JNX*5K+a4DZ$N-7;:Jc.5R!W~d@j&.@aPFjY:f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'jowp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
ini_set('log_errors','On');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ERROR );
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');