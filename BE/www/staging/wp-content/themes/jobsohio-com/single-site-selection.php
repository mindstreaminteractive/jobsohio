<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jobs_Ohio
 */

get_header();
?>

	<div id="primary" class="content-area">
<?php
$image = get_field('banner_image');

$videoid = get_field('youtube_video_banner');

if ($videoid) {
?>
<div class="banner video"><iframe class="video-iframe" src="https://www.youtube.com/embed/<?php echo $videoid; ?>?autoplay=0&amp;controls=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0" allowfullscreen=""></iframe><img class="img-responsive" alt="" src="/site/templates/images/placebo.png"></div>
<?php

} else {

        if ($image['url']) {
?>
<div class="banner banner-hero fullwidthbanner" style="background-image:url(<?php echo $image['url']; ?>);"><div class="banner-hero-content"><div class="banner-hero-table"></div></div></div>
<?php } } ?>

		<main id="main" class="site-main <?php if ($_GET['show']=='map') { echo showmap; } ?>">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-sitessingle', get_post_type() );


			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				// comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php
$previous = get_previous_post();
$next = get_next_post();


	?>
<div class="lateral">
    <div class="constrain">
        <div class="container-fluid">
            <div class="row">
            

               
   <div class="col-xs-12"><a class="lateral-back" href="/site-selection/">Back to Site Selection</a></div>

            
                <div class="col-md-6">
                	<?php if ($next) { ?>
                	<a class="lateral-sibling lateral-prev" href="<?php echo get_the_permalink($next); ?>">
                        <div class="lateral-label">Next</div>
                        <div class="lateral-title"><?php echo get_the_title($next); ?></div>
                    </a>
                <?php } ?>

                </div>
                <div class="col-md-6">
                	<?php if ($previous) { ?>
                    <a class="lateral-sibling lateral-next" href="<?php echo get_the_permalink($previous); ?>">
                        <div class="lateral-label">Previous</div>
                        <div class="lateral-title"><?php echo get_the_title($previous); ?></div>
                    </a>
                       <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
