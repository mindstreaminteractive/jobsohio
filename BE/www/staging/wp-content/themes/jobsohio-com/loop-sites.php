<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

?>

<div class="constrain">
	<div class="container-fluid">
		<div class="row">

	<?php
		$jc=0;
		while ( have_posts() ) :
			the_post();



if ($jc==0) { echo "<div class='row jo-sitesrow'>"; }
$jc++;
			get_template_part( 'template-parts/content', 'sites' );




			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		if ($jc==3) { echo "</div>"; $jc=0; } 

		endwhile; // End of the loop.

		if ($jc<3 && $jc>0) { echo "</div>";  }

	// jobsohio_numeric_post_nav();
		?>
	</div>
	</div>
</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

