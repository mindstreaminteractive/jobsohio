<?php
/**
 * Jobs Ohio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Jobs_Ohio
 */
 
if ( ! function_exists( 'jobsohio_com_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jobsohio_com_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Jobs Ohio, use a find and replace
		 * to change 'jobsohio-com' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jobsohio-com', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		add_editor_style();

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		
		

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'jobsohio-com' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'jobsohio_com_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );


		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'jobsohio_com_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jobsohio_com_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'jobsohio_com_content_width', 640 );
}
add_action( 'after_setup_theme', 'jobsohio_com_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jobsohio_com_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jobsohio-com' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jobsohio-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="posts-meta-heading">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'News Sidebar', 'jobsohio-com' ),
		'id'            => 'newssidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jobsohio-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="posts-meta-heading">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Left', 'jobsohio-com' ),
		'id'            => 'footer-left',
		'description'   => esc_html__( 'Add widgets here.', 'jobsohio-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Menu Left', 'jobsohio-com' ),
		'id'            => 'footermenu-left',
		'description'   => esc_html__( 'Add widgets here.', 'jobsohio-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Menu Left', 'jobsohio-com' ),
		'id'            => 'footermenu-right',
		'description'   => esc_html__( 'Add widgets here.', 'jobsohio-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Copyright', 'jobsohio-com' ),
		'id'            => 'footercopyright',
		'description'   => esc_html__( 'Add widgets here.', 'jobsohio-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

}
add_action( 'widgets_init', 'jobsohio_com_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function jobsohio_com_scripts() {
	wp_enqueue_style( 'jobsohio-com-style', get_stylesheet_uri() );
    wp_enqueue_script( 'jobsohio-com-headroom', get_template_directory_uri() . '/js/headroom.js?v='.time(), array('jquery') );
    wp_enqueue_script( 'jobsohio-com-libs', get_template_directory_uri() . '/js/lib.js?v='.time(), array('jquery') );
	wp_enqueue_script( 'jobsohio-com-isotope', get_template_directory_uri() . '/js/isotope.js?v='.time(), array('jquery') );
	   wp_enqueue_script( 'jobsohio-com-custom', get_template_directory_uri() . '/js/custom.js?v='.time(), array('jquery') );
	wp_enqueue_script( 'jobsohio-com-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_style( 'jobsohio-style', get_stylesheet_directory_uri() . '/css/jobsohio-style.css?v='.time());
       



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jobsohio_com_scripts' );







/**
 * Login Screen: Change the login logo to your own logo
 */
function custom_login_logo() {
   echo '<style type="text/css">
    body.login div#login h1 a { background-image:url(/assets/img/jobsohio.svg) !important; background-size: auto auto !important; width: auto  !important; }
    </style>';
}
add_action('login_head', 'custom_login_logo');

/**
 * Login Screen: Use your own external URL for login logo link
 */
function url_login(){
   return get_bloginfo( 'wpurl' );
}
add_filter('login_headerurl', 'url_login');

	
	

/**
 * Login Screen: Change login logo hover text
 */
function login_logo_url_title() {
    return 'JobsOhio';
}
add_filter( 'login_headertitle', 'login_logo_url_title' );

function wpb_password_post_filter( $where = '' ) {
    if (!is_single() && !is_admin()) {
        $where .= " AND post_password = ''";
    }
    return $where;
}
// add_filter( 'posts_where', 'wpb_password_post_filter' );


add_filter( 'protected_title_format', 'remove_protected_text' );
function remove_protected_text() {
return __('%s');
}


function mytheme_add_widget_tabs($tabs) {
    $tabs[] = array(
        'title' => __('JobsOhio', 'jobsohio-com'),
        'filter' => array(
            'groups' => array('jobsohio-com')
        )
    );

    return $tabs;
}
add_filter('siteorigin_panels_widget_dialog_tabs', 'mytheme_add_widget_tabs', 20);


function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blog';
    $submenu['edit.php'][5][0] = 'Blog Posts';
    $submenu['edit.php'][10][0] = 'Add Blog Post';
    $submenu['edit.php'][16][0] = 'Blog Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Blog Posts';
    $labels->singular_name = 'Blog Post';
    $labels->add_new = 'Add Blog Post';
    $labels->add_new_item = 'Add Blog Post';
    $labels->edit_item = 'Edit Blog Post';
    $labels->new_item = 'Blog Posts';
    $labels->view_item = 'View Blog Post';
    $labels->search_items = 'Search Blog';
    $labels->not_found = 'No posts found';
    $labels->not_found_in_trash = 'No Posts found in Trash';
    $labels->all_items = 'All Blog Posts';
    $labels->menu_name = 'Blog';
    $labels->name_admin_bar = 'Blog';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
