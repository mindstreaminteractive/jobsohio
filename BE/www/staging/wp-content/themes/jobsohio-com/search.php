<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Jobs_Ohio
 */

get_header();
global $wp_query;
$total_results = $wp_query->found_posts;
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="constrain"><div class="container-fluid"><div class="intro-main"><div class="row"><div class="col-sm-10 col-sm-offset-1">
				<div class="unit-header"><h2 class="heading">Search Results</h2></div>
				<div class="unit-content"><p>You searched for <span class="search__terms"><?php echo get_search_query(); ?></span>. The following <?php echo $total_results; ?> results matched your query:</p>
					<div class="results">

                          <?php if ( have_posts() ) : ?>

			
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

				    </div>
                
                </div>
		</div>
	</div>
</div>
</div>
</div>

		

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
// get_sidebar();
get_footer();
