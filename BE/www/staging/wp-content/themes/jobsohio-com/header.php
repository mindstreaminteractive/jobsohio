<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jobs_Ohio
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
	<?php wp_head(); ?>

<!-- CookiePro Cookies Consent Notice start -->

<script src="https://cookiepro.blob.core.windows.net/consent/e4d2de6f-5614-4de8-a79a-c7b771fa40ad-test.js" type="text/javascript" charset="UTF-8"></script>

<script type="text/javascript">

function OptanonWrapper() { }

</script>

<!-- CookiePro Cookies Consent Notice end -->
	
	<!-- Google Tag Manager -->
<script type="text/plain" class="optanon-category-2">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-N2Z99Q');</script>

<!-- End Google Tag Manager -->

<script type="text/plain"  class="optanon-category-2" src="//cdn.callrail.com/companies/812091507/6e4656eef52260dd2d97/12/swap.js" async></script><meta name="msvalidate.01" content="ED029FD73889A6D2DA63866F3869E587" /><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-1180777438134466",
    enable_page_level_ads: true
  });
</script>




	<script src="https://www.google.com/recaptcha/api.js" async="" defer=""></script>
	<script>function recaptchaCallback() { console.log('test'); jQuery( '.msg-error' ).text('');jQuery('#submitBtn').prop('disabled', false);}</script>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2Z99Q" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->

		<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'jobsohio-com' ); ?></a>
		<div id="obfuscator"></div>
		<div id="nav">

	<div class="nav-inner mobinav">
		<a class="nav-logo" href="/"><img class="img-responsive" alt="JobsOhio" src="/assets/img/nav-logo.svg"></a><div id="close"><img class="img-responsive" alt="Close" src="/assets/img/close.svg"></div><div class="nav-scroll"><form id="search-nav" class="hidden-md hidden-lg" action="/" method="get"><input class="search__query" type="text" name="s" value="" placeholder="Search"><a class="search__submit" href="javascript:{}" onclick="document.getElementById(&quot;search-nav&quot;).submit(); return false;"><img width="44" height="44" alt="Search" src="/assets/img/search-submit.svg"></a></form>


			<?php
			wp_nav_menu( array(
				'menu' => '3076',
				'menu_id'        => 'primary-menu',
			) );
			?>
			<div class="vcard nav-vcard"><div class="tel">Contact JobsOhio: <strong><a href="tel:16142159324">1.614.215.9324</a></strong></div><div class="tel">Toll Free: <strong><a href="tel:18667277908">1.866.727.7908</a></strong></div></div>
		<!-- #site-navigation -->
	</div>
</div>
</div>


<header id="header" class="headroom headroom--top headroom--not-bottom">
	<div class="topbar visible-lg">
		<div class="container">
			<div class="row">
				<div class="topbarleft col-sm-6"><h4>Ohio's Economic Development Corporation</h4></div>
				<div class="topbarright col-sm-6"><a href="tel:614-245-3048"><img src="/assets/img/phone.svg" class="phoneicon">614-245-3048</a><a href="/contact/" class="whitebtn redbtn">CONTACT US</a></div>
			</div>
		</div>
	</div>
	<div class="container header-main">
		<div class="row">
			<div class="logowrap col-sm-2">
				<a id="logo" href="/"><img class="img-responsive" alt="JobsOhio" src="/assets/img/jobsohio.svg"></a>
			</div>
			<div class="logowrap col-sm-10 ">
	<nav id="desktop-nav" class="visible-lg">
		<?php
			wp_nav_menu( array(
				'menu' => '3076',
				'menu_id'        => 'primary-menu',
			) );
			?>
	</nav>
		<div id="toggle" class="visible-sm visible-xs"><img class="img-responsive" alt="Menu" src="/assets/img/toggle.svg"></div>

	       </div>

    <a class="search__submit" href="#" id="searchicon"><img width="24" height="24" alt="Search" src="/assets/img/search-submitdark.svg"></a>
	<form id="search-header" action="/" method="get"><input class="search__query" id="searchbox" type="text" name="s" value="" placeholder="Search"></form>
 </div>
</div>

</header>





