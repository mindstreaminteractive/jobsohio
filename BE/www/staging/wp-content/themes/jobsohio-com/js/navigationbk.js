/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */

	var body = $(document.body),
    nav = $("#nav"),
    obfuscator = $("#obfuscator");
$(document).ready(function() {
        $("#toggle, #close, #obfuscator").on("click", function() {
            nav.toggleClass("nav-open"), body.toggleClass("no-scroll"), obfuscator.toggleClass("is-visible")
        })
    }), $(document).keyup(function(e) {
        obfuscator.hasClass("is-visible") && 27 == e.keyCode && (nav.toggleClass("nav-open"), body.toggleClass("no-scroll"), obfuscator.toggleClass("is-visible"))
    }),
    function() {
        var e = document.querySelector("#header");
        window.location.hash && e.classList.add("headroom--unpinned"), new Headroom(e, {
            tolerance: {
                down: 10,
                up: 20
            },
            offset: 205
        }).init()
    }(), $(document).ready(function() {
        $("#nav .trigger").click(function() {
            $(this).toggleClass("open"), $(this).next("ul").slideToggle("fast")
        })
    }), $(document).ready(function() {
        $(".page-group-page").matchHeight(), $(".template-site-selection .site-name").matchHeight()
    }), $(document).ready(function() {
        $(".unit-map-link").click(function() {
            $(this).toggleClass("open");
            var e = $(this).data("id");
            $("#" + e).toggle()
        })
    });
	