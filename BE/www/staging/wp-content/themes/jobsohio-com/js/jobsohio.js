! function(e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function(e, t) {
    "use strict";

    function n(e, t) {
        t = t || ne;
        var n = t.createElement("script");
        n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
    }

    function i(e) {
        var t = !!e && "length" in e && e.length,
            n = ge.type(e);
        return "function" !== n && !ge.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }

    function o(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }

    function r(e, t, n) {
        return ge.isFunction(t) ? ge.grep(e, function(e, i) {
            return !!t.call(e, i, e) !== n
        }) : t.nodeType ? ge.grep(e, function(e) {
            return e === t !== n
        }) : "string" != typeof t ? ge.grep(e, function(e) {
            return ae.call(t, e) > -1 !== n
        }) : Ae.test(t) ? ge.filter(t, e, n) : (t = ge.filter(t, e), ge.grep(e, function(e) {
            return ae.call(t, e) > -1 !== n && 1 === e.nodeType
        }))
    }

    function s(e, t) {
        for (;
            (e = e[t]) && 1 !== e.nodeType;);
        return e
    }

    function a(e) {
        var t = {};
        return ge.each(e.match(De) || [], function(e, n) {
            t[n] = !0
        }), t
    }

    function l(e) {
        return e
    }

    function c(e) {
        throw e
    }

    function u(e, t, n, i) {
        var o;
        try {
            e && ge.isFunction(o = e.promise) ? o.call(e).done(t).fail(n) : e && ge.isFunction(o = e.then) ? o.call(e, t, n) : t.apply(void 0, [e].slice(i))
        } catch (e) {
            n.apply(void 0, [e])
        }
    }

    function d() {
        ne.removeEventListener("DOMContentLoaded", d), e.removeEventListener("load", d), ge.ready()
    }

    function p() {
        this.expando = ge.expando + p.uid++
    }

    function $(e) {
        return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Le.test(e) ? JSON.parse(e) : e)
    }

    function f(e, t, n) {
        var i;
        if (void 0 === n && 1 === e.nodeType)
            if (i = "data-" + t.replace(qe, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(i))) {
                try {
                    n = $(n)
                } catch (e) {}
                Fe.set(e, t, n)
            } else n = void 0;
        return n
    }

    function h(e, t, n, i) {
        var o, r = 1,
            s = 20,
            a = i ? function() {
                return i.cur()
            } : function() {
                return ge.css(e, t, "")
            },
            l = a(),
            c = n && n[3] || (ge.cssNumber[t] ? "" : "px"),
            u = (ge.cssNumber[t] || "px" !== c && +l) && Re.exec(ge.css(e, t));
        if (u && u[3] !== c) {
            c = c || u[3], n = n || [], u = +l || 1;
            do {
                r = r || ".5", u /= r, ge.style(e, t, u + c)
            } while (r !== (r = a() / l) && 1 !== r && --s)
        }
        return n && (u = +u || +l || 0, o = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = o)), o
    }

    function g(e) {
        var t, n = e.ownerDocument,
            i = e.nodeName,
            o = ze[i];
        return o || (t = n.body.appendChild(n.createElement(i)), o = ge.css(t, "display"), t.parentNode.removeChild(t), "none" === o && (o = "block"), ze[i] = o, o)
    }

    function m(e, t) {
        for (var n, i, o = [], r = 0, s = e.length; r < s; r++) i = e[r], i.style && (n = i.style.display, t ? ("none" === n && (o[r] = Oe.get(i, "display") || null, o[r] || (i.style.display = "")), "" === i.style.display && Me(i) && (o[r] = g(i))) : "none" !== n && (o[r] = "none", Oe.set(i, "display", n)));
        for (r = 0; r < s; r++) null != o[r] && (e[r].style.display = o[r]);
        return e
    }

    function v(e, t) {
        var n;
        return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && o(e, t) ? ge.merge([e], n) : n
    }

    function y(e, t) {
        for (var n = 0, i = e.length; n < i; n++) Oe.set(e[n], "globalEval", !t || Oe.get(t[n], "globalEval"))
    }

    function x(e, t, n, i, o) {
        for (var r, s, a, l, c, u, d = t.createDocumentFragment(), p = [], f = 0, h = e.length; f < h; f++)
            if ((r = e[f]) || 0 === r)
                if ("object" === ge.type(r)) ge.merge(p, r.nodeType ? [r] : r);
                else if (Ye.test(r)) {
            for (s = s || d.appendChild(t.createElement("div")), a = (Ue.exec(r) || ["", ""])[1].toLowerCase(), l = Qe[a] || Qe._default, s.innerHTML = l[1] + ge.htmlPrefilter(r) + l[2], u = l[0]; u--;) s = s.lastChild;
            ge.merge(p, s.childNodes), s = d.firstChild, s.textContent = ""
        } else p.push(t.createTextNode(r));
        for (d.textContent = "", f = 0; r = p[f++];)
            if (i && ge.inArray(r, i) > -1) o && o.push(r);
            else if (c = ge.contains(r.ownerDocument, r), s = v(d.appendChild(r), "script"), c && y(s), n)
            for (u = 0; r = s[u++];) Xe.test(r.type || "") && n.push(r);
        return d
    }

    function b() {
        return !0
    }

    function w() {
        return !1
    }

    function S() {
        try {
            return ne.activeElement
        } catch (e) {}
    }

    function T(e, t, n, i, o, r) {
        var s, a;
        if ("object" == typeof t) {
            "string" != typeof n && (i = i || n, n = void 0);
            for (a in t) T(e, a, n, i, t[a], r);
            return e
        }
        if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), !1 === o) o = w;
        else if (!o) return e;
        return 1 === r && (s = o, o = function(e) {
            return ge().off(e), s.apply(this, arguments)
        }, o.guid = s.guid || (s.guid = ge.guid++)), e.each(function() {
            ge.event.add(this, t, o, i, n)
        })
    }

    function C(e, t) {
        return o(e, "table") && o(11 !== t.nodeType ? t : t.firstChild, "tr") ? ge(">tbody", e)[0] || e : e
    }

    function A(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function I(e) {
        var t = it.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function P(e, t) {
        var n, i, o, r, s, a, l, c;
        if (1 === t.nodeType) {
            if (Oe.hasData(e) && (r = Oe.access(e), s = Oe.set(t, r), c = r.events)) {
                delete s.handle, s.events = {};
                for (o in c)
                    for (n = 0, i = c[o].length; n < i; n++) ge.event.add(t, o, c[o][n])
            }
            Fe.hasData(e) && (a = Fe.access(e), l = ge.extend({}, a), Fe.set(t, l))
        }
    }

    function k(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && Ve.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue)
    }

    function E(e, t, i, o) {
        t = re.apply([], t);
        var r, s, a, l, c, u, d = 0,
            p = e.length,
            f = p - 1,
            h = t[0],
            g = ge.isFunction(h);
        if (g || p > 1 && "string" == typeof h && !fe.checkClone && nt.test(h)) return e.each(function(n) {
            var r = e.eq(n);
            g && (t[0] = h.call(this, n, r.html())), E(r, t, i, o)
        });
        if (p && (r = x(t, e[0].ownerDocument, !1, e, o), s = r.firstChild, 1 === r.childNodes.length && (r = s), s || o)) {
            for (a = ge.map(v(r, "script"), A), l = a.length; d < p; d++) c = r, d !== f && (c = ge.clone(c, !0, !0), l && ge.merge(a, v(c, "script"))), i.call(e[d], c, d);
            if (l)
                for (u = a[a.length - 1].ownerDocument, ge.map(a, I), d = 0; d < l; d++) c = a[d], Xe.test(c.type || "") && !Oe.access(c, "globalEval") && ge.contains(u, c) && (c.src ? ge._evalUrl && ge._evalUrl(c.src) : n(c.textContent.replace(ot, ""), u))
        }
        return e
    }

    function D(e, t, n) {
        for (var i, o = t ? ge.filter(t, e) : e, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || ge.cleanData(v(i)), i.parentNode && (n && ge.contains(i.ownerDocument, i) && y(v(i, "script")), i.parentNode.removeChild(i));
        return e
    }

    function N(e, t, n) {
        var i, o, r, s, a = e.style;
        return n = n || at(e), n && (s = n.getPropertyValue(t) || n[t], "" !== s || ge.contains(e.ownerDocument, e) || (s = ge.style(e, t)), !fe.pixelMarginRight() && st.test(s) && rt.test(t) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
    }

    function j(e, t) {
        return {
            get: function() {
                return e() ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function H(e) {
        if (e in ft) return e;
        for (var t = e[0].toUpperCase() + e.slice(1), n = pt.length; n--;)
            if ((e = pt[n] + t) in ft) return e
    }

    function _(e) {
        var t = ge.cssProps[e];
        return t || (t = ge.cssProps[e] = H(e) || e), t
    }

    function O(e, t, n) {
        var i = Re.exec(t);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
    }

    function F(e, t, n, i, o) {
        var r, s = 0;
        for (r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0; r < 4; r += 2) "margin" === n && (s += ge.css(e, n + Be[r], !0, o)), i ? ("content" === n && (s -= ge.css(e, "padding" + Be[r], !0, o)), "margin" !== n && (s -= ge.css(e, "border" + Be[r] + "Width", !0, o))) : (s += ge.css(e, "padding" + Be[r], !0, o), "padding" !== n && (s += ge.css(e, "border" + Be[r] + "Width", !0, o)));
        return s
    }

    function L(e, t, n) {
        var i, o = at(e),
            r = N(e, t, o),
            s = "border-box" === ge.css(e, "boxSizing", !1, o);
        return st.test(r) ? r : (i = s && (fe.boxSizingReliable() || r === e.style[t]), "auto" === r && (r = e["offset" + t[0].toUpperCase() + t.slice(1)]), (r = parseFloat(r) || 0) + F(e, t, n || (s ? "border" : "content"), i, o) + "px")
    }

    function q(e, t, n, i, o) {
        return new q.prototype.init(e, t, n, i, o)
    }

    function R() {
        gt && (!1 === ne.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(R) : e.setTimeout(R, ge.fx.interval), ge.fx.tick())
    }

    function B() {
        return e.setTimeout(function() {
            ht = void 0
        }), ht = ge.now()
    }

    function M(e, t) {
        var n, i = 0,
            o = {
                height: e
            };
        for (t = t ? 1 : 0; i < 4; i += 2 - t) n = Be[i], o["margin" + n] = o["padding" + n] = e;
        return t && (o.opacity = o.width = e), o
    }

    function W(e, t, n) {
        for (var i, o = (U.tweeners[t] || []).concat(U.tweeners["*"]), r = 0, s = o.length; r < s; r++)
            if (i = o[r].call(n, t, e)) return i
    }

    function z(e, t, n) {
        var i, o, r, s, a, l, c, u, d = "width" in t || "height" in t,
            p = this,
            f = {},
            h = e.style,
            g = e.nodeType && Me(e),
            v = Oe.get(e, "fxshow");
        n.queue || (s = ge._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function() {
            s.unqueued || a()
        }), s.unqueued++, p.always(function() {
            p.always(function() {
                s.unqueued--, ge.queue(e, "fx").length || s.empty.fire()
            })
        }));
        for (i in t)
            if (o = t[i], mt.test(o)) {
                if (delete t[i], r = r || "toggle" === o, o === (g ? "hide" : "show")) {
                    if ("show" !== o || !v || void 0 === v[i]) continue;
                    g = !0
                }
                f[i] = v && v[i] || ge.style(e, i)
            }
        if ((l = !ge.isEmptyObject(t)) || !ge.isEmptyObject(f)) {
            d && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], c = v && v.display, null == c && (c = Oe.get(e, "display")), u = ge.css(e, "display"), "none" === u && (c ? u = c : (m([e], !0), c = e.style.display || c, u = ge.css(e, "display"), m([e]))), ("inline" === u || "inline-block" === u && null != c) && "none" === ge.css(e, "float") && (l || (p.done(function() {
                h.display = c
            }), null == c && (u = h.display, c = "none" === u ? "" : u)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function() {
                h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
            })), l = !1;
            for (i in f) l || (v ? "hidden" in v && (g = v.hidden) : v = Oe.access(e, "fxshow", {
                display: c
            }), r && (v.hidden = !g), g && m([e], !0), p.done(function() {
                g || m([e]), Oe.remove(e, "fxshow");
                for (i in f) ge.style(e, i, f[i])
            })), l = W(g ? v[i] : 0, i, p), i in v || (v[i] = l.start, g && (l.end = l.start, l.start = 0))
        }
    }

    function V(e, t) {
        var n, i, o, r, s;
        for (n in e)
            if (i = ge.camelCase(n), o = t[i], r = e[n], Array.isArray(r) && (o = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), (s = ge.cssHooks[i]) && "expand" in s) {
                r = s.expand(r), delete e[i];
                for (n in r) n in e || (e[n] = r[n], t[n] = o)
            } else t[i] = o
    }

    function U(e, t, n) {
        var i, o, r = 0,
            s = U.prefilters.length,
            a = ge.Deferred().always(function() {
                delete l.elem
            }),
            l = function() {
                if (o) return !1;
                for (var t = ht || B(), n = Math.max(0, c.startTime + c.duration - t), i = n / c.duration || 0, r = 1 - i, s = 0, l = c.tweens.length; s < l; s++) c.tweens[s].run(r);
                return a.notifyWith(e, [c, r, n]), r < 1 && l ? n : (l || a.notifyWith(e, [c, 1, 0]), a.resolveWith(e, [c]), !1)
            },
            c = a.promise({
                elem: e,
                props: ge.extend({}, t),
                opts: ge.extend(!0, {
                    specialEasing: {},
                    easing: ge.easing._default
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: ht || B(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var i = ge.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                    return c.tweens.push(i), i
                },
                stop: function(t) {
                    var n = 0,
                        i = t ? c.tweens.length : 0;
                    if (o) return this;
                    for (o = !0; n < i; n++) c.tweens[n].run(1);
                    return t ? (a.notifyWith(e, [c, 1, 0]), a.resolveWith(e, [c, t])) : a.rejectWith(e, [c, t]), this
                }
            }),
            u = c.props;
        for (V(u, c.opts.specialEasing); r < s; r++)
            if (i = U.prefilters[r].call(c, e, u, c.opts)) return ge.isFunction(i.stop) && (ge._queueHooks(c.elem, c.opts.queue).stop = ge.proxy(i.stop, i)), i;
        return ge.map(u, W, c), ge.isFunction(c.opts.start) && c.opts.start.call(e, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), ge.fx.timer(ge.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })), c
    }

    function X(e) {
        return (e.match(De) || []).join(" ")
    }

    function Q(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function Y(e, t, n, i) {
        var o;
        if (Array.isArray(t)) ge.each(t, function(t, o) {
            n || Pt.test(e) ? i(e, o) : Y(e + "[" + ("object" == typeof o && null != o ? t : "") + "]", o, n, i)
        });
        else if (n || "object" !== ge.type(t)) i(e, t);
        else
            for (o in t) Y(e + "[" + o + "]", t[o], n, i)
    }

    function J(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var i, o = 0,
                r = t.toLowerCase().match(De) || [];
            if (ge.isFunction(n))
                for (; i = r[o++];) "+" === i[0] ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
        }
    }

    function K(e, t, n, i) {
        function o(a) {
            var l;
            return r[a] = !0, ge.each(e[a] || [], function(e, a) {
                var c = a(t, n, i);
                return "string" != typeof c || s || r[c] ? s ? !(l = c) : void 0 : (t.dataTypes.unshift(c), o(c), !1)
            }), l
        }
        var r = {},
            s = e === $t;
        return o(t.dataTypes[0]) || !r["*"] && o("*")
    }

    function G(e, t) {
        var n, i, o = ge.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((o[n] ? e : i || (i = {}))[n] = t[n]);
        return i && ge.extend(!0, e, i), e
    }

    function Z(e, t, n) {
        for (var i, o, r, s, a = e.contents, l = e.dataTypes;
            "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i)
            for (o in a)
                if (a[o] && a[o].test(i)) {
                    l.unshift(o);
                    break
                }
        if (l[0] in n) r = l[0];
        else {
            for (o in n) {
                if (!l[0] || e.converters[o + " " + l[0]]) {
                    r = o;
                    break
                }
                s || (s = o)
            }
            r = r || s
        }
        if (r) return r !== l[0] && l.unshift(r), n[r]
    }

    function ee(e, t, n, i) {
        var o, r, s, a, l, c = {},
            u = e.dataTypes.slice();
        if (u[1])
            for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
        for (r = u.shift(); r;)
            if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift())
                if ("*" === r) r = l;
                else if ("*" !== l && l !== r) {
            if (!(s = c[l + " " + r] || c["* " + r]))
                for (o in c)
                    if (a = o.split(" "), a[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                        !0 === s ? s = c[o] : !0 !== c[o] && (r = a[0], u.unshift(a[1]));
                        break
                    }
            if (!0 !== s)
                if (s && e.throws) t = s(t);
                else try {
                    t = s(t)
                } catch (e) {
                    return {
                        state: "parsererror",
                        error: s ? e : "No conversion from " + l + " to " + r
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }
    var te = [],
        ne = e.document,
        ie = Object.getPrototypeOf,
        oe = te.slice,
        re = te.concat,
        se = te.push,
        ae = te.indexOf,
        le = {},
        ce = le.toString,
        ue = le.hasOwnProperty,
        de = ue.toString,
        pe = de.call(Object),
        fe = {},
        he = "3.2.1",
        ge = function(e, t) {
            return new ge.fn.init(e, t)
        },
        me = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ve = /^-ms-/,
        ye = /-([a-z])/g,
        xe = function(e, t) {
            return t.toUpperCase()
        };
    ge.fn = ge.prototype = {
        jquery: he,
        constructor: ge,
        length: 0,
        toArray: function() {
            return oe.call(this)
        },
        get: function(e) {
            return null == e ? oe.call(this) : e < 0 ? this[e + this.length] : this[e]
        },
        pushStack: function(e) {
            var t = ge.merge(this.constructor(), e);
            return t.prevObject = this, t
        },
        each: function(e) {
            return ge.each(this, e)
        },
        map: function(e) {
            return this.pushStack(ge.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        },
        slice: function() {
            return this.pushStack(oe.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (e < 0 ? t : 0);
            return this.pushStack(n >= 0 && n < t ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: se,
        sort: te.sort,
        splice: te.splice
    }, ge.extend = ge.fn.extend = function() {
        var e, t, n, i, o, r, s = arguments[0] || {},
            a = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || ge.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++)
            if (null != (e = arguments[a]))
                for (t in e) n = s[t], i = e[t], s !== i && (c && i && (ge.isPlainObject(i) || (o = Array.isArray(i))) ? (o ? (o = !1, r = n && Array.isArray(n) ? n : []) : r = n && ge.isPlainObject(n) ? n : {}, s[t] = ge.extend(c, r, i)) : void 0 !== i && (s[t] = i));
        return s
    }, ge.extend({
        expando: "jQuery" + (he + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
            throw new Error(e)
        },
        noop: function() {},
        isFunction: function(e) {
            return "function" === ge.type(e)
        },
        isWindow: function(e) {
            return null != e && e === e.window
        },
        isNumeric: function(e) {
            var t = ge.type(e);
            return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
        },
        isPlainObject: function(e) {
            var t, n;
            return !(!e || "[object Object]" !== ce.call(e) || (t = ie(e)) && ("function" != typeof(n = ue.call(t, "constructor") && t.constructor) || de.call(n) !== pe))
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? le[ce.call(e)] || "object" : typeof e
        },
        globalEval: function(e) {
            n(e)
        },
        camelCase: function(e) {
            return e.replace(ve, "ms-").replace(ye, xe)
        },
        each: function(e, t) {
            var n, o = 0;
            if (i(e))
                for (n = e.length; o < n && !1 !== t.call(e[o], o, e[o]); o++);
            else
                for (o in e)
                    if (!1 === t.call(e[o], o, e[o])) break;
            return e
        },
        trim: function(e) {
            return null == e ? "" : (e + "").replace(me, "")
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (i(Object(e)) ? ge.merge(n, "string" == typeof e ? [e] : e) : se.call(n, e)), n
        },
        inArray: function(e, t, n) {
            return null == t ? -1 : ae.call(t, e, n)
        },
        merge: function(e, t) {
            for (var n = +t.length, i = 0, o = e.length; i < n; i++) e[o++] = t[i];
            return e.length = o, e
        },
        grep: function(e, t, n) {
            for (var i, o = [], r = 0, s = e.length, a = !n; r < s; r++)(i = !t(e[r], r)) !== a && o.push(e[r]);
            return o
        },
        map: function(e, t, n) {
            var o, r, s = 0,
                a = [];
            if (i(e))
                for (o = e.length; s < o; s++) null != (r = t(e[s], s, n)) && a.push(r);
            else
                for (s in e) null != (r = t(e[s], s, n)) && a.push(r);
            return re.apply([], a)
        },
        guid: 1,
        proxy: function(e, t) {
            var n, i, o;
            if ("string" == typeof t && (n = e[t], t = e, e = n), ge.isFunction(e)) return i = oe.call(arguments, 2), o = function() {
                return e.apply(t || this, i.concat(oe.call(arguments)))
            }, o.guid = e.guid = e.guid || ge.guid++, o
        },
        now: Date.now,
        support: fe
    }), "function" == typeof Symbol && (ge.fn[Symbol.iterator] = te[Symbol.iterator]), ge.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
        le["[object " + t + "]"] = t.toLowerCase()
    });
    var be = function(e) {
        function t(e, t, n, i) {
            var o, r, s, a, l, c, u, p = t && t.ownerDocument,
                h = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== h && 9 !== h && 11 !== h) return n;
            if (!i && ((t ? t.ownerDocument || t : M) !== H && j(t), t = t || H, O)) {
                if (11 !== h && (l = ye.exec(e)))
                    if (o = l[1]) {
                        if (9 === h) {
                            if (!(s = t.getElementById(o))) return n;
                            if (s.id === o) return n.push(s), n
                        } else if (p && (s = p.getElementById(o)) && R(t, s) && s.id === o) return n.push(s), n
                    } else {
                        if (l[2]) return Z.apply(n, t.getElementsByTagName(e)), n;
                        if ((o = l[3]) && S.getElementsByClassName && t.getElementsByClassName) return Z.apply(n, t.getElementsByClassName(o)), n
                    }
                if (S.qsa && !X[e + " "] && (!F || !F.test(e))) {
                    if (1 !== h) p = t, u = e;
                    else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((a = t.getAttribute("id")) ? a = a.replace(we, Se) : t.setAttribute("id", a = B), c = I(e), r = c.length; r--;) c[r] = "#" + a + " " + f(c[r]);
                        u = c.join(","), p = $.test(e) && d(t.parentNode) || t
                    }
                    if (u) try {
                        return Z.apply(n, p.querySelectorAll(u)), n
                    } catch (e) {} finally {
                        a === B && t.removeAttribute("id")
                    }
                }
            }
            return k(e.replace(le, "$1"), t, n, i)
        }

        function n() {
            function e(n, i) {
                return t.push(n + " ") > T.cacheLength && delete e[t.shift()], e[n + " "] = i
            }
            var t = [];
            return e
        }

        function i(e) {
            return e[B] = !0, e
        }

        function o(e) {
            var t = H.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function r(e, t) {
            for (var n = e.split("|"), i = n.length; i--;) T.attrHandle[n[i]] = t
        }

        function s(e, t) {
            var n = t && e,
                i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (i) return i;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === t) return -1;
            return e ? 1 : -1
        }

        function a(e) {
            return function(t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e
            }
        }

        function l(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function c(e) {
            return function(t) {
                return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && Ce(t) === e : t.disabled === e : "label" in t && t.disabled === e
            }
        }

        function u(e) {
            return i(function(t) {
                return t = +t, i(function(n, i) {
                    for (var o, r = e([], n.length, t), s = r.length; s--;) n[o = r[s]] && (n[o] = !(i[o] = n[o]))
                })
            })
        }

        function d(e) {
            return e && void 0 !== e.getElementsByTagName && e
        }

        function p() {}

        function f(e) {
            for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
            return i
        }

        function h(e, t, n) {
            var i = t.dir,
                o = t.next,
                r = o || i,
                s = n && "parentNode" === r,
                a = z++;
            return t.first ? function(t, n, o) {
                for (; t = t[i];)
                    if (1 === t.nodeType || s) return e(t, n, o);
                return !1
            } : function(t, n, l) {
                var c, u, d, p = [W, a];
                if (l) {
                    for (; t = t[i];)
                        if ((1 === t.nodeType || s) && e(t, n, l)) return !0
                } else
                    for (; t = t[i];)
                        if (1 === t.nodeType || s)
                            if (d = t[B] || (t[B] = {}), u = d[t.uniqueID] || (d[t.uniqueID] = {}), o && o === t.nodeName.toLowerCase()) t = t[i] || t;
                            else {
                                if ((c = u[r]) && c[0] === W && c[1] === a) return p[2] = c[2];
                                if (u[r] = p, p[2] = e(t, n, l)) return !0
                            } return !1
            }
        }

        function g(e) {
            return e.length > 1 ? function(t, n, i) {
                for (var o = e.length; o--;)
                    if (!e[o](t, n, i)) return !1;
                return !0
            } : e[0]
        }

        function m(e, n, i) {
            for (var o = 0, r = n.length; o < r; o++) t(e, n[o], i);
            return i
        }

        function v(e, t, n, i, o) {
            for (var r, s = [], a = 0, l = e.length, c = null != t; a < l; a++)(r = e[a]) && (n && !n(r, i, o) || (s.push(r), c && t.push(a)));
            return s
        }

        function y(e, t, n, o, r, s) {
            return o && !o[B] && (o = y(o)), r && !r[B] && (r = y(r, s)), i(function(i, s, a, l) {
                var c, u, d, p = [],
                    f = [],
                    h = s.length,
                    g = i || m(t || "*", a.nodeType ? [a] : a, []),
                    y = !e || !i && t ? g : v(g, p, e, a, l),
                    x = n ? r || (i ? e : h || o) ? [] : s : y;
                if (n && n(y, x, a, l), o)
                    for (c = v(x, f), o(c, [], a, l), u = c.length; u--;)(d = c[u]) && (x[f[u]] = !(y[f[u]] = d));
                if (i) {
                    if (r || e) {
                        if (r) {
                            for (c = [], u = x.length; u--;)(d = x[u]) && c.push(y[u] = d);
                            r(null, x = [], c, l)
                        }
                        for (u = x.length; u--;)(d = x[u]) && (c = r ? te(i, d) : p[u]) > -1 && (i[c] = !(s[c] = d))
                    }
                } else x = v(x === s ? x.splice(h, x.length) : x), r ? r(null, s, x, l) : Z.apply(s, x)
            })
        }

        function x(e) {
            for (var t, n, i, o = e.length, r = T.relative[e[0].type], s = r || T.relative[" "], a = r ? 1 : 0, l = h(function(e) {
                    return e === t
                }, s, !0), c = h(function(e) {
                    return te(t, e) > -1
                }, s, !0), u = [function(e, n, i) {
                    var o = !r && (i || n !== E) || ((t = n).nodeType ? l(e, n, i) : c(e, n, i));
                    return t = null, o
                }]; a < o; a++)
                if (n = T.relative[e[a].type]) u = [h(g(u), n)];
                else {
                    if (n = T.filter[e[a].type].apply(null, e[a].matches), n[B]) {
                        for (i = ++a; i < o && !T.relative[e[i].type]; i++);
                        return y(a > 1 && g(u), a > 1 && f(e.slice(0, a - 1).concat({
                            value: " " === e[a - 2].type ? "*" : ""
                        })).replace(le, "$1"), n, a < i && x(e.slice(a, i)), i < o && x(e = e.slice(i)), i < o && f(e))
                    }
                    u.push(n)
                }
            return g(u)
        }

        function b(e, n) {
            var o = n.length > 0,
                r = e.length > 0,
                s = function(i, s, a, l, c) {
                    var u, d, p, f = 0,
                        h = "0",
                        g = i && [],
                        m = [],
                        y = E,
                        x = i || r && T.find.TAG("*", c),
                        b = W += null == y ? 1 : Math.random() || .1,
                        w = x.length;
                    for (c && (E = s === H || s || c); h !== w && null != (u = x[h]); h++) {
                        if (r && u) {
                            for (d = 0, s || u.ownerDocument === H || (j(u), a = !O); p = e[d++];)
                                if (p(u, s || H, a)) {
                                    l.push(u);
                                    break
                                }
                            c && (W = b)
                        }
                        o && ((u = !p && u) && f--, i && g.push(u))
                    }
                    if (f += h, o && h !== f) {
                        for (d = 0; p = n[d++];) p(g, m, s, a);
                        if (i) {
                            if (f > 0)
                                for (; h--;) g[h] || m[h] || (m[h] = K.call(l));
                            m = v(m)
                        }
                        Z.apply(l, m), c && !i && m.length > 0 && f + n.length > 1 && t.uniqueSort(l)
                    }
                    return c && (W = b, E = y), g
                };
            return o ? i(s) : s
        }
        var w, S, T, C, A, I, P, k, E, D, N, j, H, _, O, F, L, q, R, B = "sizzle" + 1 * new Date,
            M = e.document,
            W = 0,
            z = 0,
            V = n(),
            U = n(),
            X = n(),
            Q = function(e, t) {
                return e === t && (N = !0), 0
            },
            Y = {}.hasOwnProperty,
            J = [],
            K = J.pop,
            G = J.push,
            Z = J.push,
            ee = J.slice,
            te = function(e, t) {
                for (var n = 0, i = e.length; n < i; n++)
                    if (e[n] === t) return n;
                return -1
            },
            ne = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ie = "[\\x20\\t\\r\\n\\f]",
            oe = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            re = "\\[" + ie + "*(" + oe + ")(?:" + ie + "*([*^$|!~]?=)" + ie + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + oe + "))|)" + ie + "*\\]",
            se = ":(" + oe + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + re + ")*)|.*)\\)|)",
            ae = new RegExp(ie + "+", "g"),
            le = new RegExp("^" + ie + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ie + "+$", "g"),
            ce = new RegExp("^" + ie + "*," + ie + "*"),
            ue = new RegExp("^" + ie + "*([>+~]|" + ie + ")" + ie + "*"),
            de = new RegExp("=" + ie + "*([^\\]'\"]*?)" + ie + "*\\]", "g"),
            pe = new RegExp(se),
            fe = new RegExp("^" + oe + "$"),
            he = {
                ID: new RegExp("^#(" + oe + ")"),
                CLASS: new RegExp("^\\.(" + oe + ")"),
                TAG: new RegExp("^(" + oe + "|[*])"),
                ATTR: new RegExp("^" + re),
                PSEUDO: new RegExp("^" + se),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ie + "*(even|odd|(([+-]|)(\\d*)n|)" + ie + "*(?:([+-]|)" + ie + "*(\\d+)|))" + ie + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + ne + ")$", "i"),
                needsContext: new RegExp("^" + ie + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ie + "*((?:-\\d)?\\d*)" + ie + "*\\)|)(?=[^-]|$)", "i")
            },
            ge = /^(?:input|select|textarea|button)$/i,
            me = /^h\d$/i,
            ve = /^[^{]+\{\s*\[native \w/,
            ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            $ = /[+~]/,
            xe = new RegExp("\\\\([\\da-f]{1,6}" + ie + "?|(" + ie + ")|.)", "ig"),
            be = function(e, t, n) {
                var i = "0x" + t - 65536;
                return i !== i || n ? t : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            },
            we = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            Se = function(e, t) {
                return t ? "\0" === e ? "ï¿½" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            },
            Te = function() {
                j()
            },
            Ce = h(function(e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            Z.apply(J = ee.call(M.childNodes), M.childNodes), J[M.childNodes.length].nodeType
        } catch (e) {
            Z = {
                apply: J.length ? function(e, t) {
                    G.apply(e, ee.call(t))
                } : function(e, t) {
                    for (var n = e.length, i = 0; e[n++] = t[i++];);
                    e.length = n - 1
                }
            }
        }
        S = t.support = {}, A = t.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, j = t.setDocument = function(e) {
            var t, n, i = e ? e.ownerDocument || e : M;
            return i !== H && 9 === i.nodeType && i.documentElement ? (H = i, _ = H.documentElement, O = !A(H), M !== H && (n = H.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Te, !1) : n.attachEvent && n.attachEvent("onunload", Te)), S.attributes = o(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), S.getElementsByTagName = o(function(e) {
                return e.appendChild(H.createComment("")), !e.getElementsByTagName("*").length
            }), S.getElementsByClassName = ve.test(H.getElementsByClassName), S.getById = o(function(e) {
                return _.appendChild(e).id = B, !H.getElementsByName || !H.getElementsByName(B).length
            }), S.getById ? (T.filter.ID = function(e) {
                var t = e.replace(xe, be);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }, T.find.ID = function(e, t) {
                if (void 0 !== t.getElementById && O) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }) : (T.filter.ID = function(e) {
                var t = e.replace(xe, be);
                return function(e) {
                    var n = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }, T.find.ID = function(e, t) {
                if (void 0 !== t.getElementById && O) {
                    var n, i, o, r = t.getElementById(e);
                    if (r) {
                        if ((n = r.getAttributeNode("id")) && n.value === e) return [r];
                        for (o = t.getElementsByName(e), i = 0; r = o[i++];)
                            if ((n = r.getAttributeNode("id")) && n.value === e) return [r]
                    }
                    return []
                }
            }), T.find.TAG = S.getElementsByTagName ? function(e, t) {
                return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : S.qsa ? t.querySelectorAll(e) : void 0
            } : function(e, t) {
                var n, i = [],
                    o = 0,
                    r = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = r[o++];) 1 === n.nodeType && i.push(n);
                    return i
                }
                return r
            }, T.find.CLASS = S.getElementsByClassName && function(e, t) {
                if (void 0 !== t.getElementsByClassName && O) return t.getElementsByClassName(e)
            }, L = [], F = [], (S.qsa = ve.test(H.querySelectorAll)) && (o(function(e) {
                _.appendChild(e).innerHTML = "<a id='" + B + "'></a><select id='" + B + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && F.push("[*^$]=" + ie + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || F.push("\\[" + ie + "*(?:value|" + ne + ")"), e.querySelectorAll("[id~=" + B + "-]").length || F.push("~="), e.querySelectorAll(":checked").length || F.push(":checked"), e.querySelectorAll("a#" + B + "+*").length || F.push(".#.+[+~]")
            }), o(function(e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = H.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && F.push("name" + ie + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && F.push(":enabled", ":disabled"), _.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && F.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), F.push(",.*:")
            })), (S.matchesSelector = ve.test(q = _.matches || _.webkitMatchesSelector || _.mozMatchesSelector || _.oMatchesSelector || _.msMatchesSelector)) && o(function(e) {
                S.disconnectedMatch = q.call(e, "*"), q.call(e, "[s!='']:x"), L.push("!=", se)
            }), F = F.length && new RegExp(F.join("|")), L = L.length && new RegExp(L.join("|")), t = ve.test(_.compareDocumentPosition), R = t || ve.test(_.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e,
                    i = t && t.parentNode;
                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1
            }, Q = t ? function(e, t) {
                if (e === t) return N = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !S.sortDetached && t.compareDocumentPosition(e) === n ? e === H || e.ownerDocument === M && R(M, e) ? -1 : t === H || t.ownerDocument === M && R(M, t) ? 1 : D ? te(D, e) - te(D, t) : 0 : 4 & n ? -1 : 1)
            } : function(e, t) {
                if (e === t) return N = !0, 0;
                var n, i = 0,
                    o = e.parentNode,
                    r = t.parentNode,
                    a = [e],
                    l = [t];
                if (!o || !r) return e === H ? -1 : t === H ? 1 : o ? -1 : r ? 1 : D ? te(D, e) - te(D, t) : 0;
                if (o === r) return s(e, t);
                for (n = e; n = n.parentNode;) a.unshift(n);
                for (n = t; n = n.parentNode;) l.unshift(n);
                for (; a[i] === l[i];) i++;
                return i ? s(a[i], l[i]) : a[i] === M ? -1 : l[i] === M ? 1 : 0
            }, H) : H
        }, t.matches = function(e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function(e, n) {
            if ((e.ownerDocument || e) !== H && j(e), n = n.replace(de, "='$1']"), S.matchesSelector && O && !X[n + " "] && (!L || !L.test(n)) && (!F || !F.test(n))) try {
                var i = q.call(e, n);
                if (i || S.disconnectedMatch || e.document && 11 !== e.document.nodeType) return i
            } catch (e) {}
            return t(n, H, null, [e]).length > 0
        }, t.contains = function(e, t) {
            return (e.ownerDocument || e) !== H && j(e), R(e, t)
        }, t.attr = function(e, t) {
            (e.ownerDocument || e) !== H && j(e);
            var n = T.attrHandle[t.toLowerCase()],
                i = n && Y.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !O) : void 0;
            return void 0 !== i ? i : S.attributes || !O ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }, t.escape = function(e) {
            return (e + "").replace(we, Se)
        }, t.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function(e) {
            var t, n = [],
                i = 0,
                o = 0;
            if (N = !S.detectDuplicates, D = !S.sortStable && e.slice(0), e.sort(Q), N) {
                for (; t = e[o++];) t === e[o] && (i = n.push(o));
                for (; i--;) e.splice(n[i], 1)
            }
            return D = null, e
        }, C = t.getText = function(e) {
            var t, n = "",
                i = 0,
                o = e.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += C(e)
                } else if (3 === o || 4 === o) return e.nodeValue
            } else
                for (; t = e[i++];) n += C(t);
            return n
        }, T = t.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: he,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(xe, be), e[3] = (e[3] || e[4] || e[5] || "").replace(xe, be), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                },
                PSEUDO: function(e) {
                    var t, n = !e[6] && e[2];
                    return he.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && pe.test(n) && (t = I(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(xe, be).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = V[e + " "];
                    return t || (t = new RegExp("(^|" + ie + ")" + e + "(" + ie + "|$)")) && V(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                    })
                },
                ATTR: function(e, n, i) {
                    return function(o) {
                        var r = t.attr(o, e);
                        return null == r ? "!=" === n : !n || (r += "", "=" === n ? r === i : "!=" === n ? r !== i : "^=" === n ? i && 0 === r.indexOf(i) : "*=" === n ? i && r.indexOf(i) > -1 : "$=" === n ? i && r.slice(-i.length) === i : "~=" === n ? (" " + r.replace(ae, " ") + " ").indexOf(i) > -1 : "|=" === n && (r === i || r.slice(0, i.length + 1) === i + "-"))
                    }
                },
                CHILD: function(e, t, n, i, o) {
                    var r = "nth" !== e.slice(0, 3),
                        s = "last" !== e.slice(-4),
                        a = "of-type" === t;
                    return 1 === i && 0 === o ? function(e) {
                        return !!e.parentNode
                    } : function(t, n, l) {
                        var c, u, d, p, f, h, g = r !== s ? "nextSibling" : "previousSibling",
                            m = t.parentNode,
                            v = a && t.nodeName.toLowerCase(),
                            y = !l && !a,
                            x = !1;
                        if (m) {
                            if (r) {
                                for (; g;) {
                                    for (p = t; p = p[g];)
                                        if (a ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) return !1;
                                    h = g = "only" === e && !h && "nextSibling"
                                }
                                return !0
                            }
                            if (h = [s ? m.firstChild : m.lastChild], s && y) {
                                for (p = m, d = p[B] || (p[B] = {}), u = d[p.uniqueID] || (d[p.uniqueID] = {}), c = u[e] || [], f = c[0] === W && c[1], x = f && c[2], p = f && m.childNodes[f]; p = ++f && p && p[g] || (x = f = 0) || h.pop();)
                                    if (1 === p.nodeType && ++x && p === t) {
                                        u[e] = [W, f, x];
                                        break
                                    }
                            } else if (y && (p = t, d = p[B] || (p[B] = {}), u = d[p.uniqueID] || (d[p.uniqueID] = {}), c = u[e] || [], f = c[0] === W && c[1], x = f), !1 === x)
                                for (;
                                    (p = ++f && p && p[g] || (x = f = 0) || h.pop()) && ((a ? p.nodeName.toLowerCase() !== v : 1 !== p.nodeType) || !++x || (y && (d = p[B] || (p[B] = {}), u = d[p.uniqueID] || (d[p.uniqueID] = {}), u[e] = [W, x]), p !== t)););
                            return (x -= o) === i || x % i == 0 && x / i >= 0
                        }
                    }
                },
                PSEUDO: function(e, n) {
                    var o, r = T.pseudos[e] || T.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return r[B] ? r(n) : r.length > 1 ? (o = [e, e, "", n], T.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function(e, t) {
                        for (var i, o = r(e, n), s = o.length; s--;) i = te(e, o[s]), e[i] = !(t[i] = o[s])
                    }) : function(e) {
                        return r(e, 0, o)
                    }) : r
                }
            },
            pseudos: {
                not: i(function(e) {
                    var t = [],
                        n = [],
                        o = P(e.replace(le, "$1"));
                    return o[B] ? i(function(e, t, n, i) {
                        for (var r, s = o(e, null, i, []), a = e.length; a--;)(r = s[a]) && (e[a] = !(t[a] = r))
                    }) : function(e, i, r) {
                        return t[0] = e, o(t, null, r, n), t[0] = null, !n.pop()
                    }
                }),
                has: i(function(e) {
                    return function(n) {
                        return t(e, n).length > 0
                    }
                }),
                contains: i(function(e) {
                    return e = e.replace(xe, be),
                        function(t) {
                            return (t.textContent || t.innerText || C(t)).indexOf(e) > -1
                        }
                }),
                lang: i(function(e) {
                    return fe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(xe, be).toLowerCase(),
                        function(t) {
                            var n;
                            do {
                                if (n = O ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-")
                            } while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                }),
                target: function(t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                },
                root: function(e) {
                    return e === _
                },
                focus: function(e) {
                    return e === H.activeElement && (!H.hasFocus || H.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                },
                enabled: c(!1),
                disabled: c(!0),
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6) return !1;
                    return !0
                },
                parent: function(e) {
                    return !T.pseudos.empty(e)
                },
                header: function(e) {
                    return me.test(e.nodeName)
                },
                input: function(e) {
                    return ge.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: u(function() {
                    return [0]
                }),
                last: u(function(e, t) {
                    return [t - 1]
                }),
                eq: u(function(e, t, n) {
                    return [n < 0 ? n + t : n]
                }),
                even: u(function(e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e
                }),
                odd: u(function(e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e
                }),
                lt: u(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; --i >= 0;) e.push(i);
                    return e
                }),
                gt: u(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
                    return e
                })
            }
        }, T.pseudos.nth = T.pseudos.eq;
        for (w in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) T.pseudos[w] = a(w);
        for (w in {
                submit: !0,
                reset: !0
            }) T.pseudos[w] = l(w);
        return p.prototype = T.filters = T.pseudos, T.setFilters = new p, I = t.tokenize = function(e, n) {
            var i, o, r, s, a, l, c, u = U[e + " "];
            if (u) return n ? 0 : u.slice(0);
            for (a = e, l = [], c = T.preFilter; a;) {
                i && !(o = ce.exec(a)) || (o && (a = a.slice(o[0].length) || a), l.push(r = [])), i = !1, (o = ue.exec(a)) && (i = o.shift(), r.push({
                    value: i,
                    type: o[0].replace(le, " ")
                }), a = a.slice(i.length));
                for (s in T.filter) !(o = he[s].exec(a)) || c[s] && !(o = c[s](o)) || (i = o.shift(), r.push({
                    value: i,
                    type: s,
                    matches: o
                }), a = a.slice(i.length));
                if (!i) break
            }
            return n ? a.length : a ? t.error(e) : U(e, l).slice(0)
        }, P = t.compile = function(e, t) {
            var n, i = [],
                o = [],
                r = X[e + " "];
            if (!r) {
                for (t || (t = I(e)), n = t.length; n--;) r = x(t[n]), r[B] ? i.push(r) : o.push(r);
                r = X(e, b(o, i)), r.selector = e
            }
            return r
        }, k = t.select = function(e, t, n, i) {
            var o, r, s, a, l, c = "function" == typeof e && e,
                u = !i && I(e = c.selector || e);
            if (n = n || [], 1 === u.length) {
                if (r = u[0] = u[0].slice(0), r.length > 2 && "ID" === (s = r[0]).type && 9 === t.nodeType && O && T.relative[r[1].type]) {
                    if (!(t = (T.find.ID(s.matches[0].replace(xe, be), t) || [])[0])) return n;
                    c && (t = t.parentNode), e = e.slice(r.shift().value.length)
                }
                for (o = he.needsContext.test(e) ? 0 : r.length; o-- && (s = r[o], !T.relative[a = s.type]);)
                    if ((l = T.find[a]) && (i = l(s.matches[0].replace(xe, be), $.test(r[0].type) && d(t.parentNode) || t))) {
                        if (r.splice(o, 1), !(e = i.length && f(r))) return Z.apply(n, i), n;
                        break
                    }
            }
            return (c || P(e, u))(i, t, !O, n, !t || $.test(e) && d(t.parentNode) || t), n
        }, S.sortStable = B.split("").sort(Q).join("") === B, S.detectDuplicates = !!N, j(), S.sortDetached = o(function(e) {
            return 1 & e.compareDocumentPosition(H.createElement("fieldset"))
        }), o(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function(e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), S.attributes && o(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || r("value", function(e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        }), o(function(e) {
            return null == e.getAttribute("disabled")
        }) || r(ne, function(e, t, n) {
            var i;
            if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }), t
    }(e);
    ge.find = be, ge.expr = be.selectors, ge.expr[":"] = ge.expr.pseudos, ge.uniqueSort = ge.unique = be.uniqueSort, ge.text = be.getText, ge.isXMLDoc = be.isXML, ge.contains = be.contains, ge.escapeSelector = be.escape;
    var we = function(e, t, n) {
            for (var i = [], o = void 0 !== n;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (o && ge(e).is(n)) break;
                    i.push(e)
                }
            return i
        },
        Se = function(e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        },
        Te = ge.expr.match.needsContext,
        Ce = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        Ae = /^.[^:#\[\.,]*$/;
    ge.filter = function(e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? ge.find.matchesSelector(i, e) ? [i] : [] : ge.find.matches(e, ge.grep(t, function(e) {
            return 1 === e.nodeType
        }))
    }, ge.fn.extend({
        find: function(e) {
            var t, n, i = this.length,
                o = this;
            if ("string" != typeof e) return this.pushStack(ge(e).filter(function() {
                for (t = 0; t < i; t++)
                    if (ge.contains(o[t], this)) return !0
            }));
            for (n = this.pushStack([]), t = 0; t < i; t++) ge.find(e, o[t], n);
            return i > 1 ? ge.uniqueSort(n) : n
        },
        filter: function(e) {
            return this.pushStack(r(this, e || [], !1))
        },
        not: function(e) {
            return this.pushStack(r(this, e || [], !0))
        },
        is: function(e) {
            return !!r(this, "string" == typeof e && Te.test(e) ? ge(e) : e || [], !1).length
        }
    });
    var Ie, Pe = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (ge.fn.init = function(e, t, n) {
        var i, o;
        if (!e) return this;
        if (n = n || Ie, "string" == typeof e) {
            if (!(i = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : Pe.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
            if (i[1]) {
                if (t = t instanceof ge ? t[0] : t, ge.merge(this, ge.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : ne, !0)), Ce.test(i[1]) && ge.isPlainObject(t))
                    for (i in t) ge.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                return this
            }
            return o = ne.getElementById(i[2]), o && (this[0] = o, this.length = 1), this
        }
        return e.nodeType ? (this[0] = e, this.length = 1, this) : ge.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(ge) : ge.makeArray(e, this)
    }).prototype = ge.fn, Ie = ge(ne);
    var ke = /^(?:parents|prev(?:Until|All))/,
        Ee = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    ge.fn.extend({
        has: function(e) {
            var t = ge(e, this),
                n = t.length;
            return this.filter(function() {
                for (var e = 0; e < n; e++)
                    if (ge.contains(this, t[e])) return !0
            })
        },
        closest: function(e, t) {
            var n, i = 0,
                o = this.length,
                r = [],
                s = "string" != typeof e && ge(e);
            if (!Te.test(e))
                for (; i < o; i++)
                    for (n = this[i]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && ge.find.matchesSelector(n, e))) {
                            r.push(n);
                            break
                        }
            return this.pushStack(r.length > 1 ? ge.uniqueSort(r) : r)
        },
        index: function(e) {
            return e ? "string" == typeof e ? ae.call(ge(e), this[0]) : ae.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            return this.pushStack(ge.uniqueSort(ge.merge(this.get(), ge(e, t))))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), ge.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return we(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return we(e, "parentNode", n)
        },
        next: function(e) {
            return s(e, "nextSibling")
        },
        prev: function(e) {
            return s(e, "previousSibling")
        },
        nextAll: function(e) {
            return we(e, "nextSibling")
        },
        prevAll: function(e) {
            return we(e, "previousSibling")
        },
        nextUntil: function(e, t, n) {
            return we(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return we(e, "previousSibling", n)
        },
        siblings: function(e) {
            return Se((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return Se(e.firstChild)
        },
        contents: function(e) {
            return o(e, "iframe") ? e.contentDocument : (o(e, "template") && (e = e.content || e), ge.merge([], e.childNodes))
        }
    }, function(e, t) {
        ge.fn[e] = function(n, i) {
            var o = ge.map(this, t, n);
            return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (o = ge.filter(i, o)), this.length > 1 && (Ee[e] || ge.uniqueSort(o), ke.test(e) && o.reverse()), this.pushStack(o)
        }
    });
    var De = /[^\x20\t\r\n\f]+/g;
    ge.Callbacks = function(e) {
        e = "string" == typeof e ? a(e) : ge.extend({}, e);
        var t, n, i, o, r = [],
            s = [],
            l = -1,
            c = function() {
                for (o = o || e.once, i = t = !0; s.length; l = -1)
                    for (n = s.shift(); ++l < r.length;) !1 === r[l].apply(n[0], n[1]) && e.stopOnFalse && (l = r.length, n = !1);
                e.memory || (n = !1), t = !1, o && (r = n ? [] : "")
            },
            u = {
                add: function() {
                    return r && (n && !t && (l = r.length - 1, s.push(n)), function t(n) {
                        ge.each(n, function(n, i) {
                            ge.isFunction(i) ? e.unique && u.has(i) || r.push(i) : i && i.length && "string" !== ge.type(i) && t(i)
                        })
                    }(arguments), n && !t && c()), this
                },
                remove: function() {
                    return ge.each(arguments, function(e, t) {
                        for (var n;
                            (n = ge.inArray(t, r, n)) > -1;) r.splice(n, 1), n <= l && l--
                    }), this
                },
                has: function(e) {
                    return e ? ge.inArray(e, r) > -1 : r.length > 0
                },
                empty: function() {
                    return r && (r = []), this
                },
                disable: function() {
                    return o = s = [], r = n = "", this
                },
                disabled: function() {
                    return !r
                },
                lock: function() {
                    return o = s = [], n || t || (r = n = ""), this
                },
                locked: function() {
                    return !!o
                },
                fireWith: function(e, n) {
                    return o || (n = n || [], n = [e, n.slice ? n.slice() : n], s.push(n), t || c()), this
                },
                fire: function() {
                    return u.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!i
                }
            };
        return u
    }, ge.extend({
        Deferred: function(t) {
            var n = [
                    ["notify", "progress", ge.Callbacks("memory"), ge.Callbacks("memory"), 2],
                    ["resolve", "done", ge.Callbacks("once memory"), ge.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", ge.Callbacks("once memory"), ge.Callbacks("once memory"), 1, "rejected"]
                ],
                i = "pending",
                o = {
                    state: function() {
                        return i
                    },
                    always: function() {
                        return r.done(arguments).fail(arguments), this
                    },
                    catch: function(e) {
                        return o.then(null, e)
                    },
                    pipe: function() {
                        var e = arguments;
                        return ge.Deferred(function(t) {
                            ge.each(n, function(n, i) {
                                var o = ge.isFunction(e[i[4]]) && e[i[4]];
                                r[i[1]](function() {
                                    var e = o && o.apply(this, arguments);
                                    e && ge.isFunction(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[i[0] + "With"](this, o ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    then: function(t, i, o) {
                        function r(t, n, i, o) {
                            return function() {
                                var a = this,
                                    u = arguments,
                                    d = function() {
                                        var e, d;
                                        if (!(t < s)) {
                                            if ((e = i.apply(a, u)) === n.promise()) throw new TypeError("Thenable self-resolution");
                                            d = e && ("object" == typeof e || "function" == typeof e) && e.then, ge.isFunction(d) ? o ? d.call(e, r(s, n, l, o), r(s, n, c, o)) : (s++, d.call(e, r(s, n, l, o), r(s, n, c, o), r(s, n, l, n.notifyWith))) : (i !== l && (a = void 0, u = [e]), (o || n.resolveWith)(a, u))
                                        }
                                    },
                                    p = o ? d : function() {
                                        try {
                                            d()
                                        } catch (e) {
                                            ge.Deferred.exceptionHook && ge.Deferred.exceptionHook(e, p.stackTrace), t + 1 >= s && (i !== c && (a = void 0, u = [e]), n.rejectWith(a, u))
                                        }
                                    };
                                t ? p() : (ge.Deferred.getStackHook && (p.stackTrace = ge.Deferred.getStackHook()), e.setTimeout(p))
                            }
                        }
                        var s = 0;
                        return ge.Deferred(function(e) {
                            n[0][3].add(r(0, e, ge.isFunction(o) ? o : l, e.notifyWith)), n[1][3].add(r(0, e, ge.isFunction(t) ? t : l)), n[2][3].add(r(0, e, ge.isFunction(i) ? i : c))
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? ge.extend(e, o) : o
                    }
                },
                r = {};
            return ge.each(n, function(e, t) {
                var s = t[2],
                    a = t[5];
                o[t[1]] = s.add, a && s.add(function() {
                    i = a
                }, n[3 - e][2].disable, n[0][2].lock), s.add(t[3].fire), r[t[0]] = function() {
                    return r[t[0] + "With"](this === r ? void 0 : this, arguments), this
                }, r[t[0] + "With"] = s.fireWith
            }), o.promise(r), t && t.call(r, r), r
        },
        when: function(e) {
            var t = arguments.length,
                n = t,
                i = Array(n),
                o = oe.call(arguments),
                r = ge.Deferred(),
                s = function(e) {
                    return function(n) {
                        i[e] = this, o[e] = arguments.length > 1 ? oe.call(arguments) : n, --t || r.resolveWith(i, o)
                    }
                };
            if (t <= 1 && (u(e, r.done(s(n)).resolve, r.reject, !t), "pending" === r.state() || ge.isFunction(o[n] && o[n].then))) return r.then();
            for (; n--;) u(o[n], s(n), r.reject);
            return r.promise()
        }
    });
    var Ne = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    ge.Deferred.exceptionHook = function(t, n) {
        e.console && e.console.warn && t && Ne.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, n)
    }, ge.readyException = function(t) {
        e.setTimeout(function() {
            throw t
        })
    };
    var je = ge.Deferred();
    ge.fn.ready = function(e) {
        return je.then(e).catch(function(e) {
            ge.readyException(e)
        }), this
    }, ge.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(e) {
            (!0 === e ? --ge.readyWait : ge.isReady) || (ge.isReady = !0, !0 !== e && --ge.readyWait > 0 || je.resolveWith(ne, [ge]))
        }
    }), ge.ready.then = je.then, "complete" === ne.readyState || "loading" !== ne.readyState && !ne.documentElement.doScroll ? e.setTimeout(ge.ready) : (ne.addEventListener("DOMContentLoaded", d), e.addEventListener("load", d));
    var He = function(e, t, n, i, o, r, s) {
            var a = 0,
                l = e.length,
                c = null == n;
            if ("object" === ge.type(n)) {
                o = !0;
                for (a in n) He(e, t, a, n[a], !0, r, s)
            } else if (void 0 !== i && (o = !0, ge.isFunction(i) || (s = !0), c && (s ? (t.call(e, i), t = null) : (c = t, t = function(e, t, n) {
                    return c.call(ge(e), n)
                })), t))
                for (; a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
            return o ? e : c ? t.call(e) : l ? t(e[0], n) : r
        },
        _e = function(e) {
            return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
        };
    p.uid = 1, p.prototype = {
        cache: function(e) {
            var t = e[this.expando];
            return t || (t = {}, _e(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        },
        set: function(e, t, n) {
            var i, o = this.cache(e);
            if ("string" == typeof t) o[ge.camelCase(t)] = n;
            else
                for (i in t) o[ge.camelCase(i)] = t[i];
            return o
        },
        get: function(e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][ge.camelCase(t)]
        },
        access: function(e, t, n) {
            return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
        },
        remove: function(e, t) {
            var n, i = e[this.expando];
            if (void 0 !== i) {
                if (void 0 !== t) {
                    Array.isArray(t) ? t = t.map(ge.camelCase) : (t = ge.camelCase(t), t = t in i ? [t] : t.match(De) || []), n = t.length;
                    for (; n--;) delete i[t[n]]
                }(void 0 === t || ge.isEmptyObject(i)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        },
        hasData: function(e) {
            var t = e[this.expando];
            return void 0 !== t && !ge.isEmptyObject(t)
        }
    };
    var Oe = new p,
        Fe = new p,
        Le = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        qe = /[A-Z]/g;
    ge.extend({
        hasData: function(e) {
            return Fe.hasData(e) || Oe.hasData(e)
        },
        data: function(e, t, n) {
            return Fe.access(e, t, n)
        },
        removeData: function(e, t) {
            Fe.remove(e, t)
        },
        _data: function(e, t, n) {
            return Oe.access(e, t, n)
        },
        _removeData: function(e, t) {
            Oe.remove(e, t)
        }
    }), ge.fn.extend({
        data: function(e, t) {
            var n, i, o, r = this[0],
                s = r && r.attributes;
            if (void 0 === e) {
                if (this.length && (o = Fe.get(r), 1 === r.nodeType && !Oe.get(r, "hasDataAttrs"))) {
                    for (n = s.length; n--;) s[n] && (i = s[n].name, 0 === i.indexOf("data-") && (i = ge.camelCase(i.slice(5)), f(r, i, o[i])));
                    Oe.set(r, "hasDataAttrs", !0)
                }
                return o
            }
            return "object" == typeof e ? this.each(function() {
                Fe.set(this, e)
            }) : He(this, function(t) {
                var n;
                if (r && void 0 === t) {
                    if (void 0 !== (n = Fe.get(r, e))) return n;
                    if (void 0 !== (n = f(r, e))) return n
                } else this.each(function() {
                    Fe.set(this, e, t)
                })
            }, null, t, arguments.length > 1, null, !0)
        },
        removeData: function(e) {
            return this.each(function() {
                Fe.remove(this, e)
            })
        }
    }), ge.extend({
        queue: function(e, t, n) {
            var i;
            if (e) return t = (t || "fx") + "queue", i = Oe.get(e, t), n && (!i || Array.isArray(n) ? i = Oe.access(e, t, ge.makeArray(n)) : i.push(n)), i || []
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = ge.queue(e, t),
                i = n.length,
                o = n.shift(),
                r = ge._queueHooks(e, t),
                s = function() {
                    ge.dequeue(e, t)
                };
            "inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, s, r)), !i && r && r.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return Oe.get(e, n) || Oe.access(e, n, {
                empty: ge.Callbacks("once memory").add(function() {
                    Oe.remove(e, [t + "queue", n])
                })
            })
        }
    }), ge.fn.extend({
        queue: function(e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? ge.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                var n = ge.queue(this, e, t);
                ge._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && ge.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                ge.dequeue(this, e)
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, t) {
            var n, i = 1,
                o = ge.Deferred(),
                r = this,
                s = this.length,
                a = function() {
                    --i || o.resolveWith(r, [r])
                };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;)(n = Oe.get(r[s], e + "queueHooks")) && n.empty && (i++, n.empty.add(a));
            return a(), o.promise(t)
        }
    });
    var $e = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Re = new RegExp("^(?:([+-])=|)(" + $e + ")([a-z%]*)$", "i"),
        Be = ["Top", "Right", "Bottom", "Left"],
        Me = function(e, t) {
            return e = t || e, "none" === e.style.display || "" === e.style.display && ge.contains(e.ownerDocument, e) && "none" === ge.css(e, "display")
        },
        We = function(e, t, n, i) {
            var o, r, s = {};
            for (r in t) s[r] = e.style[r], e.style[r] = t[r];
            o = n.apply(e, i || []);
            for (r in t) e.style[r] = s[r];
            return o
        },
        ze = {};
    ge.fn.extend({
        show: function() {
            return m(this, !0)
        },
        hide: function() {
            return m(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                Me(this) ? ge(this).show() : ge(this).hide()
            })
        }
    });
    var Ve = /^(?:checkbox|radio)$/i,
        Ue = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        Xe = /^$|\/(?:java|ecma)script/i,
        Qe = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Qe.optgroup = Qe.option, Qe.tbody = Qe.tfoot = Qe.colgroup = Qe.caption = Qe.thead, Qe.th = Qe.td;
    var Ye = /<|&#?\w+;/;
    ! function() {
        var e = ne.createDocumentFragment(),
            t = e.appendChild(ne.createElement("div")),
            n = ne.createElement("input");
        n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), t.appendChild(n), fe.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", fe.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
    }();
    var Je = ne.documentElement,
        Ke = /^key/,
        Ge = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Ze = /^([^.]*)(?:\.(.+)|)/;
    ge.event = {
        global: {},
        add: function(e, t, n, i, o) {
            var r, s, a, l, c, u, d, p, f, h, g, m = Oe.get(e);
            if (m)
                for (n.handler && (r = n, n = r.handler, o = r.selector), o && ge.find.matchesSelector(Je, o), n.guid || (n.guid = ge.guid++), (l = m.events) || (l = m.events = {}), (s = m.handle) || (s = m.handle = function(t) {
                        return void 0 !== ge && ge.event.triggered !== t.type ? ge.event.dispatch.apply(e, arguments) : void 0
                    }), t = (t || "").match(De) || [""], c = t.length; c--;) a = Ze.exec(t[c]) || [], f = g = a[1], h = (a[2] || "").split(".").sort(), f && (d = ge.event.special[f] || {}, f = (o ? d.delegateType : d.bindType) || f, d = ge.event.special[f] || {}, u = ge.extend({
                    type: f,
                    origType: g,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: o,
                    needsContext: o && ge.expr.match.needsContext.test(o),
                    namespace: h.join(".")
                }, r), (p = l[f]) || (p = l[f] = [], p.delegateCount = 0, d.setup && !1 !== d.setup.call(e, i, h, s) || e.addEventListener && e.addEventListener(f, s)), d.add && (d.add.call(e, u), u.handler.guid || (u.handler.guid = n.guid)), o ? p.splice(p.delegateCount++, 0, u) : p.push(u), ge.event.global[f] = !0)
        },
        remove: function(e, t, n, i, o) {
            var r, s, a, l, c, u, d, p, f, h, g, m = Oe.hasData(e) && Oe.get(e);
            if (m && (l = m.events)) {
                for (t = (t || "").match(De) || [""], c = t.length; c--;)
                    if (a = Ze.exec(t[c]) || [], f = g = a[1], h = (a[2] || "").split(".").sort(), f) {
                        for (d = ge.event.special[f] || {}, f = (i ? d.delegateType : d.bindType) || f, p = l[f] || [], a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = p.length; r--;) u = p[r], !o && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (p.splice(r, 1), u.selector && p.delegateCount--, d.remove && d.remove.call(e, u));
                        s && !p.length && (d.teardown && !1 !== d.teardown.call(e, h, m.handle) || ge.removeEvent(e, f, m.handle), delete l[f])
                    } else
                        for (f in l) ge.event.remove(e, f + t[c], n, i, !0);
                ge.isEmptyObject(l) && Oe.remove(e, "handle events")
            }
        },
        dispatch: function(e) {
            var t = ge.event.fix(e),
                n, i, o, r, s, a, l = new Array(arguments.length),
                c = (Oe.get(this, "events") || {})[t.type] || [],
                u = ge.event.special[t.type] || {};
            for (l[0] = t, n = 1; n < arguments.length; n++) l[n] = arguments[n];
            if (t.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, t)) {
                for (a = ge.event.handlers.call(this, t, c), n = 0;
                    (r = a[n++]) && !t.isPropagationStopped();)
                    for (t.currentTarget = r.elem, i = 0;
                        (s = r.handlers[i++]) && !t.isImmediatePropagationStopped();) t.rnamespace && !t.rnamespace.test(s.namespace) || (t.handleObj = s, t.data = s.data, void 0 !== (o = ((ge.event.special[s.origType] || {}).handle || s.handler).apply(r.elem, l)) && !1 === (t.result = o) && (t.preventDefault(), t.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, t), t.result
            }
        },
        handlers: function(e, t) {
            var n, i, o, r, s, a = [],
                l = t.delegateCount,
                c = e.target;
            if (l && c.nodeType && !("click" === e.type && e.button >= 1))
                for (; c !== this; c = c.parentNode || this)
                    if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
                        for (r = [], s = {}, n = 0; n < l; n++) i = t[n], o = i.selector + " ", void 0 === s[o] && (s[o] = i.needsContext ? ge(o, this).index(c) > -1 : ge.find(o, this, null, [c]).length), s[o] && r.push(i);
                        r.length && a.push({
                            elem: c,
                            handlers: r
                        })
                    }
            return c = this, l < t.length && a.push({
                elem: c,
                handlers: t.slice(l)
            }), a
        },
        addProp: function(e, t) {
            Object.defineProperty(ge.Event.prototype, e, {
                enumerable: !0,
                configurable: !0,
                get: ge.isFunction(t) ? function() {
                    if (this.originalEvent) return t(this.originalEvent)
                } : function() {
                    if (this.originalEvent) return this.originalEvent[e]
                },
                set: function(t) {
                    Object.defineProperty(this, e, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: t
                    })
                }
            })
        },
        fix: function(e) {
            return e[ge.expando] ? e : new ge.Event(e)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== S() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === S() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && o(this, "input")) return this.click(), !1
                },
                _default: function(e) {
                    return o(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, ge.removeEvent = function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, ge.Event = function(e, t) {
        return this instanceof ge.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? b : w, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && ge.extend(this, t), this.timeStamp = e && e.timeStamp || ge.now(), void(this[ge.expando] = !0)) : new ge.Event(e, t)
    }, ge.Event.prototype = {
        constructor: ge.Event,
        isDefaultPrevented: w,
        isPropagationStopped: w,
        isImmediatePropagationStopped: w,
        isSimulated: !1,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = b, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = b, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = b, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, ge.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(e) {
            var t = e.button;
            return null == e.which && Ke.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Ge.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, ge.event.addProp), ge.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(e, t) {
        ge.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var n, i = this,
                    o = e.relatedTarget,
                    r = e.handleObj;
                return o && (o === i || ge.contains(i, o)) || (e.type = r.origType, n = r.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), ge.fn.extend({
        on: function(e, t, n, i) {
            return T(this, e, t, n, i)
        },
        one: function(e, t, n, i) {
            return T(this, e, t, n, i, 1)
        },
        off: function(e, t, n) {
            var i, o;
            if (e && e.preventDefault && e.handleObj) return i = e.handleObj, ge(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" == typeof e) {
                for (o in e) this.off(o, t, e[o]);
                return this
            }
            return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = w), this.each(function() {
                ge.event.remove(this, e, n, t)
            })
        }
    });
    var et = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        tt = /<script|<style|<link/i,
        nt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        it = /^true\/(.*)/,
        ot = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    ge.extend({
        htmlPrefilter: function(e) {
            return e.replace(et, "<$1></$2>")
        },
        clone: function(e, t, n) {
            var i, o, r, s, a = e.cloneNode(!0),
                l = ge.contains(e.ownerDocument, e);
            if (!(fe.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ge.isXMLDoc(e)))
                for (s = v(a), r = v(e), i = 0, o = r.length; i < o; i++) k(r[i], s[i]);
            if (t)
                if (n)
                    for (r = r || v(e), s = s || v(a), i = 0, o = r.length; i < o; i++) P(r[i], s[i]);
                else P(e, a);
            return s = v(a, "script"), s.length > 0 && y(s, !l && v(e, "script")), a
        },
        cleanData: function(e) {
            for (var t, n, i, o = ge.event.special, r = 0; void 0 !== (n = e[r]); r++)
                if (_e(n)) {
                    if (t = n[Oe.expando]) {
                        if (t.events)
                            for (i in t.events) o[i] ? ge.event.remove(n, i) : ge.removeEvent(n, i, t.handle);
                        n[Oe.expando] = void 0
                    }
                    n[Fe.expando] && (n[Fe.expando] = void 0)
                }
        }
    }), ge.fn.extend({
        detach: function(e) {
            return D(this, e, !0)
        },
        remove: function(e) {
            return D(this, e)
        },
        text: function(e) {
            return He(this, function(e) {
                return void 0 === e ? ge.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        },
        append: function() {
            return E(this, arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    C(this, e).appendChild(e)
                }
            })
        },
        prepend: function() {
            return E(this, arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = C(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return E(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return E(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (ge.cleanData(v(e, !1)), e.textContent = "");
            return this
        },
        clone: function(e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function() {
                return ge.clone(this, e, t)
            })
        },
        html: function(e) {
            return He(this, function(e) {
                var t = this[0] || {},
                    n = 0,
                    i = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !tt.test(e) && !Qe[(Ue.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = ge.htmlPrefilter(e);
                    try {
                        for (; n < i; n++) t = this[n] || {}, 1 === t.nodeType && (ge.cleanData(v(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {}
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var e = [];
            return E(this, arguments, function(t) {
                var n = this.parentNode;
                ge.inArray(this, e) < 0 && (ge.cleanData(v(this)), n && n.replaceChild(t, this))
            }, e)
        }
    }), ge.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        ge.fn[e] = function(e) {
            for (var n, i = [], o = ge(e), r = o.length - 1, s = 0; s <= r; s++) n = s === r ? this : this.clone(!0), ge(o[s])[t](n), se.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var rt = /^margin/,
        st = new RegExp("^(" + $e + ")(?!px)[a-z%]+$", "i"),
        at = function(t) {
            var n = t.ownerDocument.defaultView;
            return n && n.opener || (n = e), n.getComputedStyle(t)
        };
    ! function() {
        function t() {
            if (a) {
                a.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", a.innerHTML = "", Je.appendChild(s);
                var t = e.getComputedStyle(a);
                n = "1%" !== t.top, r = "2px" === t.marginLeft, i = "4px" === t.width, a.style.marginRight = "50%", o = "4px" === t.marginRight, Je.removeChild(s), a = null
            }
        }
        var n, i, o, r, s = ne.createElement("div"),
            a = ne.createElement("div");
        a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", fe.clearCloneStyle = "content-box" === a.style.backgroundClip, s.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", s.appendChild(a), ge.extend(fe, {
            pixelPosition: function() {
                return t(), n
            },
            boxSizingReliable: function() {
                return t(), i
            },
            pixelMarginRight: function() {
                return t(), o
            },
            reliableMarginLeft: function() {
                return t(), r
            }
        }))
    }();
    var lt = /^(none|table(?!-c[ea]).+)/,
        ct = /^--/,
        ut = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        dt = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        pt = ["Webkit", "Moz", "ms"],
        ft = ne.createElement("div").style;
    ge.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = N(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: "cssFloat"
        },
        style: function(e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, r, s, a = ge.camelCase(t),
                    l = ct.test(t),
                    c = e.style;
                return l || (t = _(a)), s = ge.cssHooks[t] || ge.cssHooks[a], void 0 === n ? s && "get" in s && void 0 !== (o = s.get(e, !1, i)) ? o : c[t] : (r = typeof n, "string" === r && (o = Re.exec(n)) && o[1] && (n = h(e, t, o), r = "number"), void(null != n && n === n && ("number" === r && (n += o && o[3] || (ge.cssNumber[a] ? "" : "px")), fe.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l ? c.setProperty(t, n) : c[t] = n))))
            }
        },
        css: function(e, t, n, i) {
            var o, r, s, a = ge.camelCase(t);
            return ct.test(t) || (t = _(a)), s = ge.cssHooks[t] || ge.cssHooks[a], s && "get" in s && (o = s.get(e, !0, n)), void 0 === o && (o = N(e, t, i)), "normal" === o && t in dt && (o = dt[t]), "" === n || n ? (r = parseFloat(o), !0 === n || isFinite(r) ? r || 0 : o) : o
        }
    }), ge.each(["height", "width"], function(e, t) {
        ge.cssHooks[t] = {
            get: function(e, n, i) {
                if (n) return !lt.test(ge.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? L(e, t, i) : We(e, ut, function() {
                    return L(e, t, i)
                })
            },
            set: function(e, n, i) {
                var o, r = i && at(e),
                    s = i && F(e, t, i, "border-box" === ge.css(e, "boxSizing", !1, r), r);
                return s && (o = Re.exec(n)) && "px" !== (o[3] || "px") && (e.style[t] = n, n = ge.css(e, t)), O(e, n, s)
            }
        }
    }), ge.cssHooks.marginLeft = j(fe.reliableMarginLeft, function(e, t) {
        if (t) return (parseFloat(N(e, "marginLeft")) || e.getBoundingClientRect().left - We(e, {
            marginLeft: 0
        }, function() {
            return e.getBoundingClientRect().left
        })) + "px"
    }), ge.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        ge.cssHooks[e + t] = {
            expand: function(n) {
                for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; i < 4; i++) o[e + Be[i] + t] = r[i] || r[i - 2] || r[0];
                return o
            }
        }, rt.test(e) || (ge.cssHooks[e + t].set = O)
    }), ge.fn.extend({
        css: function(e, t) {
            return He(this, function(e, t, n) {
                var i, o, r = {},
                    s = 0;
                if (Array.isArray(t)) {
                    for (i = at(e), o = t.length; s < o; s++) r[t[s]] = ge.css(e, t[s], !1, i);
                    return r
                }
                return void 0 !== n ? ge.style(e, t, n) : ge.css(e, t)
            }, e, t, arguments.length > 1)
        }
    }), ge.Tween = q, q.prototype = {
        constructor: q,
        init: function(e, t, n, i, o, r) {
            this.elem = e, this.prop = n, this.easing = o || ge.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (ge.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = q.propHooks[this.prop];
            return e && e.get ? e.get(this) : q.propHooks._default.get(this)
        },
        run: function(e) {
            var t, n = q.propHooks[this.prop];
            return this.options.duration ? this.pos = t = ge.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : q.propHooks._default.set(this), this
        }
    }, q.prototype.init.prototype = q.prototype, q.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = ge.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0)
            },
            set: function(e) {
                ge.fx.step[e.prop] ? ge.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[ge.cssProps[e.prop]] && !ge.cssHooks[e.prop] ? e.elem[e.prop] = e.now : ge.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }, q.propHooks.scrollTop = q.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, ge.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        },
        _default: "swing"
    }, ge.fx = q.prototype.init, ge.fx.step = {};
    var ht, gt, mt = /^(?:toggle|show|hide)$/,
        vt = /queueHooks$/;
    ge.Animation = ge.extend(U, {
            tweeners: {
                "*": [function(e, t) {
                    var n = this.createTween(e, t);
                    return h(n.elem, e, Re.exec(t), n), n
                }]
            },
            tweener: function(e, t) {
                ge.isFunction(e) ? (t = e, e = ["*"]) : e = e.match(De);
                for (var n, i = 0, o = e.length; i < o; i++) n = e[i], U.tweeners[n] = U.tweeners[n] || [], U.tweeners[n].unshift(t)
            },
            prefilters: [z],
            prefilter: function(e, t) {
                t ? U.prefilters.unshift(e) : U.prefilters.push(e)
            }
        }), ge.speed = function(e, t, n) {
            var i = e && "object" == typeof e ? ge.extend({}, e) : {
                complete: n || !n && t || ge.isFunction(e) && e,
                duration: e,
                easing: n && t || t && !ge.isFunction(t) && t
            };
            return ge.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in ge.fx.speeds ? i.duration = ge.fx.speeds[i.duration] : i.duration = ge.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function() {
                ge.isFunction(i.old) && i.old.call(this), i.queue && ge.dequeue(this, i.queue)
            }, i
        }, ge.fn.extend({
            fadeTo: function(e, t, n, i) {
                return this.filter(Me).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, n, i)
            },
            animate: function(e, t, n, i) {
                var o = ge.isEmptyObject(e),
                    r = ge.speed(t, n, i),
                    s = function() {
                        var t = U(this, ge.extend({}, e), r);
                        (o || Oe.get(this, "finish")) && t.stop(!0)
                    };
                return s.finish = s, o || !1 === r.queue ? this.each(s) : this.queue(r.queue, s)
            },
            stop: function(e, t, n) {
                var i = function(e) {
                    var t = e.stop;
                    delete e.stop, t(n)
                };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && !1 !== e && this.queue(e || "fx", []), this.each(function() {
                    var t = !0,
                        o = null != e && e + "queueHooks",
                        r = ge.timers,
                        s = Oe.get(this);
                    if (o) s[o] && s[o].stop && i(s[o]);
                    else
                        for (o in s) s[o] && s[o].stop && vt.test(o) && i(s[o]);
                    for (o = r.length; o--;) r[o].elem !== this || null != e && r[o].queue !== e || (r[o].anim.stop(n), t = !1, r.splice(o, 1));
                    !t && n || ge.dequeue(this, e)
                })
            },
            finish: function(e) {
                return !1 !== e && (e = e || "fx"), this.each(function() {
                    var t, n = Oe.get(this),
                        i = n[e + "queue"],
                        o = n[e + "queueHooks"],
                        r = ge.timers,
                        s = i ? i.length : 0;
                    for (n.finish = !0, ge.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = r.length; t--;) r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
                    for (t = 0; t < s; t++) i[t] && i[t].finish && i[t].finish.call(this);
                    delete n.finish
                })
            }
        }), ge.each(["toggle", "show", "hide"], function(e, t) {
            var n = ge.fn[t];
            ge.fn[t] = function(e, i, o) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(M(t, !0), e, i, o)
            }
        }), ge.each({
            slideDown: M("show"),
            slideUp: M("hide"),
            slideToggle: M("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, t) {
            ge.fn[e] = function(e, n, i) {
                return this.animate(t, e, n, i)
            }
        }), ge.timers = [], ge.fx.tick = function() {
            var e, t = 0,
                n = ge.timers;
            for (ht = ge.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
            n.length || ge.fx.stop(), ht = void 0
        }, ge.fx.timer = function(e) {
            ge.timers.push(e), ge.fx.start()
        }, ge.fx.interval = 13, ge.fx.start = function() {
            gt || (gt = !0, R())
        }, ge.fx.stop = function() {
            gt = null
        }, ge.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, ge.fn.delay = function(t, n) {
            return t = ge.fx ? ge.fx.speeds[t] || t : t, n = n || "fx", this.queue(n, function(n, i) {
                var o = e.setTimeout(n, t);
                i.stop = function() {
                    e.clearTimeout(o)
                }
            })
        },
        function() {
            var e = ne.createElement("input"),
                t = ne.createElement("select"),
                n = t.appendChild(ne.createElement("option"));
            e.type = "checkbox", fe.checkOn = "" !== e.value, fe.optSelected = n.selected, e = ne.createElement("input"), e.value = "t", e.type = "radio", fe.radioValue = "t" === e.value
        }();
    var yt, xt = ge.expr.attrHandle;
    ge.fn.extend({
        attr: function(e, t) {
            return He(this, ge.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                ge.removeAttr(this, e)
            })
        }
    }), ge.extend({
        attr: function(e, t, n) {
            var i, o, r = e.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return void 0 === e.getAttribute ? ge.prop(e, t, n) : (1 === r && ge.isXMLDoc(e) || (o = ge.attrHooks[t.toLowerCase()] || (ge.expr.match.bool.test(t) ? yt : void 0)), void 0 !== n ? null === n ? void ge.removeAttr(e, t) : o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : o && "get" in o && null !== (i = o.get(e, t)) ? i : (i = ge.find.attr(e, t), null == i ? void 0 : i))
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!fe.radioValue && "radio" === t && o(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        removeAttr: function(e, t) {
            var n, i = 0,
                o = t && t.match(De);
            if (o && 1 === e.nodeType)
                for (; n = o[i++];) e.removeAttribute(n)
        }
    }), yt = {
        set: function(e, t, n) {
            return !1 === t ? ge.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, ge.each(ge.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = xt[t] || ge.find.attr;
        xt[t] = function(e, t, i) {
            var o, r, s = t.toLowerCase();
            return i || (r = xt[s], xt[s] = o, o = null != n(e, t, i) ? s : null, xt[s] = r), o
        }
    });
    var bt = /^(?:input|select|textarea|button)$/i,
        wt = /^(?:a|area)$/i;
    ge.fn.extend({
        prop: function(e, t) {
            return He(this, ge.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return this.each(function() {
                delete this[ge.propFix[e] || e]
            })
        }
    }), ge.extend({
        prop: function(e, t, n) {
            var i, o, r = e.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return 1 === r && ge.isXMLDoc(e) || (t = ge.propFix[t] || t, o = ge.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = ge.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : bt.test(e.nodeName) || wt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        }
    }), fe.optSelected || (ge.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        },
        set: function(e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), ge.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        ge.propFix[this.toLowerCase()] = this
    }), ge.fn.extend({
        addClass: function(e) {
            var t, n, i, o, r, s, a, l = 0;
            if (ge.isFunction(e)) return this.each(function(t) {
                ge(this).addClass(e.call(this, t, Q(this)))
            });
            if ("string" == typeof e && e)
                for (t = e.match(De) || []; n = this[l++];)
                    if (o = Q(n), i = 1 === n.nodeType && " " + X(o) + " ") {
                        for (s = 0; r = t[s++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                        a = X(i), o !== a && n.setAttribute("class", a)
                    }
            return this
        },
        removeClass: function(e) {
            var t, n, i, o, r, s, a, l = 0;
            if (ge.isFunction(e)) return this.each(function(t) {
                ge(this).removeClass(e.call(this, t, Q(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof e && e)
                for (t = e.match(De) || []; n = this[l++];)
                    if (o = Q(n), i = 1 === n.nodeType && " " + X(o) + " ") {
                        for (s = 0; r = t[s++];)
                            for (; i.indexOf(" " + r + " ") > -1;) i = i.replace(" " + r + " ", " ");
                        a = X(i), o !== a && n.setAttribute("class", a)
                    }
            return this
        },
        toggleClass: function(e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : ge.isFunction(e) ? this.each(function(n) {
                ge(this).toggleClass(e.call(this, n, Q(this), t), t)
            }) : this.each(function() {
                var t, i, o, r;
                if ("string" === n)
                    for (i = 0, o = ge(this), r = e.match(De) || []; t = r[i++];) o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
                else void 0 !== e && "boolean" !== n || (t = Q(this), t && Oe.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : Oe.get(this, "__className__") || ""))
            })
        },
        hasClass: function(e) {
            var t, n, i = 0;
            for (t = " " + e + " "; n = this[i++];)
                if (1 === n.nodeType && (" " + X(Q(n)) + " ").indexOf(t) > -1) return !0;
            return !1
        }
    });
    var St = /\r/g;
    ge.fn.extend({
        val: function(e) {
            var t, n, i, o = this[0];
            return arguments.length ? (i = ge.isFunction(e), this.each(function(n) {
                var o;
                1 === this.nodeType && (o = i ? e.call(this, n, ge(this).val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : Array.isArray(o) && (o = ge.map(o, function(e) {
                    return null == e ? "" : e + ""
                })), (t = ge.valHooks[this.type] || ge.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, o, "value") || (this.value = o))
            })) : o ? (t = ge.valHooks[o.type] || ge.valHooks[o.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(o, "value")) ? n : (n = o.value, "string" == typeof n ? n.replace(St, "") : null == n ? "" : n)) : void 0
        }
    }), ge.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = ge.find.attr(e, "value");
                    return null != t ? t : X(ge.text(e))
                }
            },
            select: {
                get: function(e) {
                    var t, n, i, r = e.options,
                        s = e.selectedIndex,
                        a = "select-one" === e.type,
                        l = a ? null : [],
                        c = a ? s + 1 : r.length;
                    for (i = s < 0 ? c : a ? s : 0; i < c; i++)
                        if (n = r[i], (n.selected || i === s) && !n.disabled && (!n.parentNode.disabled || !o(n.parentNode, "optgroup"))) {
                            if (t = ge(n).val(), a) return t;
                            l.push(t)
                        }
                    return l
                },
                set: function(e, t) {
                    for (var n, i, o = e.options, r = ge.makeArray(t), s = o.length; s--;) i = o[s], (i.selected = ge.inArray(ge.valHooks.option.get(i), r) > -1) && (n = !0);
                    return n || (e.selectedIndex = -1), r
                }
            }
        }
    }), ge.each(["radio", "checkbox"], function() {
        ge.valHooks[this] = {
            set: function(e, t) {
                if (Array.isArray(t)) return e.checked = ge.inArray(ge(e).val(), t) > -1
            }
        }, fe.checkOn || (ge.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var Tt = /^(?:focusinfocus|focusoutblur)$/;
    ge.extend(ge.event, {
        trigger: function(t, n, i, o) {
            var r, s, a, l, c, u, d, p = [i || ne],
                f = ue.call(t, "type") ? t.type : t,
                h = ue.call(t, "namespace") ? t.namespace.split(".") : [];
            if (s = a = i = i || ne, 3 !== i.nodeType && 8 !== i.nodeType && !Tt.test(f + ge.event.triggered) && (f.indexOf(".") > -1 && (h = f.split("."), f = h.shift(), h.sort()), c = f.indexOf(":") < 0 && "on" + f, t = t[ge.expando] ? t : new ge.Event(f, "object" == typeof t && t), t.isTrigger = o ? 2 : 3, t.namespace = h.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : ge.makeArray(n, [t]), d = ge.event.special[f] || {}, o || !d.trigger || !1 !== d.trigger.apply(i, n))) {
                if (!o && !d.noBubble && !ge.isWindow(i)) {
                    for (l = d.delegateType || f, Tt.test(l + f) || (s = s.parentNode); s; s = s.parentNode) p.push(s), a = s;
                    a === (i.ownerDocument || ne) && p.push(a.defaultView || a.parentWindow || e)
                }
                for (r = 0;
                    (s = p[r++]) && !t.isPropagationStopped();) t.type = r > 1 ? l : d.bindType || f, u = (Oe.get(s, "events") || {})[t.type] && Oe.get(s, "handle"), u && u.apply(s, n), (u = c && s[c]) && u.apply && _e(s) && (t.result = u.apply(s, n), !1 === t.result && t.preventDefault());
                return t.type = f, o || t.isDefaultPrevented() || d._default && !1 !== d._default.apply(p.pop(), n) || !_e(i) || c && ge.isFunction(i[f]) && !ge.isWindow(i) && (a = i[c], a && (i[c] = null), ge.event.triggered = f, i[f](), ge.event.triggered = void 0, a && (i[c] = a)), t.result
            }
        },
        simulate: function(e, t, n) {
            var i = ge.extend(new ge.Event, n, {
                type: e,
                isSimulated: !0
            });
            ge.event.trigger(i, null, t)
        }
    }), ge.fn.extend({
        trigger: function(e, t) {
            return this.each(function() {
                ge.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, t) {
            var n = this[0];
            if (n) return ge.event.trigger(e, t, n, !0)
        }
    }), ge.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, t) {
        ge.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), ge.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), fe.focusin = "onfocusin" in e, fe.focusin || ge.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var n = function(e) {
            ge.event.simulate(t, e.target, ge.event.fix(e))
        };
        ge.event.special[t] = {
            setup: function() {
                var i = this.ownerDocument || this,
                    o = Oe.access(i, t);
                o || i.addEventListener(e, n, !0), Oe.access(i, t, (o || 0) + 1)
            },
            teardown: function() {
                var i = this.ownerDocument || this,
                    o = Oe.access(i, t) - 1;
                o ? Oe.access(i, t, o) : (i.removeEventListener(e, n, !0), Oe.remove(i, t))
            }
        }
    });
    var Ct = e.location,
        At = ge.now(),
        It = /\?/;
    ge.parseXML = function(t) {
        var n;
        if (!t || "string" != typeof t) return null;
        try {
            n = (new e.DOMParser).parseFromString(t, "text/xml")
        } catch (e) {
            n = void 0
        }
        return n && !n.getElementsByTagName("parsererror").length || ge.error("Invalid XML: " + t), n
    };
    var Pt = /\[\]$/,
        kt = /\r?\n/g,
        Et = /^(?:submit|button|image|reset|file)$/i,
        Dt = /^(?:input|select|textarea|keygen)/i;
    ge.param = function(e, t) {
        var n, i = [],
            o = function(e, t) {
                var n = ge.isFunction(t) ? t() : t;
                i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
            };
        if (Array.isArray(e) || e.jquery && !ge.isPlainObject(e)) ge.each(e, function() {
            o(this.name, this.value)
        });
        else
            for (n in e) Y(n, e[n], t, o);
        return i.join("&")
    }, ge.fn.extend({
        serialize: function() {
            return ge.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = ge.prop(this, "elements");
                return e ? ge.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !ge(this).is(":disabled") && Dt.test(this.nodeName) && !Et.test(e) && (this.checked || !Ve.test(e))
            }).map(function(e, t) {
                var n = ge(this).val();
                return null == n ? null : Array.isArray(n) ? ge.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(kt, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(kt, "\r\n")
                }
            }).get()
        }
    });
    var Nt = /%20/g,
        jt = /#.*$/,
        Ht = /([?&])_=[^&]*/,
        _t = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Ot = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Ft = /^(?:GET|HEAD)$/,
        Lt = /^\/\//,
        qt = {},
        $t = {},
        Rt = "*/".concat("*"),
        Bt = ne.createElement("a");
    Bt.href = Ct.href, ge.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Ct.href,
            type: "GET",
            isLocal: Ot.test(Ct.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Rt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": ge.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? G(G(e, ge.ajaxSettings), t) : G(ge.ajaxSettings, e)
        },
        ajaxPrefilter: J(qt),
        ajaxTransport: J($t),
        ajax: function(t, n) {
            function i(t, n, i, a) {
                var c, p, f, b, w, S = n;
                u || (u = !0, l && e.clearTimeout(l), o = void 0, s = a || "", T.readyState = t > 0 ? 4 : 0, c = t >= 200 && t < 300 || 304 === t, i && (b = Z(h, T, i)), b = ee(h, b, T, c), c ? (h.ifModified && (w = T.getResponseHeader("Last-Modified"), w && (ge.lastModified[r] = w), (w = T.getResponseHeader("etag")) && (ge.etag[r] = w)), 204 === t || "HEAD" === h.type ? S = "nocontent" : 304 === t ? S = "notmodified" : (S = b.state, p = b.data, f = b.error, c = !f)) : (f = S, !t && S || (S = "error", t < 0 && (t = 0))), T.status = t, T.statusText = (n || S) + "", c ? v.resolveWith(g, [p, S, T]) : v.rejectWith(g, [T, S, f]), T.statusCode(x), x = void 0, d && m.trigger(c ? "ajaxSuccess" : "ajaxError", [T, h, c ? p : f]), y.fireWith(g, [T, S]), d && (m.trigger("ajaxComplete", [T, h]), --ge.active || ge.event.trigger("ajaxStop")))
            }
            "object" == typeof t && (n = t, t = void 0), n = n || {};
            var o, r, s, a, l, c, u, d, p, f, h = ge.ajaxSetup({}, n),
                g = h.context || h,
                m = h.context && (g.nodeType || g.jquery) ? ge(g) : ge.event,
                v = ge.Deferred(),
                y = ge.Callbacks("once memory"),
                x = h.statusCode || {},
                b = {},
                w = {},
                S = "canceled",
                T = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (u) {
                            if (!a)
                                for (a = {}; t = _t.exec(s);) a[t[1].toLowerCase()] = t[2];
                            t = a[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return u ? s : null
                    },
                    setRequestHeader: function(e, t) {
                        return null == u && (e = w[e.toLowerCase()] = w[e.toLowerCase()] || e, b[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return null == u && (h.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (u) T.always(e[T.status]);
                            else
                                for (t in e) x[t] = [x[t], e[t]];
                        return this
                    },
                    abort: function(e) {
                        var t = e || S;
                        return o && o.abort(t), i(0, t), this
                    }
                };
            if (v.promise(T), h.url = ((t || h.url || Ct.href) + "").replace(Lt, Ct.protocol + "//"), h.type = n.method || n.type || h.method || h.type, h.dataTypes = (h.dataType || "*").toLowerCase().match(De) || [""], null == h.crossDomain) {
                c = ne.createElement("a");
                try {
                    c.href = h.url, c.href = c.href, h.crossDomain = Bt.protocol + "//" + Bt.host != c.protocol + "//" + c.host
                } catch (e) {
                    h.crossDomain = !0
                }
            }
            if (h.data && h.processData && "string" != typeof h.data && (h.data = ge.param(h.data, h.traditional)), K(qt, h, n, T), u) return T;
            d = ge.event && h.global, d && 0 == ge.active++ && ge.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Ft.test(h.type), r = h.url.replace(jt, ""), h.hasContent ? h.data && h.processData && 0 === (h.contentType || "").indexOf("application/x-www-form-urlencoded") && (h.data = h.data.replace(Nt, "+")) : (f = h.url.slice(r.length), h.data && (r += (It.test(r) ? "&" : "?") + h.data, delete h.data), !1 === h.cache && (r = r.replace(Ht, "$1"), f = (It.test(r) ? "&" : "?") + "_=" + At++ + f), h.url = r + f), h.ifModified && (ge.lastModified[r] && T.setRequestHeader("If-Modified-Since", ge.lastModified[r]), ge.etag[r] && T.setRequestHeader("If-None-Match", ge.etag[r])), (h.data && h.hasContent && !1 !== h.contentType || n.contentType) && T.setRequestHeader("Content-Type", h.contentType), T.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + Rt + "; q=0.01" : "") : h.accepts["*"]);
            for (p in h.headers) T.setRequestHeader(p, h.headers[p]);
            if (h.beforeSend && (!1 === h.beforeSend.call(g, T, h) || u)) return T.abort();
            if (S = "abort", y.add(h.complete), T.done(h.success), T.fail(h.error), o = K($t, h, n, T)) {
                if (T.readyState = 1, d && m.trigger("ajaxSend", [T, h]), u) return T;
                h.async && h.timeout > 0 && (l = e.setTimeout(function() {
                    T.abort("timeout")
                }, h.timeout));
                try {
                    u = !1, o.send(b, i)
                } catch (e) {
                    if (u) throw e;
                    i(-1, e)
                }
            } else i(-1, "No Transport");
            return T
        },
        getJSON: function(e, t, n) {
            return ge.get(e, t, n, "json")
        },
        getScript: function(e, t) {
            return ge.get(e, void 0, t, "script")
        }
    }), ge.each(["get", "post"], function(e, t) {
        ge[t] = function(e, n, i, o) {
            return ge.isFunction(n) && (o = o || i, i = n, n = void 0), ge.ajax(ge.extend({
                url: e,
                type: t,
                dataType: o,
                data: n,
                success: i
            }, ge.isPlainObject(e) && e))
        }
    }), ge._evalUrl = function(e) {
        return ge.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            throws: !0
        })
    }, ge.fn.extend({
        wrapAll: function(e) {
            var t;
            return this[0] && (ge.isFunction(e) && (e = e.call(this[0])), t = ge(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e
            }).append(this)), this
        },
        wrapInner: function(e) {
            return ge.isFunction(e) ? this.each(function(t) {
                ge(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = ge(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = ge.isFunction(e);
            return this.each(function(n) {
                ge(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function(e) {
            return this.parent(e).not("body").each(function() {
                ge(this).replaceWith(this.childNodes)
            }), this
        }
    }), ge.expr.pseudos.hidden = function(e) {
        return !ge.expr.pseudos.visible(e)
    }, ge.expr.pseudos.visible = function(e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, ge.ajaxSettings.xhr = function() {
        try {
            return new e.XMLHttpRequest
        } catch (e) {}
    };
    var Mt = {
            0: 200,
            1223: 204
        },
        Wt = ge.ajaxSettings.xhr();
    fe.cors = !!Wt && "withCredentials" in Wt, fe.ajax = Wt = !!Wt, ge.ajaxTransport(function(t) {
        var n, i;
        if (fe.cors || Wt && !t.crossDomain) return {
            send: function(o, r) {
                var s, a = t.xhr();
                if (a.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
                    for (s in t.xhrFields) a[s] = t.xhrFields[s];
                t.mimeType && a.overrideMimeType && a.overrideMimeType(t.mimeType), t.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                for (s in o) a.setRequestHeader(s, o[s]);
                n = function(e) {
                    return function() {
                        n && (n = i = a.onload = a.onerror = a.onabort = a.onreadystatechange = null, "abort" === e ? a.abort() : "error" === e ? "number" != typeof a.status ? r(0, "error") : r(a.status, a.statusText) : r(Mt[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                            binary: a.response
                        } : {
                            text: a.responseText
                        }, a.getAllResponseHeaders()))
                    }
                }, a.onload = n(), i = a.onerror = n("error"), void 0 !== a.onabort ? a.onabort = i : a.onreadystatechange = function() {
                    4 === a.readyState && e.setTimeout(function() {
                        n && i()
                    })
                }, n = n("abort");
                try {
                    a.send(t.hasContent && t.data || null)
                } catch (e) {
                    if (n) throw e
                }
            },
            abort: function() {
                n && n()
            }
        }
    }), ge.ajaxPrefilter(function(e) {
        e.crossDomain && (e.contents.script = !1)
    }), ge.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(e) {
                return ge.globalEval(e), e
            }
        }
    }), ge.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), ge.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var t, n;
            return {
                send: function(i, o) {
                    t = ge("<script>").prop({
                        charset: e.scriptCharset,
                        src: e.url
                    }).on("load error", n = function(e) {
                        t.remove(), n = null, e && o("error" === e.type ? 404 : 200, e.type)
                    }), ne.head.appendChild(t[0])
                },
                abort: function() {
                    n && n()
                }
            }
        }
    });
    var zt = [],
        Vt = /(=)\?(?=&|$)|\?\?/;
    ge.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = zt.pop() || ge.expando + "_" + At++;
            return this[e] = !0, e
        }
    }), ge.ajaxPrefilter("json jsonp", function(t, n, i) {
        var o, r, s, a = !1 !== t.jsonp && (Vt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Vt.test(t.data) && "data");
        if (a || "jsonp" === t.dataTypes[0]) return o = t.jsonpCallback = ge.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(Vt, "$1" + o) : !1 !== t.jsonp && (t.url += (It.test(t.url) ? "&" : "?") + t.jsonp + "=" + o), t.converters["script json"] = function() {
            return s || ge.error(o + " was not called"), s[0]
        }, t.dataTypes[0] = "json", r = e[o], e[o] = function() {
            s = arguments
        }, i.always(function() {
            void 0 === r ? ge(e).removeProp(o) : e[o] = r, t[o] && (t.jsonpCallback = n.jsonpCallback, zt.push(o)), s && ge.isFunction(r) && r(s[0]), s = r = void 0
        }), "script"
    }), fe.createHTMLDocument = function() {
        var e = ne.implementation.createHTMLDocument("").body;
        return e.innerHTML = "<form></form><form></form>", 2 === e.childNodes.length
    }(), ge.parseHTML = function(e, t, n) {
        if ("string" != typeof e) return [];
        "boolean" == typeof t && (n = t, t = !1);
        var i, o, r;
        return t || (fe.createHTMLDocument ? (t = ne.implementation.createHTMLDocument(""), i = t.createElement("base"), i.href = ne.location.href, t.head.appendChild(i)) : t = ne), o = Ce.exec(e), r = !n && [], o ? [t.createElement(o[1])] : (o = x([e], t, r), r && r.length && ge(r).remove(), ge.merge([], o.childNodes))
    }, ge.fn.load = function(e, t, n) {
        var i, o, r, s = this,
            a = e.indexOf(" ");
        return a > -1 && (i = X(e.slice(a)), e = e.slice(0, a)), ge.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), s.length > 0 && ge.ajax({
            url: e,
            type: o || "GET",
            dataType: "html",
            data: t
        }).done(function(e) {
            r = arguments, s.html(i ? ge("<div>").append(ge.parseHTML(e)).find(i) : e)
        }).always(n && function(e, t) {
            s.each(function() {
                n.apply(this, r || [e.responseText, t, e])
            })
        }), this
    }, ge.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        ge.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), ge.expr.pseudos.animated = function(e) {
        return ge.grep(ge.timers, function(t) {
            return e === t.elem
        }).length
    }, ge.offset = {
        setOffset: function(e, t, n) {
            var i, o, r, s, a, l, c, u = ge.css(e, "position"),
                d = ge(e),
                p = {};
            "static" === u && (e.style.position = "relative"), a = d.offset(), r = ge.css(e, "top"), l = ge.css(e, "left"), c = ("absolute" === u || "fixed" === u) && (r + l).indexOf("auto") > -1, c ? (i = d.position(), s = i.top, o = i.left) : (s = parseFloat(r) || 0, o = parseFloat(l) || 0), ge.isFunction(t) && (t = t.call(e, n, ge.extend({}, a))), null != t.top && (p.top = t.top - a.top + s), null != t.left && (p.left = t.left - a.left + o), "using" in t ? t.using.call(e, p) : d.css(p)
        }
    }, ge.fn.extend({
        offset: function(e) {
            if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                ge.offset.setOffset(this, e, t)
            });
            var t, n, i, o, r = this[0];
            return r ? r.getClientRects().length ? (i = r.getBoundingClientRect(), t = r.ownerDocument, n = t.documentElement, o = t.defaultView, {
                top: i.top + o.pageYOffset - n.clientTop,
                left: i.left + o.pageXOffset - n.clientLeft
            }) : {
                top: 0,
                left: 0
            } : void 0
        },
        position: function() {
            if (this[0]) {
                var e, t, n = this[0],
                    i = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === ge.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), o(e[0], "html") || (i = e.offset()), i = {
                    top: i.top + ge.css(e[0], "borderTopWidth", !0),
                    left: i.left + ge.css(e[0], "borderLeftWidth", !0)
                }), {
                    top: t.top - i.top - ge.css(n, "marginTop", !0),
                    left: t.left - i.left - ge.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent; e && "static" === ge.css(e, "position");) e = e.offsetParent;
                return e || Je
            })
        }
    }), ge.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, t) {
        var n = "pageYOffset" === t;
        ge.fn[e] = function(i) {
            return He(this, function(e, i, o) {
                var r;
                return ge.isWindow(e) ? r = e : 9 === e.nodeType && (r = e.defaultView), void 0 === o ? r ? r[t] : e[i] : void(r ? r.scrollTo(n ? r.pageXOffset : o, n ? o : r.pageYOffset) : e[i] = o)
            }, e, i, arguments.length)
        }
    }), ge.each(["top", "left"], function(e, t) {
        ge.cssHooks[t] = j(fe.pixelPosition, function(e, n) {
            if (n) return n = N(e, t), st.test(n) ? ge(e).position()[t] + "px" : n
        })
    }), ge.each({
        Height: "height",
        Width: "width"
    }, function(e, t) {
        ge.each({
            padding: "inner" + e,
            content: t,
            "": "outer" + e
        }, function(n, i) {
            ge.fn[i] = function(o, r) {
                var s = arguments.length && (n || "boolean" != typeof o),
                    a = n || (!0 === o || !0 === r ? "margin" : "border");
                return He(this, function(t, n, o) {
                    var r;
                    return ge.isWindow(t) ? 0 === i.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (r = t.documentElement, Math.max(t.body["scroll" + e], r["scroll" + e], t.body["offset" + e], r["offset" + e], r["client" + e])) : void 0 === o ? ge.css(t, n, a) : ge.style(t, n, o, a)
                }, t, s ? o : void 0, s)
            }
        })
    }), ge.fn.extend({
        bind: function(e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, n, i) {
            return this.on(t, e, n, i)
        },
        undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    }), ge.holdReady = function(e) {
        e ? ge.readyWait++ : ge.ready(!0)
    }, ge.isArray = Array.isArray, ge.parseJSON = JSON.parse, ge.nodeName = o, "function" == typeof define && define.amd && define("jquery", [], function() {
        return ge
    });
    var Ut = e.jQuery,
        Xt = e.$;
    return ge.noConflict = function(t) {
        return e.$ === ge && (e.$ = Xt), t && e.jQuery === ge && (e.jQuery = Ut), ge
    }, t || (e.jQuery = e.$ = ge), ge
}),
function(e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? module.exports = t() : e.Headroom = t()
}(this, function() {
    "use strict";

    function e(e) {
        this.callback = e, this.ticking = !1
    }

    function t(e) {
        return e && "undefined" != typeof window && (e === window || e.nodeType)
    }

    function n(e) {
        if (arguments.length <= 0) throw new Error("Missing arguments in extend function");
        var i, o, r = e || {};
        for (o = 1; o < arguments.length; o++) {
            var s = arguments[o] || {};
            for (i in s) "object" != typeof r[i] || t(r[i]) ? r[i] = r[i] || s[i] : r[i] = n(r[i], s[i])
        }
        return r
    }

    function i(e) {
        return e === Object(e) ? e : {
            down: e,
            up: e
        }
    }

    function o(e, t) {
        t = n(t, o.options), this.lastKnownScrollY = 0, this.elem = e, this.tolerance = i(t.tolerance), this.classes = t.classes, this.offset = t.offset, this.scroller = t.scroller, this.initialised = !1, this.onPin = t.onPin, this.onUnpin = t.onUnpin, this.onTop = t.onTop, this.onNotTop = t.onNotTop, this.onBottom = t.onBottom, this.onNotBottom = t.onNotBottom
    }
    var r = {
        bind: !! function() {}.bind,
        classList: "classList" in document.documentElement,
        rAF: !!(window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame)
    };
    return window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame, e.prototype = {
        constructor: e,
        update: function() {
            this.callback && this.callback(), this.ticking = !1
        },
        requestTick: function() {
            this.ticking || (requestAnimationFrame(this.rafCallback || (this.rafCallback = this.update.bind(this))), this.ticking = !0)
        },
        handleEvent: function() {
            this.requestTick()
        }
    }, o.prototype = {
        constructor: o,
        init: function() {
            if (o.cutsTheMustard) return this.debouncer = new e(this.update.bind(this)), this.elem.classList.add(this.classes.initial), setTimeout(this.attachEvent.bind(this), 100), this
        },
        destroy: function() {
            var e = this.classes;
            this.initialised = !1;
            for (var t in e) e.hasOwnProperty(t) && this.elem.classList.remove(e[t]);
            this.scroller.removeEventListener("scroll", this.debouncer, !1)
        },
        attachEvent: function() {
            this.initialised || (this.lastKnownScrollY = this.getScrollY(), this.initialised = !0, this.scroller.addEventListener("scroll", this.debouncer, !1), this.debouncer.handleEvent())
        },
        unpin: function() {
            var e = this.elem.classList,
                t = this.classes;
            !e.contains(t.pinned) && e.contains(t.unpinned) || (e.add(t.unpinned), e.remove(t.pinned), this.onUnpin && this.onUnpin.call(this))
        },
        pin: function() {
            var e = this.elem.classList,
                t = this.classes;
            e.contains(t.unpinned) && (e.remove(t.unpinned), e.add(t.pinned), this.onPin && this.onPin.call(this))
        },
        top: function() {
            var e = this.elem.classList,
                t = this.classes;
            e.contains(t.top) || (e.add(t.top), e.remove(t.notTop), this.onTop && this.onTop.call(this))
        },
        notTop: function() {
            var e = this.elem.classList,
                t = this.classes;
            e.contains(t.notTop) || (e.add(t.notTop), e.remove(t.top), this.onNotTop && this.onNotTop.call(this))
        },
        bottom: function() {
            var e = this.elem.classList,
                t = this.classes;
            e.contains(t.bottom) || (e.add(t.bottom), e.remove(t.notBottom), this.onBottom && this.onBottom.call(this))
        },
        notBottom: function() {
            var e = this.elem.classList,
                t = this.classes;
            e.contains(t.notBottom) || (e.add(t.notBottom), e.remove(t.bottom), this.onNotBottom && this.onNotBottom.call(this))
        },
        getScrollY: function() {
            return void 0 !== this.scroller.pageYOffset ? this.scroller.pageYOffset : void 0 !== this.scroller.scrollTop ? this.scroller.scrollTop : (document.documentElement || document.body.parentNode || document.body).scrollTop
        },
        getViewportHeight: function() {
            return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
        },
        getElementPhysicalHeight: function(e) {
            return Math.max(e.offsetHeight, e.clientHeight)
        },
        getScrollerPhysicalHeight: function() {
            return this.scroller === window || this.scroller === document.body ? this.getViewportHeight() : this.getElementPhysicalHeight(this.scroller)
        },
        getDocumentHeight: function() {
            var e = document.body,
                t = document.documentElement;
            return Math.max(e.scrollHeight, t.scrollHeight, e.offsetHeight, t.offsetHeight, e.clientHeight, t.clientHeight)
        },
        getElementHeight: function(e) {
            return Math.max(e.scrollHeight, e.offsetHeight, e.clientHeight)
        },
        getScrollerHeight: function() {
            return this.scroller === window || this.scroller === document.body ? this.getDocumentHeight() : this.getElementHeight(this.scroller)
        },
        isOutOfBounds: function(e) {
            var t = e < 0,
                n = e + this.getScrollerPhysicalHeight() > this.getScrollerHeight();
            return t || n
        },
        toleranceExceeded: function(e, t) {
            return Math.abs(e - this.lastKnownScrollY) >= this.tolerance[t]
        },
        shouldUnpin: function(e, t) {
            var n = e > this.lastKnownScrollY,
                i = e >= this.offset;
            return n && i && t
        },
        shouldPin: function(e, t) {
            var n = e < this.lastKnownScrollY,
                i = e <= this.offset;
            return n && t || i
        },
        update: function() {
            var e = this.getScrollY(),
                t = e > this.lastKnownScrollY ? "down" : "up",
                n = this.toleranceExceeded(e, t);
            this.isOutOfBounds(e) || (e <= this.offset ? this.top() : this.notTop(), e + this.getViewportHeight() >= this.getScrollerHeight() ? this.bottom() : this.notBottom(), this.shouldUnpin(e, n) ? this.unpin() : this.shouldPin(e, n) && this.pin(), this.lastKnownScrollY = e)
        }
    }, o.options = {
        tolerance: {
            up: 0,
            down: 0
        },
        offset: 0,
        scroller: window,
        classes: {
            pinned: "headroom--pinned",
            unpinned: "headroom--unpinned",
            top: "headroom--top",
            notTop: "headroom--not-top",
            bottom: "headroom--bottom",
            notBottom: "headroom--not-bottom",
            initial: "headroom"
        }
    }, o.cutsTheMustard = void 0 !== r && r.rAF && r.bind && r.classList, o
}),
function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof module && module.exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function(e) {
    var t = -1,
        n = -1,
        i = function(e) {
            return parseFloat(e) || 0
        },
        o = function(t) {
            var n = 1,
                o = e(t),
                r = null,
                s = [];
            return o.each(function() {
                var t = e(this),
                    n = t.offset().top - i(t.css("margin-top")),
                    o = s.length > 0 ? s[s.length - 1] : null;
                null === o ? s.push(t) : Math.floor(Math.abs(r - n)) <= 1 ? s[s.length - 1] = o.add(t) : s.push(t), r = n
            }), s
        },
        r = function(t) {
            var n = {
                byRow: !0,
                property: "height",
                target: null,
                remove: !1
            };
            return "object" == typeof t ? e.extend(n, t) : ("boolean" == typeof t ? n.byRow = t : "remove" === t && (n.remove = !0), n)
        },
        s = e.fn.matchHeight = function(t) {
            var n = r(t);
            if (n.remove) {
                var i = this;
                return this.css(n.property, ""), e.each(s._groups, function(e, t) {
                    t.elements = t.elements.not(i)
                }), this
            }
            return this.length <= 1 && !n.target ? this : (s._groups.push({
                elements: this,
                options: n
            }), s._apply(this, n), this)
        };
    s.version = "0.7.2", s._groups = [], s._throttle = 80, s._maintainScroll = !1, s._beforeUpdate = null, s._afterUpdate = null, s._rows = o, s._parse = i, s._parseOptions = r, s._apply = function(t, n) {
        var a = r(n),
            l = e(t),
            c = [l],
            u = e(window).scrollTop(),
            d = e("html").outerHeight(!0),
            p = l.parents().filter(":hidden");
        return p.each(function() {
            var t = e(this);
            t.data("style-cache", t.attr("style"))
        }), p.css("display", "block"), a.byRow && !a.target && (l.each(function() {
            var t = e(this),
                n = t.css("display");
            "inline-block" !== n && "flex" !== n && "inline-flex" !== n && (n = "block"), t.data("style-cache", t.attr("style")), t.css({
                display: n,
                "padding-top": "0",
                "padding-bottom": "0",
                "margin-top": "0",
                "margin-bottom": "0",
                "border-top-width": "0",
                "border-bottom-width": "0",
                height: "100px",
                overflow: "hidden"
            })
        }), c = o(l), l.each(function() {
            var t = e(this);
            t.attr("style", t.data("style-cache") || "")
        })), e.each(c, function(t, n) {
            var o = e(n),
                r = 0;
            if (a.target) r = a.target.outerHeight(!1);
            else {
                if (a.byRow && o.length <= 1) return void o.css(a.property, "");
                o.each(function() {
                    var t = e(this),
                        n = t.attr("style"),
                        i = t.css("display");
                    "inline-block" !== i && "flex" !== i && "inline-flex" !== i && (i = "block");
                    var o = {
                        display: i
                    };
                    o[a.property] = "", t.css(o), t.outerHeight(!1) > r && (r = t.outerHeight(!1)), n ? t.attr("style", n) : t.css("display", "")
                })
            }
            o.each(function() {
                var t = e(this),
                    n = 0;
                a.target && t.is(a.target) || ("border-box" !== t.css("box-sizing") && (n += i(t.css("border-top-width")) + i(t.css("border-bottom-width")), n += i(t.css("padding-top")) + i(t.css("padding-bottom"))), t.css(a.property, r - n + "px"))
            })
        }), p.each(function() {
            var t = e(this);
            t.attr("style", t.data("style-cache") || null)
        }), s._maintainScroll && e(window).scrollTop(u / d * e("html").outerHeight(!0)), this
    }, s._applyDataApi = function() {
        var t = {};
        e("[data-match-height], [data-mh]").each(function() {
            var n = e(this),
                i = n.attr("data-mh") || n.attr("data-match-height");
            t[i] = i in t ? t[i].add(n) : n
        }), e.each(t, function() {
            this.matchHeight(!0)
        })
    };
    var a = function(t) {
        s._beforeUpdate && s._beforeUpdate(t, s._groups), e.each(s._groups, function() {
            s._apply(this.elements, this.options)
        }), s._afterUpdate && s._afterUpdate(t, s._groups)
    };
    s._update = function(i, o) {
        if (o && "resize" === o.type) {
            var r = e(window).width();
            if (r === t) return;
            t = r
        }
        i ? -1 === n && (n = setTimeout(function() {
            a(o), n = -1
        }, s._throttle)) : a(o)
    }, e(s._applyDataApi);
    var l = e.fn.on ? "on" : "bind";
    e(window)[l]("load", function(e) {
        s._update(!1, e)
    }), e(window)[l]("resize orientationchange", function(e) {
        s._update(!0, e)
    })
}),
function(e) {
    "object" == typeof module && "object" == typeof module.exports ? e(require("jquery"), window, document) : e(jQuery, window, document)
}(function(e, t, n, i) {
    var o = [],
        r = function() {
            return o.length ? o[o.length - 1] : null
        },
        s = function() {
            var e, t = !1;
            for (e = o.length - 1; e >= 0; e--) o[e].$blocker && (o[e].$blocker.toggleClass("current", !t).toggleClass("behind", t), t = !0)
        };
    e.modal = function(t, n) {
        var i, s;
        if (this.$body = e("body"), this.options = e.extend({}, e.modal.defaults, n), this.options.doFade = !isNaN(parseInt(this.options.fadeDuration, 10)), this.$blocker = null,
            this.options.closeExisting)
            for (; e.modal.isActive();) e.modal.close();
        if (o.push(this), t.is("a"))
            if (s = t.attr("href"), this.anchor = t, /^#/.test(s)) {
                if (this.$elm = e(s), 1 !== this.$elm.length) return null;
                this.$body.append(this.$elm), this.open()
            } else this.$elm = e("<div>"), this.$body.append(this.$elm), i = function(e, t) {
                t.elm.remove()
            }, this.showSpinner(), t.trigger(e.modal.AJAX_SEND), e.get(s).done(function(n) {
                if (e.modal.isActive()) {
                    t.trigger(e.modal.AJAX_SUCCESS);
                    var o = r();
                    o.$elm.empty().append(n).on(e.modal.CLOSE, i), o.hideSpinner(), o.open(), t.trigger(e.modal.AJAX_COMPLETE)
                }
            }).fail(function() {
                t.trigger(e.modal.AJAX_FAIL), r().hideSpinner(), o.pop(), t.trigger(e.modal.AJAX_COMPLETE)
            });
        else this.$elm = t, this.anchor = t, this.$body.append(this.$elm), this.open()
    }, e.modal.prototype = {
        constructor: e.modal,
        open: function() {
            var t = this;
            this.block(), this.anchor.blur(), this.options.doFade ? setTimeout(function() {
                t.show()
            }, this.options.fadeDuration * this.options.fadeDelay) : this.show(), e(n).off("keydown.modal").on("keydown.modal", function(e) {
                var t = r();
                27 === e.which && t.options.escapeClose && t.close()
            }), this.options.clickClose && this.$blocker.click(function(t) {
                t.target === this && e.modal.close()
            })
        },
        close: function() {
            o.pop(), this.unblock(), this.hide(), e.modal.isActive() || e(n).off("keydown.modal")
        },
        block: function() {
            this.$elm.trigger(e.modal.BEFORE_BLOCK, [this._ctx()]), this.$body.css("overflow", "hidden"), this.$blocker = e('<div class="' + this.options.blockerClass + ' blocker current"></div>').appendTo(this.$body), s(), this.options.doFade && this.$blocker.css("opacity", 0).animate({
                opacity: 1
            }, this.options.fadeDuration), this.$elm.trigger(e.modal.BLOCK, [this._ctx()])
        },
        unblock: function(t) {
            !t && this.options.doFade ? this.$blocker.fadeOut(this.options.fadeDuration, this.unblock.bind(this, !0)) : (this.$blocker.children().appendTo(this.$body), this.$blocker.remove(), this.$blocker = null, s(), e.modal.isActive() || this.$body.css("overflow", ""))
        },
        show: function() {
            this.$elm.trigger(e.modal.BEFORE_OPEN, [this._ctx()]), this.options.showClose && (this.closeButton = e('<a href="#close-modal" rel="modal:close" class="close-modal ' + this.options.closeClass + '">' + this.options.closeText + "</a>"), this.$elm.append(this.closeButton)), this.$elm.addClass(this.options.modalClass).appendTo(this.$blocker), this.options.doFade ? this.$elm.css({
                opacity: 0,
                display: "inline-block"
            }).animate({
                opacity: 1
            }, this.options.fadeDuration) : this.$elm.css("display", "inline-block"), this.$elm.trigger(e.modal.OPEN, [this._ctx()])
        },
        hide: function() {
            this.$elm.trigger(e.modal.BEFORE_CLOSE, [this._ctx()]), this.closeButton && this.closeButton.remove();
            var t = this;
            this.options.doFade ? this.$elm.fadeOut(this.options.fadeDuration, function() {
                t.$elm.trigger(e.modal.AFTER_CLOSE, [t._ctx()])
            }) : this.$elm.hide(0, function() {
                t.$elm.trigger(e.modal.AFTER_CLOSE, [t._ctx()])
            }), this.$elm.trigger(e.modal.CLOSE, [this._ctx()])
        },
        showSpinner: function() {
            this.options.showSpinner && (this.spinner = this.spinner || e('<div class="' + this.options.modalClass + '-spinner"></div>').append(this.options.spinnerHtml), this.$body.append(this.spinner), this.spinner.show())
        },
        hideSpinner: function() {
            this.spinner && this.spinner.remove()
        },
        _ctx: function() {
            return {
                elm: this.$elm,
                $elm: this.$elm,
                $blocker: this.$blocker,
                options: this.options
            }
        }
    }, e.modal.close = function(t) {
        if (e.modal.isActive()) {
            t && t.preventDefault();
            var n = r();
            return n.close(), n.$elm
        }
    }, e.modal.isActive = function() {
        return o.length > 0
    }, e.modal.getCurrent = r, e.modal.defaults = {
        closeExisting: !0,
        escapeClose: !0,
        clickClose: !0,
        closeText: "Close",
        closeClass: "",
        modalClass: "modal",
        blockerClass: "jquery-modal",
        spinnerHtml: '<div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div>',
        showSpinner: !0,
        showClose: !0,
        fadeDuration: null,
        fadeDelay: 1
    }, e.modal.BEFORE_BLOCK = "modal:before-block", e.modal.BLOCK = "modal:block", e.modal.BEFORE_OPEN = "modal:before-open", e.modal.OPEN = "modal:open", e.modal.BEFORE_CLOSE = "modal:before-close", e.modal.CLOSE = "modal:close", e.modal.AFTER_CLOSE = "modal:after-close", e.modal.AJAX_SEND = "modal:ajax:send", e.modal.AJAX_SUCCESS = "modal:ajax:success", e.modal.AJAX_FAIL = "modal:ajax:fail", e.modal.AJAX_COMPLETE = "modal:ajax:complete", e.fn.modal = function(t) {
        return 1 === this.length && new e.modal(this, t), this
    }, e(n).on("click.modal", 'a[rel~="modal:close"]', e.modal.close), e(n).on("click.modal", 'a[rel~="modal:open"]', function(t) {
        t.preventDefault(), e(this).modal()
    })
}),
function(e) {
    "use strict";

    function t(e) {
        return (e || "").toLowerCase()
    }
    e.fn.cycle = function(n) {
        var i;
        return 0 !== this.length || e.isReady ? this.each(function() {
            var i, o, r, s, a = e(this),
                l = e.fn.cycle.log;
            if (!a.data("cycle.opts")) {
                (!1 === a.data("cycle-log") || n && !1 === n.log || o && !1 === o.log) && (l = e.noop), l("--c2 init--"), i = a.data();
                for (var c in i) i.hasOwnProperty(c) && /^cycle[A-Z]+/.test(c) && (s = i[c], r = c.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, t), l(r + ":", s, "(" + typeof s + ")"), i[r] = s);
                o = e.extend({}, e.fn.cycle.defaults, i, n || {}), o.timeoutId = 0, o.paused = o.paused || !1, o.container = a, o._maxZ = o.maxZ, o.API = e.extend({
                    _container: a
                }, e.fn.cycle.API), o.API.log = l, o.API.trigger = function(e, t) {
                    return o.container.trigger(e, t), o.API
                }, a.data("cycle.opts", o), a.data("cycle.API", o.API), o.API.trigger("cycle-bootstrap", [o, o.API]), o.API.addInitialSlides(), o.API.preInitSlideshow(), o.slides.length && o.API.initSlideshow()
            }
        }) : (i = {
            s: this.selector,
            c: this.context
        }, e.fn.cycle.log("requeuing slideshow (dom not ready)"), e(function() {
            e(i.s, i.c).cycle(n)
        }), this)
    }, e.fn.cycle.API = {
        opts: function() {
            return this._container.data("cycle.opts")
        },
        addInitialSlides: function() {
            var t = this.opts(),
                n = t.slides;
            t.slideCount = 0, t.slides = e(), n = n.jquery ? n : t.container.find(n), t.random && n.sort(function() {
                return Math.random() - .5
            }), t.API.add(n)
        },
        preInitSlideshow: function() {
            var t = this.opts();
            t.API.trigger("cycle-pre-initialize", [t]);
            var n = e.fn.cycle.transitions[t.fx];
            n && e.isFunction(n.preInit) && n.preInit(t), t._preInitialized = !0
        },
        postInitSlideshow: function() {
            var t = this.opts();
            t.API.trigger("cycle-post-initialize", [t]);
            var n = e.fn.cycle.transitions[t.fx];
            n && e.isFunction(n.postInit) && n.postInit(t)
        },
        initSlideshow: function() {
            var t, n = this.opts(),
                i = n.container;
            n.API.calcFirstSlide(), "static" == n.container.css("position") && n.container.css("position", "relative"), e(n.slides[n.currSlide]).css({
                opacity: 1,
                display: "block",
                visibility: "visible"
            }), n.API.stackSlides(n.slides[n.currSlide], n.slides[n.nextSlide], !n.reverse), n.pauseOnHover && (!0 !== n.pauseOnHover && (i = e(n.pauseOnHover)), i.hover(function() {
                n.API.pause(!0)
            }, function() {
                n.API.resume(!0)
            })), n.timeout && (t = n.API.getSlideOpts(n.currSlide), n.API.queueTransition(t, t.timeout + n.delay)), n._initialized = !0, n.API.updateView(!0), n.API.trigger("cycle-initialized", [n]), n.API.postInitSlideshow()
        },
        pause: function(t) {
            var n = this.opts(),
                i = n.API.getSlideOpts(),
                o = n.hoverPaused || n.paused;
            t ? n.hoverPaused = !0 : n.paused = !0, o || (n.container.addClass("cycle-paused"), n.API.trigger("cycle-paused", [n]).log("cycle-paused"), i.timeout && (clearTimeout(n.timeoutId), n.timeoutId = 0, n._remainingTimeout -= e.now() - n._lastQueue, (n._remainingTimeout < 0 || isNaN(n._remainingTimeout)) && (n._remainingTimeout = void 0)))
        },
        resume: function(e) {
            var t = this.opts(),
                n = !t.hoverPaused && !t.paused;
            e ? t.hoverPaused = !1 : t.paused = !1, n || (t.container.removeClass("cycle-paused"), 0 === t.slides.filter(":animated").length && t.API.queueTransition(t.API.getSlideOpts(), t._remainingTimeout), t.API.trigger("cycle-resumed", [t, t._remainingTimeout]).log("cycle-resumed"))
        },
        add: function(t, n) {
            var i, o = this.opts(),
                r = o.slideCount,
                s = !1;
            "string" == e.type(t) && (t = e.trim(t)), e(t).each(function() {
                var t, i = e(this);
                n ? o.container.prepend(i) : o.container.append(i), o.slideCount++, t = o.API.buildSlideOpts(i), o.slides = n ? e(i).add(o.slides) : o.slides.add(i), o.API.initSlide(t, i, --o._maxZ), i.data("cycle.opts", t), o.API.trigger("cycle-slide-added", [o, t, i])
            }), o.API.updateView(!0), (s = o._preInitialized && 2 > r && o.slideCount >= 1) && (o._initialized ? o.timeout && (i = o.slides.length, o.nextSlide = o.reverse ? i - 1 : 1, o.timeoutId || o.API.queueTransition(o)) : o.API.initSlideshow())
        },
        calcFirstSlide: function() {
            var e, t = this.opts();
            e = parseInt(t.startingSlide || 0, 10), (e >= t.slides.length || 0 > e) && (e = 0), t.currSlide = e, t.reverse ? (t.nextSlide = e - 1, t.nextSlide < 0 && (t.nextSlide = t.slides.length - 1)) : (t.nextSlide = e + 1, t.nextSlide == t.slides.length && (t.nextSlide = 0))
        },
        calcNextSlide: function() {
            var e, t = this.opts();
            t.reverse ? (e = t.nextSlide - 1 < 0, t.nextSlide = e ? t.slideCount - 1 : t.nextSlide - 1, t.currSlide = e ? 0 : t.nextSlide + 1) : (e = t.nextSlide + 1 == t.slides.length, t.nextSlide = e ? 0 : t.nextSlide + 1, t.currSlide = e ? t.slides.length - 1 : t.nextSlide - 1)
        },
        calcTx: function(t, n) {
            var i, o = t;
            return o._tempFx ? i = e.fn.cycle.transitions[o._tempFx] : n && o.manualFx && (i = e.fn.cycle.transitions[o.manualFx]), i || (i = e.fn.cycle.transitions[o.fx]), o._tempFx = null, this.opts()._tempFx = null, i || (i = e.fn.cycle.transitions.fade, o.API.log('Transition "' + o.fx + '" not found.  Using fade.')), i
        },
        prepareTx: function(e, t) {
            var n, i, o, r, s, a = this.opts();
            return a.slideCount < 2 ? void(a.timeoutId = 0) : (!e || a.busy && !a.manualTrump || (a.API.stopTransition(), a.busy = !1, clearTimeout(a.timeoutId), a.timeoutId = 0), void(a.busy || (0 !== a.timeoutId || e) && (i = a.slides[a.currSlide], o = a.slides[a.nextSlide], r = a.API.getSlideOpts(a.nextSlide), s = a.API.calcTx(r, e), a._tx = s, e && void 0 !== r.manualSpeed && (r.speed = r.manualSpeed), a.nextSlide != a.currSlide && (e || !a.paused && !a.hoverPaused && a.timeout) ? (a.API.trigger("cycle-before", [r, i, o, t]), s.before && s.before(r, i, o, t), n = function() {
                a.busy = !1, a.container.data("cycle.opts") && (s.after && s.after(r, i, o, t), a.API.trigger("cycle-after", [r, i, o, t]), a.API.queueTransition(r), a.API.updateView(!0))
            }, a.busy = !0, s.transition ? s.transition(r, i, o, t, n) : a.API.doTransition(r, i, o, t, n), a.API.calcNextSlide(), a.API.updateView()) : a.API.queueTransition(r))))
        },
        doTransition: function(t, n, i, o, r) {
            var s = t,
                a = e(n),
                l = e(i),
                c = function() {
                    l.animate(s.animIn || {
                        opacity: 1
                    }, s.speed, s.easeIn || s.easing, r)
                };
            l.css(s.cssBefore || {}), a.animate(s.animOut || {}, s.speed, s.easeOut || s.easing, function() {
                a.css(s.cssAfter || {}), s.sync || c()
            }), s.sync && c()
        },
        queueTransition: function(t, n) {
            var i = this.opts(),
                o = void 0 !== n ? n : t.timeout;
            return 0 === i.nextSlide && 0 == --i.loop ? (i.API.log("terminating; loop=0"), i.timeout = 0, o ? setTimeout(function() {
                i.API.trigger("cycle-finished", [i])
            }, o) : i.API.trigger("cycle-finished", [i]), void(i.nextSlide = i.currSlide)) : void 0 !== i.continueAuto && (!1 === i.continueAuto || e.isFunction(i.continueAuto) && !1 === i.continueAuto()) ? (i.API.log("terminating automatic transitions"), i.timeout = 0, void(i.timeoutId && clearTimeout(i.timeoutId))) : void(o && (i._lastQueue = e.now(), void 0 === n && (i._remainingTimeout = t.timeout), i.paused || i.hoverPaused || (i.timeoutId = setTimeout(function() {
                i.API.prepareTx(!1, !i.reverse)
            }, o))))
        },
        stopTransition: function() {
            var e = this.opts();
            e.slides.filter(":animated").length && (e.slides.stop(!1, !0), e.API.trigger("cycle-transition-stopped", [e])), e._tx && e._tx.stopTransition && e._tx.stopTransition(e)
        },
        advanceSlide: function(e) {
            var t = this.opts();
            return clearTimeout(t.timeoutId), t.timeoutId = 0, t.nextSlide = t.currSlide + e, t.nextSlide < 0 ? t.nextSlide = t.slides.length - 1 : t.nextSlide >= t.slides.length && (t.nextSlide = 0), t.API.prepareTx(!0, e >= 0), !1
        },
        buildSlideOpts: function(n) {
            var i, o, r = this.opts(),
                s = n.data() || {};
            for (var a in s) s.hasOwnProperty(a) && /^cycle[A-Z]+/.test(a) && (i = s[a], o = a.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, t), r.API.log("[" + (r.slideCount - 1) + "]", o + ":", i, "(" + typeof i + ")"), s[o] = i);
            s = e.extend({}, e.fn.cycle.defaults, r, s), s.slideNum = r.slideCount;
            try {
                delete s.API, delete s.slideCount, delete s.currSlide, delete s.nextSlide, delete s.slides
            } catch (e) {}
            return s
        },
        getSlideOpts: function(t) {
            var n = this.opts();
            void 0 === t && (t = n.currSlide);
            var i = n.slides[t],
                o = e(i).data("cycle.opts");
            return e.extend({}, n, o)
        },
        initSlide: function(t, n, i) {
            var o = this.opts();
            n.css(t.slideCss || {}), i > 0 && n.css("zIndex", i), isNaN(t.speed) && (t.speed = e.fx.speeds[t.speed] || e.fx.speeds._default), t.sync || (t.speed = t.speed / 2), n.addClass(o.slideClass)
        },
        updateView: function(e, t) {
            var n = this.opts();
            if (n._initialized) {
                var i = n.API.getSlideOpts(),
                    o = n.slides[n.currSlide];
                !e && !0 !== t && (n.API.trigger("cycle-update-view-before", [n, i, o]), n.updateView < 0) || (n.slideActiveClass && n.slides.removeClass(n.slideActiveClass).eq(n.currSlide).addClass(n.slideActiveClass), e && n.hideNonActive && n.slides.filter(":not(." + n.slideActiveClass + ")").css("visibility", "hidden"), 0 === n.updateView && setTimeout(function() {
                    n.API.trigger("cycle-update-view", [n, i, o, e])
                }, i.speed / (n.sync ? 2 : 1)), 0 !== n.updateView && n.API.trigger("cycle-update-view", [n, i, o, e]), e && n.API.trigger("cycle-update-view-after", [n, i, o]))
            }
        },
        getComponent: function(t) {
            var n = this.opts(),
                i = n[t];
            return "string" == typeof i ? /^\s*[\>|\+|~]/.test(i) ? n.container.find(i) : e(i) : i.jquery ? i : e(i)
        },
        stackSlides: function(t, n, i) {
            var o = this.opts();
            t || (t = o.slides[o.currSlide], n = o.slides[o.nextSlide], i = !o.reverse), e(t).css("zIndex", o.maxZ);
            var r, s = o.maxZ - 2,
                a = o.slideCount;
            if (i) {
                for (r = o.currSlide + 1; a > r; r++) e(o.slides[r]).css("zIndex", s--);
                for (r = 0; r < o.currSlide; r++) e(o.slides[r]).css("zIndex", s--)
            } else {
                for (r = o.currSlide - 1; r >= 0; r--) e(o.slides[r]).css("zIndex", s--);
                for (r = a - 1; r > o.currSlide; r--) e(o.slides[r]).css("zIndex", s--)
            }
            e(n).css("zIndex", o.maxZ - 1)
        },
        getSlideIndex: function(e) {
            return this.opts().slides.index(e)
        }
    }, e.fn.cycle.log = function() {
        window.console && console.log && console.log("[cycle2] " + Array.prototype.join.call(arguments, " "))
    }, e.fn.cycle.version = function() {
        return "Cycle2: 2.1.6"
    }, e.fn.cycle.transitions = {
        custom: {},
        none: {
            before: function(e, t, n, i) {
                e.API.stackSlides(n, t, i), e.cssBefore = {
                    opacity: 1,
                    visibility: "visible",
                    display: "block"
                }
            }
        },
        fade: {
            before: function(t, n, i, o) {
                var r = t.API.getSlideOpts(t.nextSlide).slideCss || {};
                t.API.stackSlides(n, i, o), t.cssBefore = e.extend(r, {
                    opacity: 0,
                    visibility: "visible",
                    display: "block"
                }), t.animIn = {
                    opacity: 1
                }, t.animOut = {
                    opacity: 0
                }
            }
        },
        fadeout: {
            before: function(t, n, i, o) {
                var r = t.API.getSlideOpts(t.nextSlide).slideCss || {};
                t.API.stackSlides(n, i, o), t.cssBefore = e.extend(r, {
                    opacity: 1,
                    visibility: "visible",
                    display: "block"
                }), t.animOut = {
                    opacity: 0
                }
            }
        },
        scrollHorz: {
            before: function(e, t, n, i) {
                e.API.stackSlides(t, n, i);
                var o = e.container.css("overflow", "hidden").width();
                e.cssBefore = {
                    left: i ? o : -o,
                    top: 0,
                    opacity: 1,
                    visibility: "visible",
                    display: "block"
                }, e.cssAfter = {
                    zIndex: e._maxZ - 2,
                    left: 0
                }, e.animIn = {
                    left: 0
                }, e.animOut = {
                    left: i ? -o : o
                }
            }
        }
    }, e.fn.cycle.defaults = {
        allowWrap: !0,
        autoSelector: ".cycle-slideshow[data-cycle-auto-init!=false]",
        delay: 0,
        easing: null,
        fx: "fade",
        hideNonActive: !0,
        loop: 0,
        manualFx: void 0,
        manualSpeed: void 0,
        manualTrump: !0,
        maxZ: 100,
        pauseOnHover: !1,
        reverse: !1,
        slideActiveClass: "cycle-slide-active",
        slideClass: "cycle-slide",
        slideCss: {
            position: "absolute",
            top: 0,
            left: 0
        },
        slides: "> img",
        speed: 500,
        startingSlide: 0,
        sync: !0,
        timeout: 4e3,
        updateView: 0
    }, e(document).ready(function() {
        e(e.fn.cycle.defaults.autoSelector).cycle()
    })
}(jQuery),
function(e) {
    "use strict";

    function t(t, i) {
        var o, r, s, a = i.autoHeight;
        if ("container" == a) r = e(i.slides[i.currSlide]).outerHeight(), i.container.height(r);
        else if (i._autoHeightRatio) i.container.height(i.container.width() / i._autoHeightRatio);
        else if ("calc" === a || "number" == e.type(a) && a >= 0) {
            if ((s = "calc" === a ? n(t, i) : a >= i.slides.length ? 0 : a) == i._sentinelIndex) return;
            i._sentinelIndex = s, i._sentinel && i._sentinel.remove(), o = e(i.slides[s].cloneNode(!0)), o.removeAttr("id name rel").find("[id],[name],[rel]").removeAttr("id name rel"), o.css({
                position: "static",
                visibility: "hidden",
                display: "block"
            }).prependTo(i.container).addClass("cycle-sentinel cycle-slide").removeClass("cycle-slide-active"), o.find("*").css("visibility", "hidden"), i._sentinel = o
        }
    }

    function n(t, n) {
        var i = 0,
            o = -1;
        return n.slides.each(function(t) {
            var n = e(this).height();
            n > o && (o = n, i = t)
        }), i
    }

    function i(t, n, i, o) {
        var r = e(o).outerHeight();
        n.container.animate({
            height: r
        }, n.autoHeightSpeed, n.autoHeightEasing)
    }

    function o(n, r) {
        r._autoHeightOnResize && (e(window).off("resize orientationchange", r._autoHeightOnResize), r._autoHeightOnResize = null), r.container.off("cycle-slide-added cycle-slide-removed", t), r.container.off("cycle-destroyed", o), r.container.off("cycle-before", i), r._sentinel && (r._sentinel.remove(), r._sentinel = null)
    }
    e.extend(e.fn.cycle.defaults, {
        autoHeight: 0,
        autoHeightSpeed: 250,
        autoHeightEasing: null
    }), e(document).on("cycle-initialized", function(n, r) {
        function s() {
            t(n, r)
        }
        var a, l = r.autoHeight,
            c = e.type(l),
            u = null;
        ("string" === c || "number" === c) && (r.container.on("cycle-slide-added cycle-slide-removed", t), r.container.on("cycle-destroyed", o), "container" == l ? r.container.on("cycle-before", i) : "string" === c && /\d+\:\d+/.test(l) && (a = l.match(/(\d+)\:(\d+)/), a = a[1] / a[2], r._autoHeightRatio = a), "number" !== c && (r._autoHeightOnResize = function() {
            clearTimeout(u), u = setTimeout(s, 50)
        }, e(window).on("resize orientationchange", r._autoHeightOnResize)), setTimeout(s, 30))
    })
}(jQuery),
function(e) {
    "use strict";
    e.extend(e.fn.cycle.defaults, {
        caption: "> .cycle-caption",
        captionTemplate: "{{slideNum}} / {{slideCount}}",
        overlay: "> .cycle-overlay",
        overlayTemplate: "<div>{{title}}</div><div>{{desc}}</div>",
        captionModule: "caption"
    }), e(document).on("cycle-update-view", function(t, n, i, o) {
        "caption" === n.captionModule && e.each(["caption", "overlay"], function() {
            var e = this,
                t = i[e + "Template"],
                r = n.API.getComponent(e);
            r.length && t ? (r.html(n.API.tmpl(t, i, n, o)), r.show()) : r.hide()
        })
    }), e(document).on("cycle-destroyed", function(t, n) {
        var i;
        e.each(["caption", "overlay"], function() {
            var e = this,
                t = n[e + "Template"];
            n[e] && t && (i = n.API.getComponent("caption"), i.empty())
        })
    })
}(jQuery),
function(e) {
    "use strict";
    var t = e.fn.cycle;
    e.fn.cycle = function(n) {
        var i, o, r, s = e.makeArray(arguments);
        return "number" == e.type(n) ? this.cycle("goto", n) : "string" == e.type(n) ? this.each(function() {
            var a;
            return i = n, r = e(this).data("cycle.opts"), void 0 === r ? void t.log('slideshow must be initialized before sending commands; "' + i + '" ignored') : (i = "goto" == i ? "jump" : i, o = r.API[i], e.isFunction(o) ? (a = e.makeArray(s), a.shift(), o.apply(r.API, a)) : void t.log("unknown command: ", i))
        }) : t.apply(this, arguments)
    }, e.extend(e.fn.cycle, t), e.extend(t.API, {
        next: function() {
            var e = this.opts();
            if (!e.busy || e.manualTrump) {
                var t = e.reverse ? -1 : 1;
                !1 === e.allowWrap && e.currSlide + t >= e.slideCount || (e.API.advanceSlide(t), e.API.trigger("cycle-next", [e]).log("cycle-next"))
            }
        },
        prev: function() {
            var e = this.opts();
            if (!e.busy || e.manualTrump) {
                var t = e.reverse ? 1 : -1;
                !1 === e.allowWrap && e.currSlide + t < 0 || (e.API.advanceSlide(t), e.API.trigger("cycle-prev", [e]).log("cycle-prev"))
            }
        },
        destroy: function() {
            this.stop();
            var t = this.opts(),
                n = e.isFunction(e._data) ? e._data : e.noop;
            clearTimeout(t.timeoutId), t.timeoutId = 0, t.API.stop(), t.API.trigger("cycle-destroyed", [t]).log("cycle-destroyed"), t.container.removeData(), n(t.container[0], "parsedAttrs", !1), t.retainStylesOnDestroy || (t.container.removeAttr("style"), t.slides.removeAttr("style"), t.slides.removeClass(t.slideActiveClass)), t.slides.each(function() {
                var i = e(this);
                i.removeData(), i.removeClass(t.slideClass), n(this, "parsedAttrs", !1)
            })
        },
        jump: function(e, t) {
            var n, i = this.opts();
            if (!i.busy || i.manualTrump) {
                var o = parseInt(e, 10);
                if (isNaN(o) || 0 > o || o >= i.slides.length) return void i.API.log("goto: invalid slide index: " + o);
                if (o == i.currSlide) return void i.API.log("goto: skipping, already on slide", o);
                i.nextSlide = o, clearTimeout(i.timeoutId), i.timeoutId = 0, i.API.log("goto: ", o, " (zero-index)"), n = i.currSlide < i.nextSlide, i._tempFx = t, i.API.prepareTx(!0, n)
            }
        },
        stop: function() {
            var t = this.opts(),
                n = t.container;
            clearTimeout(t.timeoutId), t.timeoutId = 0, t.API.stopTransition(), t.pauseOnHover && (!0 !== t.pauseOnHover && (n = e(t.pauseOnHover)), n.off("mouseenter mouseleave")), t.API.trigger("cycle-stopped", [t]).log("cycle-stopped")
        },
        reinit: function() {
            var e = this.opts();
            e.API.destroy(), e.container.cycle()
        },
        remove: function(t) {
            for (var n, i, o = this.opts(), r = [], s = 1, a = 0; a < o.slides.length; a++) n = o.slides[a], a == t ? i = n : (r.push(n), e(n).data("cycle.opts").slideNum = s, s++);
            i && (o.slides = e(r), o.slideCount--, e(i).remove(), t == o.currSlide ? o.API.advanceSlide(1) : t < o.currSlide ? o.currSlide-- : o.currSlide++, o.API.trigger("cycle-slide-removed", [o, t, i]).log("cycle-slide-removed"), o.API.updateView())
        }
    }), e(document).on("click.cycle", "[data-cycle-cmd]", function(t) {
        t.preventDefault();
        var n = e(this),
            i = n.data("cycle-cmd"),
            o = n.data("cycle-context") || ".cycle-slideshow";
        e(o).cycle(i, n.data("cycle-arg"))
    })
}(jQuery),
function(e) {
    "use strict";

    function t(t, n) {
        var i;
        return t._hashFence ? void(t._hashFence = !1) : (i = window.location.hash.substring(1), void t.slides.each(function(o) {
            if (e(this).data("cycle-hash") == i) {
                if (!0 === n) t.startingSlide = o;
                else {
                    var r = t.currSlide < o;
                    t.nextSlide = o, t.API.prepareTx(!0, r)
                }
                return !1
            }
        }))
    }
    e(document).on("cycle-pre-initialize", function(n, i) {
        t(i, !0), i._onHashChange = function() {
            t(i, !1)
        }, e(window).on("hashchange", i._onHashChange)
    }), e(document).on("cycle-update-view", function(e, t, n) {
        n.hash && "#" + n.hash != window.location.hash && (t._hashFence = !0, window.location.hash = n.hash)
    }), e(document).on("cycle-destroyed", function(t, n) {
        n._onHashChange && e(window).off("hashchange", n._onHashChange)
    })
}(jQuery),
function(e) {
    "use strict";
    e.extend(e.fn.cycle.defaults, {
        loader: !1
    }), e(document).on("cycle-bootstrap", function(t, n) {
        function i(t, i) {
            function r(t) {
                var r;
                "wait" == n.loader ? (a.push(t), 0 === c && (a.sort(s), o.apply(n.API, [a, i]), n.container.removeClass("cycle-loading"))) : (r = e(n.slides[n.currSlide]), o.apply(n.API, [t, i]), r.show(), n.container.removeClass("cycle-loading"))
            }

            function s(e, t) {
                return e.data("index") - t.data("index")
            }
            var a = [];
            if ("string" == e.type(t)) t = e.trim(t);
            else if ("array" === e.type(t))
                for (var l = 0; l < t.length; l++) t[l] = e(t[l])[0];
            t = e(t);
            var c = t.length;
            c && (t.css("visibility", "hidden").appendTo("body").each(function(t) {
                function s() {
                    0 == --l && (--c, r(u))
                }
                var l = 0,
                    u = e(this),
                    d = u.is("img") ? u : u.find("img");
                return u.data("index", t), d = d.filter(":not(.cycle-loader-ignore)").filter(':not([src=""])'), d.length ? (l = d.length, void d.each(function() {
                    this.complete ? s() : e(this).load(function() {
                        s()
                    }).on("error", function() {
                        0 == --l && (n.API.log("slide skipped; img not loaded:", this.src), 0 == --c && "wait" == n.loader && o.apply(n.API, [a, i]))
                    })
                })) : (--c, void a.push(u))
            }), c && n.container.addClass("cycle-loading"))
        }
        var o;
        n.loader && (o = n.API.add, n.API.add = i)
    })
}(jQuery),
function(e) {
    "use strict";

    function t(t, n, i) {
        var o;
        t.API.getComponent("pager").each(function() {
            var r = e(this);
            if (n.pagerTemplate) {
                var s = t.API.tmpl(n.pagerTemplate, n, t, i[0]);
                o = e(s).appendTo(r)
            } else o = r.children().eq(t.slideCount - 1);
            o.on(t.pagerEvent, function(e) {
                t.pagerEventBubble || e.preventDefault(), t.API.page(r, e.currentTarget)
            })
        })
    }

    function n(e, t) {
        var n = this.opts();
        if (!n.busy || n.manualTrump) {
            var i = e.children().index(t),
                o = i,
                r = n.currSlide < o;
            n.currSlide != o && (n.nextSlide = o, n._tempFx = n.pagerFx, n.API.prepareTx(!0, r), n.API.trigger("cycle-pager-activated", [n, e, t]))
        }
    }
    e.extend(e.fn.cycle.defaults, {
        pager: "> .cycle-pager",
        pagerActiveClass: "cycle-pager-active",
        pagerEvent: "click.cycle",
        pagerEventBubble: void 0,
        pagerTemplate: "<span>&bull;</span>"
    }), e(document).on("cycle-bootstrap", function(e, n, i) {
        i.buildPagerLink = t
    }), e(document).on("cycle-slide-added", function(e, t, i, o) {
        t.pager && (t.API.buildPagerLink(t, i, o), t.API.page = n)
    }), e(document).on("cycle-slide-removed", function(t, n, i) {
        if (n.pager) {
            n.API.getComponent("pager").each(function() {
                var t = e(this);
                e(t.children()[i]).remove()
            })
        }
    }), e(document).on("cycle-update-view", function(t, n) {
        var i;
        n.pager && (i = n.API.getComponent("pager"), i.each(function() {
            e(this).children().removeClass(n.pagerActiveClass).eq(n.currSlide).addClass(n.pagerActiveClass)
        }))
    }), e(document).on("cycle-destroyed", function(e, t) {
        var n = t.API.getComponent("pager");
        n && (n.children().off(t.pagerEvent), t.pagerTemplate && n.empty())
    })
}(jQuery),
function(e) {
    "use strict";
    e.extend(e.fn.cycle.defaults, {
        next: "> .cycle-next",
        nextEvent: "click.cycle",
        disabledClass: "disabled",
        prev: "> .cycle-prev",
        prevEvent: "click.cycle",
        swipe: !1
    }), e(document).on("cycle-initialized", function(e, t) {
        if (t.API.getComponent("next").on(t.nextEvent, function(e) {
                e.preventDefault(), t.API.next()
            }), t.API.getComponent("prev").on(t.prevEvent, function(e) {
                e.preventDefault(), t.API.prev()
            }), t.swipe) {
            var n = t.swipeVert ? "swipeUp.cycle" : "swipeLeft.cycle swipeleft.cycle",
                i = t.swipeVert ? "swipeDown.cycle" : "swipeRight.cycle swiperight.cycle";
            t.container.on(n, function() {
                t._tempFx = t.swipeFx, t.API.next()
            }), t.container.on(i, function() {
                t._tempFx = t.swipeFx, t.API.prev()
            })
        }
    }), e(document).on("cycle-update-view", function(e, t) {
        if (!t.allowWrap) {
            var n = t.disabledClass,
                i = t.API.getComponent("next"),
                o = t.API.getComponent("prev"),
                r = t._prevBoundry || 0,
                s = void 0 !== t._nextBoundry ? t._nextBoundry : t.slideCount - 1;
            t.currSlide == s ? i.addClass(n).prop("disabled", !0) : i.removeClass(n).prop("disabled", !1), t.currSlide === r ? o.addClass(n).prop("disabled", !0) : o.removeClass(n).prop("disabled", !1)
        }
    }), e(document).on("cycle-destroyed", function(e, t) {
        t.API.getComponent("prev").off(t.nextEvent), t.API.getComponent("next").off(t.prevEvent), t.container.off("swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle")
    })
}(jQuery),
function(e) {
    "use strict";
    e.extend(e.fn.cycle.defaults, {
        progressive: !1
    }), e(document).on("cycle-pre-initialize", function(t, n) {
        if (n.progressive) {
            var i, o, r = n.API,
                s = r.next,
                a = r.prev,
                l = r.prepareTx,
                c = e.type(n.progressive);
            if ("array" == c) i = n.progressive;
            else if (e.isFunction(n.progressive)) i = n.progressive(n);
            else if ("string" == c) {
                if (o = e(n.progressive), !(i = e.trim(o.html()))) return;
                if (/^(\[)/.test(i)) try {
                    i = e.parseJSON(i)
                } catch (e) {
                    return void r.log("error parsing progressive slides", e)
                } else i = i.split(new RegExp(o.data("cycle-split") || "\n")), i[i.length - 1] || i.pop()
            }
            l && (r.prepareTx = function(e, t) {
                var o, r;
                return e || 0 === i.length ? void l.apply(n.API, [e, t]) : void(t && n.currSlide == n.slideCount - 1 ? (r = i[0], i = i.slice(1), n.container.one("cycle-slide-added", function(e, t) {
                    setTimeout(function() {
                        t.API.advanceSlide(1)
                    }, 50)
                }), n.API.add(r)) : t || 0 !== n.currSlide ? l.apply(n.API, [e, t]) : (o = i.length - 1, r = i[o], i = i.slice(0, o), n.container.one("cycle-slide-added", function(e, t) {
                    setTimeout(function() {
                        t.currSlide = 1, t.API.advanceSlide(-1)
                    }, 50)
                }), n.API.add(r, !0)))
            }), s && (r.next = function() {
                var e = this.opts();
                if (i.length && e.currSlide == e.slideCount - 1) {
                    var t = i[0];
                    i = i.slice(1), e.container.one("cycle-slide-added", function(e, t) {
                        s.apply(t.API), t.container.removeClass("cycle-loading")
                    }), e.container.addClass("cycle-loading"), e.API.add(t)
                } else s.apply(e.API)
            }), a && (r.prev = function() {
                var e = this.opts();
                if (i.length && 0 === e.currSlide) {
                    var t = i.length - 1,
                        n = i[t];
                    i = i.slice(0, t), e.container.one("cycle-slide-added", function(e, t) {
                        t.currSlide = 1, t.API.advanceSlide(-1), t.container.removeClass("cycle-loading")
                    }), e.container.addClass("cycle-loading"), e.API.add(n, !0)
                } else a.apply(e.API)
            })
        }
    })
}(jQuery),
function(e) {
    "use strict";
    e.extend(e.fn.cycle.defaults, {
        tmplRegex: "{{((.)?.*?)}}"
    }), e.extend(e.fn.cycle.API, {
        tmpl: function(t, n) {
            var i = new RegExp(n.tmplRegex || e.fn.cycle.defaults.tmplRegex, "g"),
                o = e.makeArray(arguments);
            return o.shift(), t.replace(i, function(t, n) {
                var i, r, s, a, l = n.split(".");
                for (i = 0; i < o.length; i++)
                    if (s = o[i]) {
                        if (l.length > 1)
                            for (a = s, r = 0; r < l.length; r++) s = a, a = a[l[r]] || n;
                        else a = s[n];
                        if (e.isFunction(a)) return a.apply(s, o);
                        if (void 0 !== a && null !== a && a != n) return a
                    }
                return n
            })
        }
    })
}(jQuery),
function(e) {
    "use strict";

    function t(t, n, i, o) {
        "caption2" === n.captionPlugin && e.each(["caption", "overlay"], function() {
            var e, t = this + "Fx",
                r = n[t + "Out"] || "hide",
                s = i[this + "Template"],
                a = n.API.getComponent(this),
                l = n[t + "Sel"],
                c = n.speed;
            n.sync && (c /= 2), e = l ? a.find(l) : a, a.length && s ? ("hide" == r && (c = 0), e[r](c, function() {
                var u = n.API.tmpl(s, i, n, o);
                a.html(u), e = l ? a.find(l) : a, l && e.hide(), r = n[t + "In"] || "show", e[r](c)
            })) : a.hide()
        })
    }

    function n(t, n, i, o) {
        "caption2" === n.captionPlugin && e.each(["caption", "overlay"], function() {
            var e = i[this + "Template"],
                t = n.API.getComponent(this);
            t.length && e && t.html(n.API.tmpl(e, i, n, o))
        })
    }
    e.extend(e.fn.cycle.defaults, {
        captionFxOut: "fadeOut",
        captionFxIn: "fadeIn",
        captionFxSel: void 0,
        overlayFxOut: "fadeOut",
        overlayFxIn: "fadeIn",
        overlayFxSel: void 0
    }), e(document).on("cycle-bootstrap", function(e, i) {
        i.container.on("cycle-update-view-before", t), i.container.one("cycle-update-view-after", n)
    })
}(jQuery),
function(e) {
    "use strict";
    e(document).on("cycle-bootstrap", function(e, t, n) {
        "carousel" === t.fx && (n.getSlideIndex = function(e) {
            var t = this.opts()._carouselWrap.children();
            return t.index(e) % t.length
        }, n.next = function() {
            var e = t.reverse ? -1 : 1;
            !1 === t.allowWrap && t.currSlide + e > t.slideCount - t.carouselVisible || (t.API.advanceSlide(e), t.API.trigger("cycle-next", [t]).log("cycle-next"))
        })
    }), e.fn.cycle.transitions.carousel = {
        preInit: function(t) {
            t.hideNonActive = !1, t.container.on("cycle-destroyed", e.proxy(this.onDestroy, t.API)), t.API.stopTransition = this.stopTransition;
            for (var n = 0; n < t.startingSlide; n++) t.container.append(t.slides[0])
        },
        postInit: function(t) {
            var n, i, o, r, s = t.carouselVertical;
            t.carouselVisible && t.carouselVisible > t.slideCount && (t.carouselVisible = t.slideCount - 1);
            var a = t.carouselVisible || t.slides.length,
                l = {
                    display: s ? "block" : "inline-block",
                    position: "static"
                };
            if (t.container.css({
                    position: "relative",
                    overflow: "hidden"
                }), t.slides.css(l), t._currSlide = t.currSlide, r = e('<div class="cycle-carousel-wrap"></div>').prependTo(t.container).css({
                    margin: 0,
                    padding: 0,
                    top: 0,
                    left: 0,
                    position: "absolute"
                }).append(t.slides), t._carouselWrap = r, s || r.css("white-space", "nowrap"), !1 !== t.allowWrap) {
                for (i = 0; i < (void 0 === t.carouselVisible ? 2 : 1); i++) {
                    for (n = 0; n < t.slideCount; n++) r.append(t.slides[n].cloneNode(!0));
                    for (n = t.slideCount; n--;) r.prepend(t.slides[n].cloneNode(!0))
                }
                r.find(".cycle-slide-active").removeClass("cycle-slide-active"), t.slides.eq(t.startingSlide).addClass("cycle-slide-active")
            }
            t.pager && !1 === t.allowWrap && (o = t.slideCount - a, e(t.pager).children().filter(":gt(" + o + ")").hide()), t._nextBoundry = t.slideCount - t.carouselVisible, this.prepareDimensions(t)
        },
        prepareDimensions: function(t) {
            var n, i, o, r, s = t.carouselVertical,
                a = t.carouselVisible || t.slides.length;
            if (t.carouselFluid && t.carouselVisible ? t._carouselResizeThrottle || this.fluidSlides(t) : t.carouselVisible && t.carouselSlideDimension ? (n = a * t.carouselSlideDimension, t.container[s ? "height" : "width"](n)) : t.carouselVisible && (n = a * e(t.slides[0])[s ? "outerHeight" : "outerWidth"](!0), t.container[s ? "height" : "width"](n)), i = t.carouselOffset || 0, !1 !== t.allowWrap)
                if (t.carouselSlideDimension) i -= (t.slideCount + t.currSlide) * t.carouselSlideDimension;
                else
                    for (o = t._carouselWrap.children(), r = 0; r < t.slideCount + t.currSlide; r++) i -= e(o[r])[s ? "outerHeight" : "outerWidth"](!0);
            t._carouselWrap.css(s ? "top" : "left", i)
        },
        fluidSlides: function(t) {
            function n() {
                clearTimeout(o), o = setTimeout(i, 20)
            }

            function i() {
                t._carouselWrap.stop(!1, !0);
                var e = t.container.width() / t.carouselVisible;
                e = Math.ceil(e - s), t._carouselWrap.children().width(e), t._sentinel && t._sentinel.width(e), a(t)
            }
            var o, r = t.slides.eq(0),
                s = r.outerWidth() - r.width(),
                a = this.prepareDimensions;
            e(window).on("resize", n), t._carouselResizeThrottle = n, i()
        },
        transition: function(t, n, i, o, r) {
            var s, a = {},
                l = t.nextSlide - t.currSlide,
                c = t.carouselVertical,
                u = t.speed;
            if (!1 === t.allowWrap) {
                o = l > 0;
                var d = t._currSlide,
                    p = t.slideCount - t.carouselVisible;
                l > 0 && t.nextSlide > p && d == p ? l = 0 : l > 0 && t.nextSlide > p ? l = t.nextSlide - d - (t.nextSlide - p) : 0 > l && t.currSlide > p && t.nextSlide > p ? l = 0 : 0 > l && t.currSlide > p ? l += t.currSlide - p : d = t.currSlide, s = this.getScroll(t, c, d, l), t.API.opts()._currSlide = t.nextSlide > p ? p : t.nextSlide
            } else o && 0 === t.nextSlide ? (s = this.getDim(t, t.currSlide, c), r = this.genCallback(t, o, c, r)) : o || t.nextSlide != t.slideCount - 1 ? s = this.getScroll(t, c, t.currSlide, l) : (s = this.getDim(t, t.currSlide, c), r = this.genCallback(t, o, c, r));
            a[c ? "top" : "left"] = o ? "-=" + s : "+=" + s, t.throttleSpeed && (u = s / e(t.slides[0])[c ? "height" : "width"]() * t.speed), t._carouselWrap.animate(a, u, t.easing, r)
        },
        getDim: function(t, n, i) {
            return e(t.slides[n])[i ? "outerHeight" : "outerWidth"](!0)
        },
        getScroll: function(e, t, n, i) {
            var o, r = 0;
            if (i > 0)
                for (o = n; n + i > o; o++) r += this.getDim(e, o, t);
            else
                for (o = n; o > n + i; o--) r += this.getDim(e, o, t);
            return r
        },
        genCallback: function(t, n, i, o) {
            return function() {
                var n = e(t.slides[t.nextSlide]).position(),
                    r = 0 - n[i ? "top" : "left"] + (t.carouselOffset || 0);
                t._carouselWrap.css(t.carouselVertical ? "top" : "left", r), o()
            }
        },
        stopTransition: function() {
            var e = this.opts();
            e.slides.stop(!1, !0), e._carouselWrap.stop(!1, !0)
        },
        onDestroy: function() {
            var t = this.opts();
            t._carouselResizeThrottle && e(window).off("resize", t._carouselResizeThrottle), t.slides.prependTo(t.container), t._carouselWrap.remove()
        }
    }
}(jQuery),
function(e) {
    "use strict";
    e.event.special.swipe = e.event.special.swipe || {
        scrollSupressionThreshold: 10,
        durationThreshold: 1e3,
        horizontalDistanceThreshold: 30,
        verticalDistanceThreshold: 75,
        setup: function() {
            var t = e(this);
            t.bind("touchstart", function(n) {
                function i(t) {
                    if (s) {
                        var n = t.originalEvent.touches ? t.originalEvent.touches[0] : t;
                        o = {
                            time: (new Date).getTime(),
                            coords: [n.pageX, n.pageY]
                        }, Math.abs(s.coords[0] - o.coords[0]) > e.event.special.swipe.scrollSupressionThreshold && t.preventDefault()
                    }
                }
                var o, r = n.originalEvent.touches ? n.originalEvent.touches[0] : n,
                    s = {
                        time: (new Date).getTime(),
                        coords: [r.pageX, r.pageY],
                        origin: e(n.target)
                    };
                t.bind("touchmove", i).one("touchend", function() {
                    t.unbind("touchmove", i), s && o && o.time - s.time < e.event.special.swipe.durationThreshold && Math.abs(s.coords[0] - o.coords[0]) > e.event.special.swipe.horizontalDistanceThreshold && Math.abs(s.coords[1] - o.coords[1]) < e.event.special.swipe.verticalDistanceThreshold && s.origin.trigger("swipe").trigger(s.coords[0] > o.coords[0] ? "swipeleft" : "swiperight"), s = o = void 0
                })
            })
        }
    }, e.event.special.swipeleft = e.event.special.swipeleft || {
        setup: function() {
            e(this).bind("swipe", e.noop)
        }
    }, e.event.special.swiperight = e.event.special.swiperight || e.event.special.swipeleft
}(jQuery), $(document).ready(function() {
    $(".lightgallery").lightGallery({
        preload: 2,
        subHtmlSelectorRelative: !0
    }), $(".imagegallery").lightGallery({
        preload: 2,
        subHtmlSelectorRelative: !0
    }), $(".videogallery").lightGallery({
        subHtmlSelectorRelative: !0,
        youtubePlayerParams: {
            controls: 1,
            modestbranding: 1,
            rel: 0,
            showinfo: 0
        },
        vimeoPlayerParams: {
            badge: 0,
            byline: 0,
            portrait: 0,
            title: 0
        }
    })
}), $(document).ready(function($) {
    $("a.open-modal").click(function(e) {
        return $(this).modal({
            fadeDuration: 250,
            fadeDelay: .8
        }), !1
    })
});
var body = $(document.body),
    nav = $("#nav"),
    obfuscator = $("#obfuscator");
$(document).ready(function() {
        $("#toggle, #close, #obfuscator").on("click", function() {
            nav.toggleClass("nav-open"), body.toggleClass("no-scroll"), obfuscator.toggleClass("is-visible")
        })
    }), $(document).keyup(function(e) {
        obfuscator.hasClass("is-visible") && 27 == e.keyCode && (nav.toggleClass("nav-open"), body.toggleClass("no-scroll"), obfuscator.toggleClass("is-visible"))
    }),
    function() {
        var e = document.querySelector("#header");
        window.location.hash && e.classList.add("headroom--unpinned"), new Headroom(e, {
            tolerance: {
                down: 10,
                up: 20
            },
            offset: 205
        }).init()
    }(), $(document).ready(function() {
        $("#nav .trigger").click(function() {
            $(this).toggleClass("open"), $(this).next("ul").slideToggle("fast")
        })
    }), $(document).ready(function() {
        $(".page-group-page").matchHeight(), $(".template-site-selection .site-name").matchHeight()
    }), $(document).ready(function() {
        $(".unit-map-link").click(function() {
            $(this).toggleClass("open");
            var e = $(this).data("id");
            $("#" + e).toggle()
        })
    });