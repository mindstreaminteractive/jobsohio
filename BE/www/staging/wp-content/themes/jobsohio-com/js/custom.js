/**
 * File custom.js.
 *
 */

 
	( function( $ ) {

	

$(document).ready(function() {

function init() {
var vidDefer = document.getElementsByTagName('iframe');
for (var i=0; i<vidDefer.length; i++) {
if(vidDefer[i].getAttribute('data-src')) {
vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
} } }
window.onload = init;
  $(".lightgallery").lightGallery({
        preload: 2,
        subHtmlSelectorRelative: !0
    });
  $(".imagegallery").lightGallery({
        preload: 2,
        subHtmlSelectorRelative: !0
    });
  

  var $lg = $('.videogallery');
    $lg.lightGallery();

  var $grid = $('.successpostswrap').isotope({
  itemSelector: '.industry'
});
  $grid.imagesLoaded().progress( function() {
  $grid.isotope('layout');
});
var filters = {};
$('.filters-form select').change(function () {


  var $button = $( event.currentTarget );
  // get group key
  var $buttonGroup = $(this);
  var filterGroup = $(this).attr('data-filter-group');
  
  // set filter for group
  filters[ filterGroup ] = this.value;
  // combine filters
  var filterValue = concatValues( filters );
  // set filter for Isotope
  $grid.isotope({ filter: filterValue });
});

// change is-checked class on buttons
$('.industryfilter').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function( event ) {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    var $button = $( event.currentTarget );
    $button.addClass('is-checked');
  });
});


// Utility Map

$(".jo-map-filter").on("click", function() {
  $(this).toggleClass('active');
  var utility=$(this).data("jo-utility");
  console.log(utility);
  $('.map-'+utility).toggleClass('active');

});

// End Uitlity Map

  
// flatten object by concatting values
function concatValues( obj ) {
  var value = '';
  for ( var prop in obj ) {
    value += obj[ prop ];
  }
  return value;
}

// grab an element
var headroomel = document.querySelector("header");


var headroom = new Headroom(headroomel, {
        tolerance: {
                down: 10,
                up: 20
            },
            offset: 205
});
headroom.init();




	var body = $(document.body),
    nav = $("#nav"),
    obfuscator = $("#obfuscator");



        $("#toggle, #close, #obfuscator").on("click", function() {
            nav.toggleClass("nav-open"), body.toggleClass("no-scroll"), $("#obfuscator").toggleClass("is-visible")
        })
    }); 
$(document).keyup(function(e) {
        $("#obfuscator").hasClass("is-visible") && 27 == e.keyCode && (nav.toggleClass("nav-open"), body.toggleClass("no-scroll"), $("#obfuscator").toggleClass("is-visible"))
    });
 $(document).ready(function() {
   $('#searchicon').click(function (e) {
        e.preventDefault();
       
      $('#search-header').toggleClass("open");
       $('#searchbox').focus();

    });
        $("#nav .menu-item-has-children a").click(function() {
            $(this).toggleClass("open"), $(this).next("ul").slideToggle("fast")
        })
    });
 $(document).ready(function() {
        $(".page-group-page").matchHeight(), $(".template-site-selection .site-name").matchHeight()
    })
 $(document).ready(function() {
  $('.sitespostswrap .col-sm-6 .site-name').matchHeight();
        $(".unit-map-link").click(function() {
            $(this).toggleClass("open");
            var e = $(this).data("id");
            $("#" + e).toggle()
        })
    });

	} )( jQuery );
