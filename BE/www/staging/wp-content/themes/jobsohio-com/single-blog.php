<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jobs_Ohio
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );


			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				// comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php
$previous = get_previous_post();
$next = get_next_post();


	?>
<div class="lateral">
    <div class="constrain">
        <div class="container-fluid">
            <div class="row">
            

               
   <div class="col-xs-12"><a class="lateral-back" href="/blog/">Back to Blog</a></div>

            
                <div class="col-md-6">
                	<?php if ($next) { ?>
                	<a class="lateral-sibling lateral-prev" href="<?php echo get_the_permalink($next); ?>">
                        <div class="lateral-label">Next</div>
                        <div class="lateral-title"><?php echo get_the_title($next); ?></div>
                        <div class="lateral-date"><?php echo mysql2date('F jS, Y', $next->post_date, false); ?></div>
                    </a>
                <?php } ?>

                </div>
                <div class="col-md-6">
                	<?php if ($previous) { ?>
                    <a class="lateral-sibling lateral-next" href="<?php echo get_the_permalink($previous); ?>">
                        <div class="lateral-label">Previous</div>
                        <div class="lateral-title"><?php echo get_the_title($previous); ?></div>
                        <div class="lateral-date"><?php echo mysql2date('F jS, Y', $previous->post_date, false); ?></div>
                    </a>
                       <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
