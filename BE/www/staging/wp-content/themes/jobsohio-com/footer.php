<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jobs_Ohio
 */

?>

<footer id="contact" class="footer">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6 col-lg-5 footer-col footer-col--contact">
            <?php dynamic_sidebar( 'footer-left' ); ?>
         </div>
         <div class="col-sm-6 col-lg-7 footer-col footer-col--utilities">
         		<div class="col-xs-12">
				   <h4>About JobsOhio</h4>
				   <div class="row">
				      <div class="col-lg-6">
				         <ul class="menu">
				            <li><a href="/about-jobsohio/about-us/">About Us</a></li>
				            <li><a href="/jobsohio-results/">JobsOhio Results</a></li>
				            <li><a href="/corporate-governance/">Corporate Governance</a></li>
				            <li><a href="/financial-statements/">Financial Statements &amp; IRS Filings</a></li>
				            <li><a href="/site/assets/files/1328/jobsohio_terms_and_conditions_071718.pdf" target="_blank">Terms and Conditions</a></li>
				            <li><a href="/privacy/">Privacy Policy</a></li>
				         </ul>
				      </div>
				      <div class="col-lg-6">
				         <ul class="menu">
				            <li><a href="/careers/">JobsOhio Careers</a></li>
				            <li><a href="/board-committees/">Board of Directors</a></li>
				            <li><a href="/jobsohio-network/">JobsOhio Network</a></li>
				            <li><a href="/state-agencies-of-ohio/">State Agencies of Ohio</a></li>
				            <li><a href="/contact/">Contact Us</a></li>
				            <li><a href="/email/">Sign Up for Email</a></li>
				         </ul>
				      </div>
   </div>
</div>

         </div>
      </div>
   </div>
   <div class="footer-legal">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6">
            <div class="footer-copyright">Copyright <?php echo date('Y'); ?> JobsOhio. All Rights Reserved.</div>
         </div>
         <div class="col-sm-6">
            <div class="footer-badges"><a href="https://www.guidestar.org/profile/45-2798687" target="_blank"><img class="img-responsive" src="https://widgets.guidestar.org/gximage2?o=9322205&amp;l=v4" alt="Guidestar Seal of Transparency"></a></div>
         </div>
      </div>
   </div>
</div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
