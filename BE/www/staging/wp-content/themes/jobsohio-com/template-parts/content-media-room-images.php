<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

$image = get_field('image');
$size = 'large';
?>


<div class="asset">
    <div class="row">
        <div class="col-sm-4">
            <div class="image"><img class="img-responsive" alt="Columbus Skyline" src="<?php echo $image['sizes'][ $size ]; ?>"></div>
        </div>
        <div class="col-sm-8">
            <?php the_title( '<h2>', '</h2>' ); ?>
            <div class="body">
              <?php the_field('description'); ?>
            </div>
            <div class="buttons">
                <div class="button button--cta"><a href="<?php echo $image['url']; ?>" download="<?php echo basename($image['url']); ?>" target="_blank">Download</a></div>
            </div>
        </div>
    </div>
</div>

	