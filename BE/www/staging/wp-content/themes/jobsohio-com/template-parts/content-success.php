<?php
/**
 * Template part for displaying events posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

$image = get_field('banner_image');
?>



<div class="col-sm-6">
   <a class="story" href="<?php echo get_permalink();  ?>">
      <div class="story-image-content">
         <div class="story-image-table">
            <div class="story-image-title"><?php the_title(); ?></div>
         </div>
      </div>
      <div class="story-overlay"></div>
      <img class="img-responsive" alt="<?php the_field('lead'); ?>" src="<?php echo $image['url']; ?>">
   </a>
</div>





