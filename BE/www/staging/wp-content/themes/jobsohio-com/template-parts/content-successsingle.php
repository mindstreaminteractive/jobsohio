<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */


$logo = get_field('logo');
$filedownload = get_field('case_study_download');

?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <div class="post-header">
      <div class="row">
         <div class="col-xs-12">
            <div class="kicker"><a href="/ohio-success/">Ohio Success</a></div>
            <h1 class="post-title"><?php the_field('heading'); ?></h1>
         </div>
      </div>
   </div>
   <div class="post-content">
      <div class="row">
         <div class="col-sm-7">
            <div class="header-meta">
               <div class="share">
               <!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_facebook"></a>
</div>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
               </div>
            </div>
            <?php
               the_content( sprintf(
               	wp_kses(
               		/* translators: %s: Name of current post. Only visible to screen readers */
               		__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'jobsohio-com' ),
               		array(
               			'span' => array(
               				'class' => array(),
               			),
               		)
               	),
               	get_the_title()
               ) );
               
               
               ?>
         </div>
         <aside class="col-sm-5"> <?php if ($logo['url']) { ?>
            <aside class="subunit images">
              <figure><img class="img-responsive" alt="fujao_logo.png" src="<?php echo $logo['url']; ?>"></figure>
            </aside> <?php } ?>
            <?php the_field('quote'); ?>
            <?php if ($filedownload['url']) { ?>
            <div class="button button--download"><a href="<?php echo $filedownload['url']; ?>" target="_blank">Download <?php the_title(); ?> Case Study</a></div>
         <?php } ?>
            <?php the_field('right_column_bottom'); ?>
         </aside>
      </div>
      <!-- .entry-content -->
   </div>
</article>
<!-- #post-<?php the_ID(); ?> -->

	

	