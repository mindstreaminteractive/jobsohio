<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */


$image = get_field('banner_image');

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row featuredpostwrap">
		<div class="col-sm-7">
			<div class="post-image"><?php if ($image['url']) { ?><img class="img-responsive" alt="<?php the_title(); ?>" src="<?php echo $image['url']; ?>"><?php } ?></div>
		
</div>
<div class="col-sm-5">
	<header class="entry-header">
		<?php
		jobsohio_com_getfeaturedpost_header_link(get_post_type( get_the_ID()));
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h3 class="blog-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		endif;

		?>
<div class="deck"><?php the_field('deck'); ?></div>
<div class="author"><?php the_field('author'); ?></div>
<div class="date"><?php jobsohio_com_posted_on(); ?></div>
<div class="abstract"><p><?php the_field('abstract'); ?></p></div>
<div class="buttons"><div class="button button--cta"><a href="<?php the_permalink(); ?>">Read More</a></div></div>
<div class="button button--cta"><a href="/ohio-success/">Learn More About Other Successes</a></div>

	

		
	</header><!-- .entry-header -->

	

	
</div>
</div>
</div><!-- #post-<?php the_ID(); ?> -->
