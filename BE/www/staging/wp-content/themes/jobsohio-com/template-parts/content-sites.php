<?php
/**
 * Template part for displaying events posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

$image = get_field('banner_image');
$asset= get_field('assets');

$iconimg=""; $iconimg2=""; $iconimg3=""; $iconimg4="";
if ($asset[0]) { $iconimg = str_replace(' ', '-', $asset[0]); }
if ($asset[1]) { $iconimg2 = str_replace(' ', '-', $asset[1]); }
if ($asset[2]) { $iconimg3 = str_replace(' ', '-', $asset[2]);  }
if ($asset[3]) { $iconimg4 = str_replace(' ', '-', $asset[3]); }

?>



<div class="col-sm-6 col-md-4" style="">
   <div class="site">
      <a href="<?php  echo esc_url( get_permalink());?>">
         <div class="site-image"><img class="img-responsive" alt="<?php the_title(); ?>" src="<?php echo $image['url']; ?>"></div>
      </a>
      <div class="site-name"><a href="<?php echo get_permalink();  ?>"><?php the_title(); ?></a></div>
      <div class="site-assets">
      	<?php if ($iconimg) { ?><img class="img-responsive" alt="" src="/wp-content/uploads/2018/08/asset-<?php echo strtolower($iconimg); ?>.png"> <?php } ?>
         <?php if ($iconimg2) { ?><img class="img-responsive" alt="" src="/wp-content/uploads/2018/08/asset-<?php echo strtolower($iconimg2); ?>.png"> <?php } ?>
         <?php if ($iconimg3) { ?><img class="img-responsive" alt="" src="/wp-content/uploads/2018/08/asset-<?php echo strtolower($iconimg3); ?>.png"> <?php } ?>
         <?php if ($iconimg4) { ?><img class="img-responsive" alt="" src="/wp-content/uploads/2018/08/asset-<?php echo strtolower($iconimg4); ?>.png"> <?php } ?>
      </div>
      <div class="site-location">
         <div class="site-attribute">
            <div class="site-label">Region:</div>
            <div class="site-value"><?php the_field('region'); ?></div>
         </div>
         <div class="site-attribute">
            <span class="site-label">County:</span>
            <div class="site-value"><?php the_field('county'); ?></div>
         </div>
      </div>
      <div class="site-attributes">
         <?php the_field('site_overview'); ?>
      </div>
   </div>
</div>





