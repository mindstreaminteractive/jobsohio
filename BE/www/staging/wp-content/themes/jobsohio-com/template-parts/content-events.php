<?php
/**
 * Template part for displaying events posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */
$sdate = get_field('start_date', false, false);
$sdate=strtotime($sdate);


$edate = get_field('end_date', false, false);

$city = get_field('city');

$edate=strtotime($edate);

if ($edate>time()) {
?>


<div class="event">
	 <div class="constrain">
	 	<div class="container-fluid">
	 		<div class="event-main">
	 			<div class="row"><div class="col-sm-10 col-sm-offset-1">
	 				<div class="event-header"><?php the_title( '<h2 class="heading event-heading">', '</h2>' ); ?></h2></div>
	 				<div class="event-info"><div class="event-building"><?php the_field('location_name'); ?></div>
	 				<div class="event-adr"><span class="event-locality"><?php if ($city) { the_field('city'); ?></span>, <?php } ?> <span class="event-region"><?php the_field('state'); ?></span></div>
	 				<div class="event-date">
	 					<?php   
	 					if ($edate<$sdate) {
           echo date('F jS, Y',$sdate); 

                         } else if (date("F Y", $sdate) == date("F Y", $edate)) {
                         echo date('F j', $sdate); echo "-".date('jS, Y',$edate); 
                     } else { 

                             echo date('F j',$sdate)."-".date('F jS, Y',$edate);

}
	 						?>

	 				

	 						</div></div>
	 				<div class="event-content">
	 					<div class="event-body"><?php the_content(); ?></div>
	 				</div>
	 				<div class="buttons buttons--center"><div class="button button--cta"><a href="<?php the_field('learn_more_button'); ?>" target="_blank">Learn More</a></div></div>
	 			</div>
	 		</div>
	 	</div>
	 </div>
	</div>
</div>
<?php } ?>

