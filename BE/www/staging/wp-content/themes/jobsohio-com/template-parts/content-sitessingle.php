<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

$image = get_field('banner_image');
$filedownload = get_field('site_details_download');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

<div class="unit-site-main" style="padding-top:0;">
   <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
         <div class="unit-header">
            <h1 class="heading"><?php the_title();  ?></h1>
         </div>
         <div class="unit-content">
            <div class="lead">
               <p><?php the_field('lead'); ?></p>
            </div>
            <div class="body">
           <?php the_field('body'); ?>
            </div>
            
           <?php if($filedownload['url']) { ?> <div class="button button--download" style=""><a href="<?php echo $filedownload['url']; ?>" target="_blank">Download Full Site Details</a></div>  <?php } ?> 
         </div>
      </div>
   </div>
</div>



		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'jobsohio-com' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		
		?>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->

	