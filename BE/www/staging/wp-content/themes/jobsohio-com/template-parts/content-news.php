<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="unit-header"><div class="kicker"><a href="/news/">News</a></div><?php the_title( '<h1 class="heading heading--post"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?><div class="deck"><?php the_field('deck'); ?></div></div>
	</header><!-- .entry-header -->



	<div class="content">
      <div class="row"><div class="col-md-7">

		<div class="header-meta"><div class="author"><?php the_field('author'); ?></div><div class="date"><?php jobsohio_com_posted_on(); ?></div><div class="share">
		<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_facebook"></a>
</div>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
		</div></div></div></div></div>
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'jobsohio-com' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		
		?>
			<div class="post-meta"><div class="post-meta-group"><span class="post-meta-heading">
<?php 
$postid=get_the_ID();

$terms = wp_get_post_terms($postid,"news-category",array('fields' => 'all'));


 $count = count($terms); if ( $count > 0 ){
$catlinks="";
   foreach ( $terms as $term ) { 
   	if ($term->name!="Uncategorized") {
   	                        $term_link = get_term_link( $term, 'news-category');

   	$catlinks.= '<a href="'.$term_link.'">'.$term->name.'</a>,  '; 

   } 
}
if ($catlinks) { echo "Categories: ".rtrim($catlinks, ', '); }
   } ?> 


			</span> <?php the_category(', '); ?></div></div>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->

	