<?php
/**
 * Template part for displaying events posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

$image = get_field('photo');
$biotxt=get_field('bio');
$industries=get_field('industries');
$subsector=get_field('subsector');

?>



<div class="col-sm-6 col-md-4 col-centered">
    <div class="employee">
        <div class="employee-portrait"><img class="img-responsive" alt="<?php the_title(); ?>" src="<?php echo $image['url']; ?>"></div>
        <div class="employee-name"><?php the_title(); ?></div>
        <div class="employee-position"><?php the_field('position'); ?></div>
        <?php if( $industries ) { ?>
       
        <div class="employee-industries" style=""> 
            <?php $i=0; $indylinks=""; foreach ($industries as $industry) { 
                     $txtstr="";
                     $i++;
                    $txtstr=str_replace('-',' ',$industry);
                    $indylinks.=' <a href="/industries/'.$industry.'/">'.ucwords($txtstr).'</a>,'; 

                      
                } 
                   echo rtrim($indylinks,',');
                   if ($subsector) { echo ", ".$subsector; } ?> </div>
                 
        <?php  } ?>

        <div class="employee-industries"></div>
        <div class="email employee-email"><a href="mailto:<?php the_field('email'); ?>?bcc=jobsohio@dev-jobsohio.local">Email Me</a></div>
        <div class="employee-tel tel"><a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a></div>
        <?php if ($biotxt) { ?>
        <div class="employee-bio"><a class="open-modal" href="#employee-<?php the_id(); ?>" rel="modal:open">View Biography</a>
            <div id="employee-<?php the_id(); ?>" class="modal">
               <?php the_field('bio'); ?>
            </div>
        </div>
    <?php } ?>
    </div>
</div>




