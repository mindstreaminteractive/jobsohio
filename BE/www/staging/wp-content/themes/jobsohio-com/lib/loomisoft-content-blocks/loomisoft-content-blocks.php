<?php

defined( 'ABSPATH' ) or die();

define( 'LS_CB_PLUGIN', __FILE__ );
define( 'LS_CB_PLUGIN_PATH', get_template_directory() . '/lib/loomisoft-content-blocks/');
// echo  LS_CB_PLUGIN_PATH;

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
require_once( LS_CB_PLUGIN_PATH . 'includes/ls_cb_main.php' );
require_once( LS_CB_PLUGIN_PATH . 'includes/ls_cb_widget.php' );

ls_cb_main::start( LS_CB_PLUGIN );

function ls_content_block_by_id( $id = false, $para = false, $vars = array() ) {
	return ls_cb_main::get_block_by_id( $id, $para, $vars );
}

function ls_content_block_by_slug( $slug = false, $para = false, $vars = array() ) {
	return ls_cb_main::get_block_by_slug( $slug, $para, $vars );
}
