<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Jobs_Ohio
 */

if ( ! function_exists( 'jobsohio_com_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function jobsohio_com_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'jobsohio-com' ), $time_string);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'jobsohio_com_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function jobsohio_com_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'jobsohio-com' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'jobsohio_com_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function jobsohio_com_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'jobsohio-com' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'jobsohio-com' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'jobsohio-com' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'jobsohio-com' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'jobsohio-com' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'jobsohio-com' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;


if ( ! function_exists( 'hide_email' ) ) :
function hide_email($email)

{ $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';

  $key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1,999999999);

  for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];

  $script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';

  $script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';

  $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"?bcc=&#106;&#111;&#098;&#115;&#111;&#104;&#105;&#111;&#064;&#106;&#111;&#098;&#115;&#111;&#104;&#105;&#111;&#046;&#099;&#111;&#109;\\">Email Me</a>"';

  $script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")"; 

  $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';

  return '<span id="'.$id.'">[javascript protected email address]</span>'.$script;

}
endif;

if ( ! function_exists( 'jobsohio_com_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function jobsohio_com_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;


if ( ! function_exists( 'jobsohio_com_getfeaturedpost_header_link' ) ) :
function jobsohio_com_getfeaturedpost_header_link($type) {
     $johl="";
    // echo $type;
	if ( $type == 'news-posts' ) {
          $johl='<div class="kicker"><a href="/news/">News</a></div>';
                  } else if ( $type == 'ohio-success' ) {
          $johl='<div class="kicker"><a href="/ohio-success/">OHIO SUCCESS</a></div>';
                  }  else if ( $type == 'industry-events' ) {
          $johl='<div class="kicker"><a href="/events/">Events</a></div>';
                  } else {

                    $johl='<div class="kicker"><a href="/blog/">Blog</a></div>';	
                  }


		echo $johl;
}
endif;


if ( ! function_exists( 'jobsohio_com_getfeaturedpost_header_linknewwindow' ) ) :
function jobsohio_com_getfeaturedpost_header_linknewwindow($type) {
     $johl="";
    // echo $type;
  if ( $type == 'news-posts' ) {
          $johl='<div class="kicker"><a href="/news/" target="_blank">News</a></div>';
                  } else if ( $type == 'ohio-success' ) {
          $johl='<div class="kicker"><a href="/ohio-success/" target="_blank">OHIO SUCCESS</a></div>';
                  }  else if ( $type == 'industry-events' ) {
          $johl='<div class="kicker"><a href="/events/" target="_blank">Events</a></div>';
                  } else {

                    $johl='<div class="kicker"><a href="/blog/" target="_blank">Blog</a></div>';  
                  }


    echo $johl;
}
endif;

// Exclude Uncategorized Category

function exclude_post_categories($excl='', $spacer=' ') {
  $categories = get_the_category($post->ID);
  if (!empty($categories)) {
  	$pass=false;
    $exclude = $excl;
    $exclude = explode(",", $exclude);
    $thecount = count(get_the_category()) - count($exclude);
    foreach ($categories as $cat) {
      $html = '';
      if (!in_array($cat->cat_ID, $exclude)) {
        $html .= '<a href="' . get_category_link($cat->cat_ID) . '" ';
        $html .= 'title="' . $cat->cat_name . '">' . $cat->cat_name . '</a>, ';
        if ($thecount > 0) {
          $html .= $spacer;
        }
        $thecount--;
         $cathtml.=$html;
         $pass=true;
      }
    }
    if ($pass) { echo '<span class="post-meta-heading">Categories:</span> '.rtrim($cathtml,', '); }
  }
}



function jobsohioreviewscount_func( $atts ) {
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.birdeye.com/resources/v1/review/businessid/153183581396547/summary?api_key=cTrXDHobWrcsevGvNsgwF4Tk3P0T16w4&statuses=published");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

$response = curl_exec($ch);
curl_close($ch);
$xml = simplexml_load_string($response);
$json = json_encode($xml);
$arr = json_decode($json,true);
ob_start();
?>
<div class="publishRectBoxInner3 resellerPublishRectBoxInner" style="width:160px; height: 174px; background-image:url(//dev-jobsohio.local/assets/img/StateOfOhioBadge.png); background-size:100%;">
    <div class="reviewCountBox"><?php echo $arr['reviewCount']; ?></div>
    <div class="widgetLabelTextWrapper">Client Reviews</div>
    <div class="reviewCountStarB2"><i class="reviewCountStarIconSmall"></i><i class="reviewCountStarIconSmall"></i><i class="reviewCountStarIconSmall"></i><i class="reviewCountStarIconSmall"></i><i class="reviewCountStarIconSmall" style="width: 14.4px;"></i></div>
</div>

<style type="text/css">
.publishRectBoxInner3 {
    text-align: center;


    }
 .resellerPublishRectBoxInner { background-repeat: no-repeat; }
.reviewCountBox {
    color: #FFF;
    font-size: 30px;
    font-style: normal;
    font-weight: 400;
    margin: 0 auto;
    text-align: center;
    width: 100%;
    padding-top: 40px;
}
	.reviewCountStarIconSmall {
    background: url(https://d3cnqzq0ivprch.cloudfront.net/prod/css/images/small-star.png) no-repeat;
    cursor: pointer;
    height: 13px;
    width: 13px;
    float: left;
}

.widgetLabelTextWrapper {
    margin: 2px auto;
    line-height: 6px;
        margin-top: 5px;
        color: #fff;
}

.reviewCountStarB2 {
    margin-top: 8px;
    display: inline-block;
    padding: 0;
    width: auto;
    text-align: center;
}
</style>
<?php
return ob_get_clean();
}

add_shortcode( 'jobsohioreviewscount', 'jobsohioreviewscount_func' );

// Custom Post Taxonomy Category Menus
function list_terms_custom_taxonomy( $atts ) {
 
// Inside the function we extract custom taxonomy parameter of our shortcode
 
    extract( shortcode_atts( array(
        'custom_taxonomy' => '',
    ), $atts ) );
 
// arguments for function wp_list_categories
$args = array( 
taxonomy => $custom_taxonomy,
title_li => ''
);
 
// We wrap it in unordered list 
echo '<ul>'; 
echo wp_list_categories($args);
echo '</ul>';
}
 
// Add a shortcode that executes our function
add_shortcode( 'ct_terms', 'list_terms_custom_taxonomy' );
 
//Allow Text widgets to execute shortcodes
 
add_filter('widget_text', 'do_shortcode');



if (!class_exists('ls_cb_main')) {


	include_once(get_template_directory() . '/lib/include.php');
}





if ( ! function_exists( 'jo_check_prev_password' ) ) :

function jo_check_prev_password( $post_id ) {
 $pid=$post_id;

  while($pid>0) {

    $previous=jo_get_previous_post($pid);
       
      if ($previous->post_password =="") {
        return ($previous);
         break; 

      }  else { $pid=$previous->ID;}

    }

}
endif;

if ( ! function_exists( 'jo_get_previous_post_id' ) ) :

function jo_get_previous_post( $post_id ) {
    // Get a global post reference since get_adjacent_post() references it
    global $post;

    // Store the existing post object for later so we don't lose it
    $oldGlobal = $post;

    // Get the post object for the specified post and place it in the global variable
    $post = get_post( $post_id );

    // Get the post object for the previous post
    $previous_post = get_previous_post();

    // Reset our global object
    $post = $oldGlobal;

    if ( '' == $previous_post ) {
        return 0;
    }

    return $previous_post;
}

endif;






if ( ! function_exists( 'jo_check_next_password' ) ) :

function jo_check_next_password( $post_id ) {
 $pid=$post_id;

  while($pid>0) {

    $next=jo_get_next_post($pid);
       
      if ($next->post_password =="") {
        return ($next);
         break; 

      }  else { $pid=$next->ID;}

    }

}
endif;

if ( ! function_exists( 'jo_get_next_post_id' ) ) :

function jo_get_next_post( $post_id ) {
    // Get a global post reference since get_adjacent_post() references it
    global $post;

    // Store the existing post object for later so we don't lose it
    $oldGlobal = $post;

    // Get the post object for the specified post and place it in the global variable
    $post = get_post( $post_id );

    // Get the post object for the next post
    $next_post = get_next_post();

    // Reset our global object
    $post = $oldGlobal;

    if ( '' == $next_post ) {
        return 0;
    }

    return $next_post;
}

endif;





if ( ! function_exists( 'jobsohio_numeric_post_nav' ) ) :

// numbered pagination
function jobsohio_numeric_post_nav($pages = '', $range = 10)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination newpager\"><ul class='MarkupPagerNav'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo; First</a></li>";
         if($paged > 1) echo "<li class='jonp'><a href='".get_pagenum_link($paged - 1)."'><img width=\"48\" height=\"48\" alt=\"Next\" src=\"/site/templates/images/prev.svg\"></a></li>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class=\"MarkupPagerNavOn\"><a><span>".$i."</span></a></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</li></a>";
             }
         }
 
         if ($paged < $pages) echo "<li class='jonp'><a href=\"".get_pagenum_link($paged + 1)."\"><span><img width=\"48\" height=\"48\" alt=\"Next\" src=\"/site/templates/images/next.svg\"></span></a></li>";  
         // if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) //echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</ul></div>";
     }
}

endif;