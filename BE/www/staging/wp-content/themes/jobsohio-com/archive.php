<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Jobs_Ohio
 */

get_header();
?>
<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="intro"><div class="constrain"><div class="container-fluid">
				<div class="intro-main">
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
								<?php jobsohio_com_getfeaturedpost_header_link(get_post_type( get_the_ID())); ?>
								<h2 class="heading"><?php echo str_replace('Category:','', get_the_archive_title());  ?></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="constrain">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-9 col-md-push-3">

		<?php if ( have_posts() ) : ?>

			

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-blog', get_post_type() );

			endwhile;

			jobsohio_numeric_post_nav();


		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div>
		<div class="col-md-3 col-md-pull-9"><div class="posts-meta">
                    <?php 
                $posttype=get_post_type();
          
                if ($posttype=="news-posts") {
                   // dynamic_sidebar( 'newssidebar-1' );
                   echo do_shortcode('[ct_terms custom_taxonomy=news-category]');

                } else {
                    dynamic_sidebar( 'sidebar-1' ); 
                }
?>
		</div>
	</div>
</div>
</div>
</div>
</main>
</div>

<?php

get_footer();
