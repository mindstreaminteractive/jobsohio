<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jobs_Ohio
 */

get_header();
?>

	<div id="primary" class="content-area">
	<?php
	
$image = get_field('banner_image');
if ($image['url']) {
?>
<div class="banner banner-hero fullwidthbanner" style="background-image:url(<?php echo $image['url']; ?>);"><div class="banner-hero-content"><div class="banner-hero-table"></div></div></div>
<?php } ?>
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-news', get_post_type() );


			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				// comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php
$previous = get_previous_post();
$next = get_next_post();

$previous = jo_check_prev_password($previous->ID);
$next = jo_check_prev_password($next->ID);


	?>
<div class="lateral">
    <div class="constrain">
        <div class="container-fluid">
            <div class="row">
            

               
   <div class="col-xs-12"><a class="lateral-back" href="/news/">Back to News</a></div>

            
                <div class="col-md-6">
                	<?php if ($next) { ?>
                	<a class="lateral-sibling lateral-prev" href="<?php echo get_the_permalink($next); ?>">
                        <div class="lateral-label">Next</div>
                        <div class="lateral-title"><?php echo get_the_title($next); ?></div>
                        <div class="lateral-date"><?php echo mysql2date('F j, Y', $next->post_date, false); ?></div>
                    </a>
                <?php } ?>

                </div>
                <div class="col-md-6">
                	<?php if ($previous) { ?>
                    <a class="lateral-sibling lateral-next" href="<?php echo get_the_permalink($previous); ?>">
                        <div class="lateral-label">Previous</div>
                        <div class="lateral-title"><?php echo get_the_title($previous); ?></div>
                        <div class="lateral-date"><?php echo mysql2date('F j, Y', $previous->post_date, false); ?></div>
                    </a>
                       <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();