<?php

/*
Plugin Name: JobsOhio SiteOrigin Widgets
Description: JobsOhio Custom SiteOrigin Widgets
Version: 0.1
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
License: GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.txt
*/

function add_jobsohio_widgets_collection($folders){
	$folders[] = plugin_dir_path(__FILE__).'extra-widgets/';
	return $folders;
}
add_filter('siteorigin_widgets_widget_folders', 'add_jobsohio_widgets_collection');

