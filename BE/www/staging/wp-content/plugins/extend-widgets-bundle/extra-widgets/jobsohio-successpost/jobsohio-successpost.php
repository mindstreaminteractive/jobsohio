<?php

/*
Widget Name: JobsOhio Success Spotlight Block
Description: dev-jobsohio.local Success Spotlight Block
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_successpost_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-successpost') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}

function jobsohio_get_successposts($type) { 
         $post_args = array(
    'posts_per_page'   => 500,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => $type,
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'       => '',
    'author_name'       => '',
    'post_status'      => 'publish',
    'suppress_filters' => false 
);
$posts_array = get_posts( $post_args );
foreach ($posts_array as $post) {
	$posts[$post->ID] = $post->post_title." ".$post->post_date;
}
	return $posts;
    }

  

add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_successpost_banner_img_src', 10, 2);

class jobsohio_successpost_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-successpost',
			__('JobsOhio Success Spotlight Block', 'jobsohio_successpost-text-domain'),
			array(
				'description' => __('JobsOhio Success Spotlight Block', 'jobsohio-successpost-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					 'imageposition' => array (
			   		'type' => 'select',
					        'label' => esc_html__( 'Image Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'right' => esc_html__( 'Right', 'jobsohio-com' )
					     
					        )
					    	),
					'post' => array(
					'type' => 'select',
					'state_handler' => array(
        'posttype[blog]' => array('show'),
        'posttype[news]' => array('hide'),
        'posttype[media]' => array('hide')
    ),	
						 'prompt' => esc_html__( '-', 'jobsohio-com' ),
					'label' => 'Choose a Success Story',
					'description' => '',
					'options' => jobsohio_get_successposts('ohio-success')		
				),'imageorvideo' => array (
			   		'type' => 'select',
					        'label' => esc_html__( 'Display Image or Video', 'jobsohio-com' ),
					        'description' => 'If a video is not assigned to the post the post banner image will be displayed.',
					        'default' => 'image',
					        'options' => array(
					            'image' => esc_html__( 'Image', 'jobsohio-com' ),
					            'video' => esc_html__( 'Video', 'jobsohio-com' )
					     
					        )
					    	)
					
				
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-successpost-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-successpost-style';
	}
}

siteorigin_widget_register('jobsohio-successpost', __FILE__, 'jobsohio_successpost_Widget');