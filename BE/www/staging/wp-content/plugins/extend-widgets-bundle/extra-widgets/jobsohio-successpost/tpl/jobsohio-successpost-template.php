<?php


$postid = $instance['post'];

$imgpostition= $instance['imageposition'];

$post = get_post( $postid  ); 


$image = get_field('banner_image',$postid);

$sdate = $post->post_date;
$publishdate = new DateTime($sdate);

$videoid = $post->video_banner;
$imageorvideo= $instance['imageorvideo'];
?>

<div id="post-<?php the_ID(); ?>" class="post unit-spotlight-main">
    <div class="row featuredpostwrap">
        <?php if($image) { ?>
        <div class="col-md-5 <?php if ($imgpostition=='right') { echo "col-md-push-7"; } ?>">
            <?php if ($imageorvideo=='video' && $videoid) {
               ?>
<div class="unit-video-post-image">
                <div class="videogallery">
                    <a href="https://www.youtube.com/watch?v=<? echo esc_html($videoid); ?>" data-poster="<?php echo $image['url']; ?>" data-sub-html="#video-<?php echo $vidid; ?>"><img class="img-responsive" alt="<?php echo $post->post_title; ?>" src="<?php echo $image['url']; ?>">
                        <div class="videogallery-play"><img alt="Play" src="/wp-content/uploads/2018/08/play.svg"></div>
                    </a>
                </div>
               
            </div>

               <?php

            }  else { ?>
            <div class="post-image">
                <?php if ($image['url']) { ?><a href="<?php echo get_permalink($postid); ?>"><img class="img-responsive" alt="<?php echo $post->post_title; ?>" src="<?php echo $image['url']; ?>"></a>
                    <?php } ?>
            </div>
        <?php } ?>

        </div>
    <?php } ?>
        <div class="<?php if(!$image) { echo "col-sm-10 col-sm-offset-1 "; } else { ?>col-md-7 <?php if ($imgpostition=='right') { echo "col-md-pull-5"; } }?>" >
            <header class="entry-header">
                <?php

        jobsohio_com_getfeaturedpost_header_link(get_post_type($postid));
        // echo $kicker;

       

        ?>
        <h2 class="heading unit-post-heading"><a href="<?php echo get_permalink($postid); ?>"><?php echo $post->post_title; ?></a></h2>
                    <div class="deck">
                        <?php echo get_field('deck', $postid); ?>
                    </div>
                    <div class="author">
                        <?php echo get_field('author', $postid); ?>
                    </div>
                   
                    <div class="abstract">
                        <p>
                            <?php echo get_field('abstract', $postid); ?>
                        </p>
                    </div>
                    <div class="buttons">
                        <div class="button button--cta"><a href="<?php echo get_permalink($postid); ?>">Read More</a></div>
                        <div class="button button--cta"><a href="/ohio-success/">Learn More About Other Successes</a></div>
                    </div>

            </header>
            <!-- .entry-header -->

        </div>
    </div>
</div>
<!-- #post-<?php the_ID(); ?> -->








