<?php

/*
Widget Name: JobsOhio Intro  Layout
Description: Title, Kicker, Lead, Body, Text Alignment
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_intro_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-intro') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_intro_banner_img_src', 10, 2);

class jobsohio_intro_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-intro',
			__('JobsOhio Intro Layout Block', 'jobsohio_intro-text-domain'),
			array(
				'description' => __('JobsOhio Intro Layout', 'jobsohio-intro-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
			  	array(
					'title' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Headline', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					'kicker' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Kicker', 'jobsohio-com' ),
			         'description' => 'All caps text above header',
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'body' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Body', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
		
			   'align' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Text Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'right' => esc_html__( 'Right', 'jobsohio-com' ),
					            'center' => esc_html__( 'Center', 'jobsohio-com' )
					     
					        )
					    )
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-intro-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-intro-style';
	}
}

siteorigin_widget_register('jobsohio-intro', __FILE__, 'jobsohio_intro_Widget');