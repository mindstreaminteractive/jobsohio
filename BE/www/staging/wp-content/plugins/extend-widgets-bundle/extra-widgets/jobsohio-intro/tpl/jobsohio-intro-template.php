<?php

$title = $instance['title'];
$kicker = $instance['kicker'];
$headline = $instance['headline'];
$body = $instance['body'];
$align = $instance['align'];

?>

<div class="unit-intro-main" style="text-align: <?php echo $align; ?>">
    <div class="row">
        <div class="col-sm-12">
            <?php if ($kicker) { ?><div class="kicker"><?php echo $kicker; ?></div><? } ?>
            <div class="unit-header">
                <h2 class="heading"><?php echo $title; ?></h2></div>
            <div class="unit-content">
                <div class="body">
                   <? echo $body; ?>
                </div>
            </div>
        </div>
    </div>
</div>