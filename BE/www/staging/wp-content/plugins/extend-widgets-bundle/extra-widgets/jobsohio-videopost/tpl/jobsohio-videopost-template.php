<?php

$videottitle = $instance['videottitle'];
// $videolead = $instance['videolead'];
$videocaption = $instance['videocaption'];

$youtube = $instance['youtube'];
$image = $instance['image'];
$videobody = $instance['videobody'];
$imageposition = $instance['imageposition'];
$videoid=rand();
$large_image = wp_get_attachment_image_src($image, 'full');?>


<div class="unit-video-post-main">
    <div class="row">
        <div class="col-md-5 <?php if ($imageposition=='icon-right') { echo "col-md-push-7"; } ?>">
            <div class="unit-video-post-image">
                <div class="videogallery">
                    <a href="<? echo esc_html($youtube); ?>" data-poster="<?php echo wp_kses_post($large_image[0]);?>" data-sub-html="#video-<?php echo $videoid; ?>"><img class="img-responsive" alt="Ohio's Biohealth Innovators" src="<?php echo wp_kses_post($large_image[0]);?>">
                        <div class="videogallery-play"><img alt="Play" src="/wp-content/uploads/2018/08/play.svg"></div>
                    </a>
                </div>
                <div id="video-<?php echo $videoid; ?>" class="unit-video-post-caption"><? echo esc_html($videocaption); ?></div>
            </div>
        </div>
        <div class="col-md-7 <?php if ($imageposition=='icon-right') { echo "col-md-pull-5"; } ?>">
            <div class="unit-header">
                <h2 class="heading"><? echo esc_html($videottitle); ?></h2></div>
            <div class="unit-content">
                <div class="body">
                   <? echo $videobody; ?>
                </div>
            </div>
    </div>
</div>
</div>