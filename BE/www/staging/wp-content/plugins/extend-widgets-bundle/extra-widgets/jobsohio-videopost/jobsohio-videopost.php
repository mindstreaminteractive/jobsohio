<?php

/*
Widget Name: JobsOhio Video Post Block
Description: dev-jobsohio.local video options
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_videopost_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-videopost') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_videopost_banner_img_src', 10, 2);

class jobsohio_videopost_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-videopost',
			__('JobsOhio Video Post Block', 'jobsohio_videopost-text-domain'),
			array(
				'description' => __('JobsOhio Video Post Block', 'jobsohio-videopost-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					'videottitle' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Title', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'videobody' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Body', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'videocaption' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Video Caption', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
			    'youtube' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Youtube Video URL', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
			   'image' => array (
					'type' => 'media',
					'label' => esc_html__( 'Video Poster Image', 'jobsohio-com' ),
					'library' => 'image',
					'fallback' => false,
				),
			   'imageposition' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Image Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'icon-left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'icon-right' => esc_html__( 'Right', 'jobsohio-com' )
					     
					        )
					    )
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-videopost-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-videopost-style';
	}
}

siteorigin_widget_register('jobsohio-videopost', __FILE__, 'jobsohio_videopost_Widget');