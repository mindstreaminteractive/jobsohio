<?php

/*
Widget Name: JobsOhio General Page  Layout
Description: Title, Kicker, Lead, Body, Top Border
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_general_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-general') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_general_banner_img_src', 10, 2);

class jobsohio_general_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-general',
			__('JobsOhio General Page Layout Block', 'jobsohio_general-text-domain'),
			array(
				'description' => __('JobsOhio General Layout', 'jobsohio-general-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
			  	array(
					'title' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Headline', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					'lead' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Lead', 'jobsohio-com' ),
			         'description' => 'The first paragraph of the body set in larger type.',
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'body' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Body', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
		
			   'align' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Text Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'right' => esc_html__( 'Right', 'jobsohio-com' ),
					            'center' => esc_html__( 'Center', 'jobsohio-com' )
					     
					        )
					    )
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-general-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-general-style';
	}
}

siteorigin_widget_register('jobsohio-general', __FILE__, 'jobsohio_general_Widget');