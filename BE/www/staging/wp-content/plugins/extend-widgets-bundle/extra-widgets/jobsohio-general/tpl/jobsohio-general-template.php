<?php

$title = $instance['title'];
$lead = $instance['lead'];
$headline = $instance['headline'];
$body = $instance['body'];
$align = $instance['align'];

?>

<div class="unit-general-main" style="text-align: <?php echo $align; ?>">
    <div class="row">
        <div class="col-sm-12">
            <?php if ($kicker) { ?><div class="kicker"><?php echo $kicker; ?></div><? } ?>
            <div class="unit-header">
                <h2 class="heading"><?php echo $title; ?></h2></div>
            <div class="unit-content">
                <div class="lead"><p><? echo $lead; ?></p></div>
                <div class="body">
                   <? echo $body; ?>
                </div>
            </div>
        </div>
    </div>
</div>