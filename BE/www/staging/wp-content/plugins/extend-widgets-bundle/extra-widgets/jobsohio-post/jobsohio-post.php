<?php

/*
Widget Name: JobsOhio Post Block
Description: dev-jobsohio.local Post Block
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_post_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-post') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}

function jobsohio_get_posts($type) { 
         $post_args = array(
    'posts_per_page'   => 500,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => $type,
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'       => '',
    'author_name'       => '',
    'post_status'      => 'publish',
    'suppress_filters' => false 
);
$posts_array = get_posts( $post_args );
foreach ($posts_array as $post) {
	$posts[$post->ID] = $post->post_title." ".$post->post_date;
}
	return $posts;
    }

  

add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_post_banner_img_src', 10, 2);

class jobsohio_post_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-post',
			__('JobsOhio Blog/Media/News Post Block', 'jobsohio_post-text-domain'),
			array(
				'description' => __('JobsOhio Blog/Media/News Post Block', 'jobsohio-post-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					 'imageposition' => array (
			   		'type' => 'select',
					        'label' => esc_html__( 'Image Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'right' => esc_html__( 'Right', 'jobsohio-com' )
					     
					        )
					    	), 'newwindow' => array ( 
					    		'type' => 'checkbox',
					        'label' => esc_html__( 'Open Links in a New Window', 'jobsohio-com' ),
                            'default' => false
					       
					    	), 'posttype' => array (
			   				'type' => 'select',
					        'label' => esc_html__( 'Post Type', 'jobsohio-com' ),
					         'state_emitter' => array(
        					   'callback' => 'select',
        						'args' => array( 'posttype' )
   							 	),
					        'default' => '',
					        'options' => array(
					            'blog' => esc_html__( 'Blog Post', 'jobsohio-com' ),
					            'news' => esc_html__( 'News Post', 'jobsohio-com' ),
					            'media' => esc_html__( 'Media PR', 'jobsohio-com' ),
					     
					        )
					    ),
					'post' => array(
					'type' => 'select',
					'state_handler' => array(
        'posttype[blog]' => array('show'),
        'posttype[news]' => array('hide'),
        'posttype[media]' => array('hide')
    ),	
						 'prompt' => esc_html__( '-', 'jobsohio-com' ),
					'label' => 'Choose a Blog Post',
					'description' => '',
					'options' => jobsohio_get_posts('post')		
				), 'newspost' => array(
					'type' => 'select',
					'state_handler' => array(
        'posttype[blog]' => array('hide'),
        'posttype[news]' => array('show'),
        'posttype[media]' => array('hide')
    ),	
					 'prompt' => esc_html__( '-', 'jobsohio-com' ),
					 'label' => 'Choose a News Post',
					'description' => '',
			
					'options' => jobsohio_get_posts('news-posts')		
				) , 'mediapost' => array(
					'type' => 'select', 
					'state_handler' => array(
        'posttype[blog]' => array('hide'),
        'posttype[news]' => array('hide'),
        'posttype[media]' => array('show')
    ),	
					 'prompt' => esc_html__( '-', 'jobsohio-com' ),
					'label' => 'Choose a Media Relations Post',
					'description' => '',
			
					'options' => jobsohio_get_posts('media-pr')		
				), 'imageorvideo' => array (
			   		'type' => 'select',
					        'label' => esc_html__( 'Display Image or Video', 'jobsohio-com' ),
					        'description' => 'If a video is not assigned to the post the post banner image will be displayed.',
					        'default' => 'image',
					        'options' => array(
					            'image' => esc_html__( 'Image', 'jobsohio-com' ),
					            'video' => esc_html__( 'Video', 'jobsohio-com' )
					     
					        )
					    	)
				
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-post-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-post-style';
	}
}

siteorigin_widget_register('jobsohio-post', __FILE__, 'jobsohio_post_Widget');