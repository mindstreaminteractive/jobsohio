<?php

if ($instance['posttype']=="blog") {
$postid = $instance['post'];
}
if ($instance['posttype']=="news") {
$postid = $instance['newspost'];
}
if ($instance['posttype']=="media") {
$postid = $instance['mediapost'];
}
$imgpostition= $instance['imageposition'];

$post = get_post( $postid  ); 


$image = get_field('banner_image',$postid);
$newwindow = $instance['newwindow'];

$sdate = $post->post_date;
$publishdate = new DateTime($sdate);

$videoid = $post->youtube_video;
$imageorvideo= $instance['imageorvideo'];

?>

<div id="post-<?php the_ID(); ?>" class="post unit-post-main">
    <div class="row featuredpostwrap">
         <?php if ($imageorvideo=='video' && $videoid) {
               ?>
                 <div class="col-md-5 <?php if ($imgpostition=='right') { echo "col-md-push-7"; } ?>">
<div class="unit-video-post-image">
                <div class="videogallery">
                    <a href="https://www.youtube.com/watch?v=<? echo esc_html($videoid); ?>" data-poster="<?php echo $image['url']; ?>" data-sub-html="#video-<?php echo $vidid; ?>"><img class="img-responsive" alt="Ohio's Biohealth Innovators" src="<?php echo $image['url']; ?>">
                        <div class="videogallery-play"><img alt="Play" src="/wp-content/uploads/2018/08/play.svg"></div>
                    </a>
                </div>
               </div>
            </div>

               <?php

            }  else if($image) { ?>
        <div class="col-md-5 <?php if ($imgpostition=='right') { echo "col-md-push-7"; } ?>">
            <div class="post-image">
                <?php if ($image['url']) { ?><a href="<?php echo get_permalink($postid); ?>" <?php if ($newwindow) { echo "target='_blank'"; } ?>><img class="img-responsive" alt="<?php echo $post->post_title; ?>" src="<?php echo $image['url']; ?>"></a>
                    <?php } ?>
            </div>

        </div>
    <?php } ?>
        <div class="<?php if(!$image) { echo "col-sm-10 col-sm-offset-1 "; } else { ?>col-md-7 <?php if ($imgpostition=='right') { echo "col-md-pull-5"; } }?>" >
            <header class="entry-header">
                <?php
        if ($newwindow) { jobsohio_com_getfeaturedpost_header_linknewwindow(get_post_type($postid)); } else {
        jobsohio_com_getfeaturedpost_header_link(get_post_type($postid));
        }
        // echo $kicker;

       

        ?>
        <h2 class="heading unit-post-heading"><a href="<?php echo get_permalink($postid); ?>"><?php echo $post->post_title; ?></a></h2>
                    <div class="deck">
                        <?php echo get_field('deck', $postid); ?>
                    </div>
                    <div class="author">
                        <?php echo get_field('author', $postid); ?>
                    </div>
                    <div class="date">
                        <?php  echo $publishdate->format('F j, Y'); ?>
                    </div>
                    <div class="abstract">
                        <p>
                            <?php echo get_field('abstract', $postid); ?>
                        </p>
                    </div>
                    <div class="buttons">
                        <div class="button button--cta"><a href="<?php echo get_permalink($postid); ?>"  <?php if ($newwindow) { echo "target='_blank'"; } ?>>Read More</a></div>
                    </div>

            </header>
            <!-- .entry-header -->

        </div>
    </div>
</div>
<!-- #post-<?php the_ID(); ?> -->








