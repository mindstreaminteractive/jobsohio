<?php

$title = $instance['title'];
$kicker = $instance['kicker'];
$headline = $instance['headline'];
$body = $instance['body'];
$links = $instance['links'];

?>

<div class="unit-links-main">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <?php if ($kicker) { ?><div class="kicker"><?php echo $kicker; ?></div><? } ?>
            <div class="unit-header">
                <h2 class="heading"><?php echo $title; ?></h2></div>
            <div class="unit-content">
                <div class="body">
                   <? echo $body; ?>
                </div>
                                    <ul>

  <?php foreach ($instance['links_repeater'] as $link) {
$linktext = $link['linktext']; 
$linkurl = $link['linkurl'];
?>
<li><a href="<? echo esc_html($linkurl ); ?>"><?php echo esc_html($linktext); ?></a></li>


<?php }  ?></ul>

            </div>
        </div>
    </div>
</div>