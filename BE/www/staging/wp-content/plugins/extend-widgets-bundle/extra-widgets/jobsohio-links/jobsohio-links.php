<?php

/*
Widget Name: JobsOhio Links  Layout
Description: Title, Kicker, Lead, List of Links
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_links_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-links') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_links_banner_img_src', 10, 2);

class jobsohio_links_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-links',
			__('JobsOhio links Layout Block', 'jobsohio_links-text-domain'),
			array(
				'description' => __('JobsOhio links Layout', 'jobsohio-links-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
			  	array(
					'title' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Headline', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					'kicker' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Kicker', 'jobsohio-com' ),
			         'description' => 'All caps text above header',
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'body' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Body', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
		
			  'links_repeater' => array(
			        'type' => 'repeater',
			        'label' => esc_html__( 'Add Link' , 'jobsohio-com' ),
			        'item_name'  => esc_html__( 'Add Link', 'jobsohio-com' ),
			          'label' => esc_html__( 'Links', 'jobsohio-com' ),
		        	'fields' => array(
						'linktext' => array(
							'type' => 'text',
							'label' => esc_html__( 'Link Text', 'jobsohio-com' ),
							'description' => esc_html__( 'Add Link Text', 'obsohio-com' ),
							'default' => ''
						), 'linkurl' => array(
							'type' => 'link',
							'label' => esc_html__( 'Link URL', 'jobsohio-com' ),
							'description' => esc_html__( 'Add Link', 'obsohio-com' ),
							'default' => ''
						),
						
					),
		        ),
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-links-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-links-style';
	}
}

siteorigin_widget_register('jobsohio-links', __FILE__, 'jobsohio_links_Widget');