<?php

$buttontext = $instance['button'];
$button_color = $instance['style_section']['button_color'];
$btn_style = $instance['style_section']['btn_style'];
$icon = $instance['icon_section']['icon'];
$icon_position = $instance['icon_section']['icon_position'];
$url = $instance['url'];
$btn_type = $instance['style_section']['btn_type'];
$btn_size = $instance['style_section']['btn_size'];
$btn_alignment = $instance['style_section']['btn_alignment'];
$new_tab = $instance['new_tab'];
$icon_color	 = $instance['icon_section']['icon_color'];
// get link;
if (preg_match('#^post#', $url) === 1) {
	preg_match_all('!\d+!', $url, $post_id);
	$post_url = get_permalink($post_id[0][0]);
	$url = $post_url;
}
if ( $btn_alignment != 'center' && $btn_alignment != '' ) {
	$button_classes .= ' ' . $btn_alignment;
}
?>
<?php
if ($btn_type=="btn-reddownload") {
?>
<div class="buttons"><div class="button button--download"><a href="<?php echo esc_url($url);?>" target="_blank"><?php echo esc_html($buttontext); ?></a></div></div>
<?php
} else { 

 if ($btn_alignment == 'center') : ?>
	<div class="wrapper block" style="text-align:center">
<?php endif;?>
<div class="buttons <?php if ($btn_alignment == 'block') { echo "fullwidth"; }  ?>"><div class="button button--cta"><a href="<?php echo esc_url($url);?>" class="<?php echo esc_attr($button_classes);?>" <?php echo esc_attr($target);?>>
	<?php if ($icon_position == 'icon-left' || $icon_position == 'inset-left' ) {
				echo siteorigin_widget_get_icon( $icon, $icon_style);
			}; ?>

	<?php echo esc_html($buttontext); 
	if ($icon_position == 'icon-right' || $icon_position == 'inset-right' ) {
				echo siteorigin_widget_get_icon( $icon, $icon_style);
			};?>
	</a></div></div>
<?php if ($btn_alignment == 'center') : ?>
</div>
<?php endif;

}
?>