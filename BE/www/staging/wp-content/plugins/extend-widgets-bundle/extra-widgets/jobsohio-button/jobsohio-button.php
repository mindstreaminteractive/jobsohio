<?php

/*
Widget Name: JobsOhio Button
Description: dev-jobsohio.local button options
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_button_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-button') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_button_banner_img_src', 10, 2);

class Jobsohio_Button_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-button',
			__('JobsOhio Button Widget', 'jobsohio_button-text-domain'),
			array(
				'description' => __('JobsOhio Button', 'jobsohio-button-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
			    'button' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Button Text', 'jobsohio-com' ),
					'default' => esc_html__('Button', 'jobsohio-com'),
			    ),
			    'url' => array(
			        'type' => 'link',
			        'label' => esc_html__('Destination URL', 'jobsohio-com'),
			        'default' => esc_html__('#', 'jobsohio-com'),
			    ),
			    'new_tab' => array(
			        'type' => 'checkbox',
			        'label' => esc_html__( 'Open in a new window', 'jobsohio-com' ),
			        'default' => false,
			    ),
				'icon_section' => array(
			        'type' => 'section',
			        'label' => esc_html__( 'Icon' , 'jobsohio-com' ),
			        'hide' => true,
			        'fields' => array(	
					    'icon' => array(
					        'type' => 'icon',
					        'label' => esc_html__('Select Icon', 'jobsohio-com'),
					    ),
						'icon_color' => array(
					        'type' => 'color',
					        'label' => esc_html__( 'Icon Color', 'jobsohio-com' ),
					        'default' => '',
					    ),					    		    
					    'icon_position' => array(
					        'type' => 'select',
					        'label' => esc_html__( 'Icon Position', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'icon-left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'icon-right' => esc_html__( 'Right', 'jobsohio-com' ),
					            'inset-left' => esc_html__( 'Inset Left', 'jobsohio-com' ),
					            'inset-right' => esc_html__( 'Inset Right', 'jobsohio-com' ),
					        ),
					    ),

					),
				),
				'style_section' => array(
			        'type' => 'section',
			        'label' => esc_html__( 'Style & Layout' , 'jobsohio-com' ),
			        'hide' => true,
			        'fields' => array(	
					    'btn_type' => array(
					        'type' => 'select',
					        'label' => esc_html__( 'Button Style', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'btn-flat' => esc_html__( 'Normal', 'jobsohio-com' ),
					            'btn-reddownload' => esc_html__( 'Red Download', 'jobsohio-com' )
					        ),
					    ),
					    'button_color' => array(
					        'type' => 'select',
					        'label' => esc_html__( 'Button Color', 'jobsohio-com' ),
					        'default' => 'btn',
					        'options' => array(
					            'btn' => esc_html__( 'Yellow', 'jobsohio-com' ),
					            'btn btn-c1' => esc_html__( 'Red', 'jobsohio-com' ),
					            'btn btn-black' => esc_html__( 'Black', 'jobsohio-com' ),
					            'btn btn-white' => esc_html__( 'White', 'jobsohio-com' ),
					        ),
					    ),
					    'btn_alignment' => array(
					        'type' => 'select',
					        'label' => esc_html__( 'Align', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'float-left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'float-right' => esc_html__( 'Right', 'jobsohio-com' ),
					            'center' => esc_html__( 'Center', 'jobsohio-com' ),
					            'block' => esc_html__( 'Fullwidth', 'jobsohio-com' ),
					            '' => esc_html__( 'None', 'jobsohio-com' ),
					        ),
					    ),
					),
				),
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-button-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-button-style';
	}
}

siteorigin_widget_register('jobsohio-button', __FILE__, 'Jobsohio_Button_Widget');