<?php

/*
Widget Name: JobsOhio Image Post Block
Description: dev-jobsohio.local 2 column image with title/text.
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_imagepost_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-imagepost') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_imagepost_banner_img_src', 10, 2);

class jobsohio_imagepost_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-imagepost',
			__('JobsOhio Image Post Block', 'jobsohio_imagepost-text-domain'),
			array(
				'description' => __('JobsOhio Image Block Two Column, title, text, image', 'jobsohio-imagepost-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					'imagetitle' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Title', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'imagebody' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Body', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
			
			   'image' => array (
					'type' => 'media',
					'label' => esc_html__( 'Image', 'jobsohio-com' ),
					'library' => 'image',
					'fallback' => false,
				),
			   'imageposition' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Image Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'icon-left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'icon-right' => esc_html__( 'Right', 'jobsohio-com' )
					     
					        )
					    )
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-imagepost-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-imagepost-style';
	}
}

siteorigin_widget_register('jobsohio-imagepost', __FILE__, 'jobsohio_imagepost_Widget');