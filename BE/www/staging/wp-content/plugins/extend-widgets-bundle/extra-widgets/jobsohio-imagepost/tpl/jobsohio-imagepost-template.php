<?php

$imagetitle = $instance['imagetitle'];


$image = $instance['image'];
$imagebody = $instance['imagebody'];
$imageposition = $instance['imageposition'];
$videoid=rand();
$large_image = wp_get_attachment_image_src($image, 'full');?>


<div class="unit-image-post-main">
    <div class="row">
        <div class="col-md-5 <?php if ($imageposition=='icon-right') { echo "col-md-push-7"; } ?>">
            <div class="unit-image-post-image">
    <div class="imagegallery">
        <a href="<?php echo wp_kses_post($large_image[0]);?>" data-src="<?php echo wp_kses_post($large_image[0]);?>" data-sub-html="#image-3188"><img class="img-responsive" alt="this is the description" src="<?php echo wp_kses_post($large_image[0]);?>"></a>
    </div>
    <div class="button button--cta lightgallery"><a href="<?php echo wp_kses_post($large_image[0]);?>">Click to Enlarge</a></div>
</div>


        </div>
        <div class="col-md-7 <?php if ($imageposition=='icon-right') { echo "col-md-pull-5"; } ?>">
            <div class="unit-header">
                <h2 class="heading"><? echo esc_html($imagetitle); ?></h2></div>
            <div class="unit-content">
                <div class="body">
                   <? echo $imagebody; ?>
                </div>
            </div>
    </div>
</div>
</div>