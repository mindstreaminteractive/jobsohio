<?php

/*
Widget Name: JobsOhio Hero Block
Description: dev-jobsohio.local Hero Banner
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_hero_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-hero') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_hero_banner_img_src', 10, 2);

class jobsohio_hero_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-hero',
			__('JobsOhio Hero Block', 'jobsohio_hero-text-domain'),
			array(
				'description' => __('Hero image/headline', 'jobsohio-hero-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					'title' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Title', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
			
			   'image' => array (
					'type' => 'media',
					'label' => esc_html__( 'Image', 'jobsohio-com' ),
					'library' => 'image',
					'fallback' => false,
				),
			   'herosize' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Hero Size', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'full' => esc_html__( '720px Height, 100% Width', 'jobsohio-com' ),
					             'med' => esc_html__( '520px Height, 100% Width', 'jobsohio-com' ),
					              'med2' => esc_html__( '418px Height, 100% Width', 'jobsohio-com' ),
					            'small' => esc_html__( '320px Height, 100% Width', 'jobsohio-com' ),
					            'flex' => esc_html__( 'Image Height (1280px Max Width)', 'jobsohio-com' ),
					            'flexfull' => esc_html__( '100% Width/No Cropping)', 'jobsohio-com' )
					     
					        )
					    )
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-hero-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-hero-style';
	}
}

siteorigin_widget_register('jobsohio-hero', __FILE__, 'jobsohio_hero_Widget');