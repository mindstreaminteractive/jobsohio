<?php

$imagetitle = $instance['imagetitle'];


$image = $instance['image'];
$title = $instance['title'];
$size = $instance['herosize'];
$large_image = wp_get_attachment_image_src($image, 'full');
$height="banner".$size;


if ($size=="flex" || $size=="flexfull") {
?>
<div class="banner banner-hero <?php if ($size=="flexfull") { echo "flexfullhero"; } ?>"><div class="banner-hero-content"><div class="banner-hero-table"><h1 class="banner-hero-title"><?PHP echo $title; ?></h1></div></div><img class="banner-hero-image img-responsive" alt="<?PHP echo $title; ?>" src="<?php echo wp_kses_post($large_image[0]);?>"></div>

<?php

} else {
?>


<div class="banner banner-hero fullwidthbanner <?php echo $height; ?>" style="background-image:url(<?php echo wp_kses_post($large_image[0]);?>);">
    <div class="banner-hero-content">
        <div class="banner-hero-table">
            <h1 class="banner-hero-title"><?PHP echo $title; ?></h1>
        </div>
    </div>
 </div>

 <?php } ?>