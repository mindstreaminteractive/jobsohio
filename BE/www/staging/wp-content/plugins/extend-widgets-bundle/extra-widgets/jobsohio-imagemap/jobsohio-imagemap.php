<?php

/*
Widget Name: JobsOhio Utility Map   Block
Description: Site Selection Utility Image Map
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_imagemap_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-imagemap') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}



  

add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_imagemap_banner_img_src', 10, 2);

class jobsohio_imagemap_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-imagemap',
			__('JobsOhio Utility Map Block', 'jobsohio_imagemap-text-domain'),
			array(
				'description' => __('JobsOhio Utility Map Block', 'jobsohio-imagemap-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					 'basemap' => array(
			        'type' => 'media',
			        'label' => esc_html__( 'Map Image (1280px x 720px)', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					  
					'layer_repeater' => array(
			        'type' => 'repeater',
			        'label' => esc_html__( 'Add Utility Layer' , 'jobsohio-com' ),
			        'item_name'  => esc_html__( 'Add Layer', 'jobsohio-com' ),
			          'label' => esc_html__( 'Utility Layer', 'jobsohio-com' ),
		        	'fields' => array(
						'layerlabel' => array(
							'type' => 'text',
							'label' => esc_html__( 'Utility Name', 'jobsohio-com' ),
							'description' => esc_html__( 'Menu Name', 'jobsohio-com' ),
							'default' => ''
						), 'layerimg' => array(
							'type' => 'media',
							 'label' => esc_html__( 'Layer Image (1280px x 720px PNG with transparent background)', 'jobsohio-com' ),
							'default' => ''
						),
						
					),
		        ),
					
				
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-imagemap-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-imagemap-style';
	}
}

siteorigin_widget_register('jobsohio-imagemap', __FILE__, 'jobsohio_imagemap_Widget');