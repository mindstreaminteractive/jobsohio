<?php

$videottitle = $instance['videottitle'];
$lead = $instance['lead'];
// $videolead = $instance['videolead'];
$videocaption = $instance['videocaption'];

$btntext = $instance['btntext'];
$btnurl = $instance['btnurl'];

$btntext2 = $instance['btntext2'];
$btnurl2 = $instance['btnurl2'];

$youtube = $instance['youtube'];
$image = $instance['image'];
$videobody = $instance['videobody'];
$imageposition = $instance['imageposition'];
$videoid=rand();
$large_image = wp_get_attachment_image_src($image, 'full');?>


<div class="unit-video-post-main">
    <div class="unit-header">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="heading heading--grandstand"><? echo esc_html($videottitle); ?></h2></div>
                </div>
            </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="unit-video-post-image textwrapvideo <?php if ($imageposition=='icon-right') { echo "alignright"; } else { echo "alignleft"; } ?>">
                <div class="videogallery">
                    <?php if ($youtube) { ?><a href="<? echo esc_html($youtube); ?>" data-poster="<?php echo wp_kses_post($large_image[0]);?>" data-sub-html="#video-<?php echo $videoid; ?>"><?php } ?><img class="img-responsive" alt="Ohio's Biohealth Innovators" src="<?php echo wp_kses_post($large_image[0]);?>">
                        <?php if ($youtube) { ?><div class="videogallery-play"><img alt="Play" src="/wp-content/uploads/2018/08/play.svg"></div>
                    </a> <?php } ?>
                </div>
                <div id="video-<?php echo $videoid; ?>" class="unit-video-post-caption"><? echo esc_html($videocaption); ?></div>
            </div>
    
           <div class="lead">
                            <p><? echo $lead; ?></p>
                        </div>
            <div class="unit-content">
                <div class="body">
                   <? echo $videobody; ?>
                </div>
            </div>
       
                    <?php foreach ($instance['layer_repeater'] as $layer) { 
$btnlabel = $layer['btnlabel'];
$btnurl = $layer['btnurl']; ?>
                    <div class="buttons">
                        <div class="button button--download"><a href="<?php echo esc_url($btnurl); ?>" target="_blank"><?php echo esc_html($btnlabel); ?></a></div>
                    </div><?php } ?>
    </div>
</div>
</div>