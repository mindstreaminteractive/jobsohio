<?php

/*
Widget Name: JobsOhio Video Grandstand Block
Description: dev-jobsohio.local video grandstand block with text wrapping
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_videoposttextwrap_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-videoposttextwrap') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_videoposttextwrap_banner_img_src', 10, 2);

class jobsohio_videoposttextwrap_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-videoposttextwrap',
			__('JobsOhio Video Grandstand Block with Text Wrapping', 'jobsohio_videoposttextwrap-text-domain'),
			array(
				'description' => __('JobsOhio Video Grandstand Block With Text Wrapping', 'jobsohio-videoposttextwrap-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					'videottitle' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Title', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ), 'lead' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Lead', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
					 'description' => esc_html__( 'The first paragraph of the body set in larger type.

', 'jobsohio-com' )
			    ),
					 'videobody' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Body', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'videocaption' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Image/Video Caption', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ), 
			    'youtube' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Youtube Video URL', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
					'description' => esc_html__( 'Leave Empty for Image Only', 'jobsohio-com' )
			    ),
			   'image' => array (
					'type' => 'media',
					'label' => esc_html__( 'Image/Video Poster', 'jobsohio-com' ),
					'library' => 'image',
					'fallback' => false,
				),
			   'imageposition' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Image Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'icon-left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'icon-right' => esc_html__( 'Right', 'jobsohio-com' )
					     
					        )
					    ),	'layer_repeater' => array(
			        'type' => 'repeater',
			        'label' => esc_html__( 'Add Buttons' , 'jobsohio-com' ),
			        'item_name'  => esc_html__( 'Add Button', 'jobsohio-com' ),
			          'label' => esc_html__( 'Button', 'jobsohio-com' ),
		        	'fields' => array(
						'btnlabel' => array(
							'type' => 'text',
							'label' => esc_html__( 'Button Label', 'jobsohio-com' ),
							'default' => ''
						), 'btnurl' => array(
							'type' => 'link',
							 'label' => esc_html__( 'Button URL', 'jobsohio-com' ),
							'default' => ''
						),
						
					),
		        ),
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-videoposttextwrap-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-videoposttextwrap-style';
	}
}

siteorigin_widget_register('jobsohio-videoposttextwrap', __FILE__, 'jobsohio_videoposttextwrap_Widget');