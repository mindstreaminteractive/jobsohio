<?php

$quotettitle = $instance['quotettitle'];
$quotetext = $instance['quotetext'];

?>


<aside class="subunit quote"><div class="quote-description"><p><? echo esc_html($quotetext); ?></p></div><div class="quote-cite">—<? echo esc_html($quotettitle); ?></div></aside>