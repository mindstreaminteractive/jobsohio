<?php

/*
Widget Name: JobsOhio Quote Box
Description: dev-jobsohio.local quote options
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_quote_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-quote') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_quote_banner_img_src', 10, 2);

class jobsohio_quote_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-quote',
			__('JobsOhio Quote Box Widget', 'jobsohio_quote-text-domain'),
			array(
				'description' => __('JobsOhio Quote Box', 'jobsohio-quote-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					'quotettitle' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Quoted Person/Title', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ), 'quotetext' => array(
			        'type' => 'textarea',
			        'label' => esc_html__( 'Quote Text', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    )
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-quote-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-quote-style';
	}
}

siteorigin_widget_register('jobsohio-quote', __FILE__, 'jobsohio_quote_Widget');