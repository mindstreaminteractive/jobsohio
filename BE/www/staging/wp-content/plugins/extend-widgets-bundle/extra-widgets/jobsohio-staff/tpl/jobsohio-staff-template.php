<?php
global $post;
$postid = $instance['postid'];
$title =$instance['title'];
$intro =$instance['intro'];

//$post = get_post( $postid  ); 

?>
<div class="unit-employees-main">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="unit-header">
                <h2 class="heading"><?php echo $title; ?></h2>
            </div>
      
        <?php if ($intro) { ?><div class="unit-content"><div class="body"><?php echo $intro; ?></div></div><?php } ?>
    </div>
      </div>


    <div class="row">
<div class="col-sm-10 col-sm-offset-1">
    <?php foreach ($instance['staff_repeater'] as $staff) {
$page_id = $staff['postid'];
$image = get_field('photo', $page_id);
$biotxt= get_field('bio', $page_id);
$industries=get_field('industries', $page_id);
$subsector=get_field('subsector', $page_id);

?>



<div class="col-sm-6 col-md-4 col-centered">
    <div class="employee">
        <div class="employee-portrait"><img class="img-responsive" alt="<?php echo get_the_title($page_id); ?>" src="<?php echo $image['url']; ?>"></div>
        <div class="employee-name"><?php echo get_the_title($page_id); ?></div>
        <div class="employee-position"><?php echo get_field('position', $page_id); ?></div>
      
              <?php if( $industries ) { ?>
                  <div class="employee-industries <?php echo $post->ID; ?>" style=""> 
            <?php $i=0; $indylinks=""; foreach ($industries as $industry) { 
                     $txtstr="";
                     $i++;
                    $txtstr=str_replace('-',' ',$industry);
                    if($post->ID!=1044 && $industry != "energy-chemicals") {
                    $indylinks.=' <a href="/industries/'.$industry.'/" class="role'.$i.'">'.ucwords($txtstr).'</a>,'; 
                    }

                      
                } 
                   echo rtrim($indylinks,',');
                   if ($subsector) { echo ", ".$subsector; } ?>  
               </div>
                 
        <?php  } ?>
        <div class="email employee-email"><a href="mailto:<?php echo get_field('email', $page_id); ?>?bcc=jobsohio@dev-jobsohio.local">Email Me</a></div>
        <div class="employee-tel tel"><a href="tel:<?php echo get_field('phone', $page_id); ?>"><?php echo get_field('phone', $page_id); ?></a></div>
       
    </div>
</div>



 <?php   }  ?>
</div>
</div>
</div>
















