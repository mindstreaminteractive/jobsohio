<?php

/*
Widget Name: JobsOhio Staff Member  Block
Description: Select staff member blocks
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_staff_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-staff') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}

function jobsohio_get_staffs() { 
         $post_args = array(
    'posts_per_page'   => 500,
    'orderby'          => 'title',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'team',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'       => '',
    'author_name'       => '',
    'post_status'      => 'publish',
    'suppress_filters' => false 
);
$posts_array = get_posts( $post_args );
foreach ($posts_array as $post) {
	$posts[$post->ID] = $post->post_title." (".get_field('position', $post->ID).")";
}
	return $posts;
    }

  

add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_staff_banner_img_src', 10, 2);

class jobsohio_staff_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-staff',
			__('JobsOhio Employee Block', 'jobsohio_staff-text-domain'),
			array(
				'description' => __('JobsOhio Employee Block', 'jobsohio-staff-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					 'title' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Headline Text', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					  'intro' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Intro Text', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					'staff_repeater' => array(
			        'type' => 'repeater',
			        'label' => esc_html__( 'Add Staff Member' , 'jobsohio-com' ),
			        'item_name'  => esc_html__( 'Click to select a staff member', 'jobsohio-com' ),
			        'item_label' => array(
			            'selector'     => "[id*='page_id'] option:selected",
			            'update_event' => 'change',
			            'value_method' => 'text'
			        ),
		        	'fields' => array(
						'postid' => array(
							'type' => 'select',
							'label' => esc_html__( 'Choose Staff by Name.', 'jobsohio-com' ),
							'description' => esc_html__( 'Add excerpt and featured image on page edit screen, to display it inside widget.', 'obsohio-com' ),
							'default' => '',
							'options' => jobsohio_get_staffs(),
						)
						
					),
		        ),
					
				
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-staff-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-staff-style';
	}
}

siteorigin_widget_register('jobsohio-staff', __FILE__, 'jobsohio_staff_Widget');