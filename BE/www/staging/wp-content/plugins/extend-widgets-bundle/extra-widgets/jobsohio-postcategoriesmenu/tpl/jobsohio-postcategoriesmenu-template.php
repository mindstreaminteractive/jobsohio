<?php

$type = $instance['posttype'];

// arguments for function wp_list_categories
$args = array( 
taxonomy => $type,
title_li => ''
);
 
// We wrap it in unordered list 
echo '<div class="posts-meta"><h3 class="widget-title">Categories</h3><ul>'; 
echo wp_list_categories($args);
echo '</ul></div>';
?>
