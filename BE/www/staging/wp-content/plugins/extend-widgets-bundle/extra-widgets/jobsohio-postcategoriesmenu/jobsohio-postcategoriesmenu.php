<?php

/*
Widget Name: JobsOhio Post Category Menu
Description: Post Sidebar Cargory Menu
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_postcategoriesmenu_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-postcategoriesmenu') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_postcategoriesmenu_banner_img_src', 10, 2);

class jobsohio_postcategoriesmenu_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-postcategoriesmenu',
			__('JobsOhio postcategoriesmenu Layout Block', 'jobsohio_postcategoriesmenu-text-domain'),
			array(
				'description' => __('JobsOhio Post Sidebar Cargory Menu', 'jobsohio-postcategoriesmenu-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			), array(
			   'posttype' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Select Post Type', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'category' => esc_html__( 'Blog', 'jobsohio-com' ),
					            'news-category' => esc_html__( 'News', 'jobsohio-com' ),
					            'pr-category' => esc_html__( 'Media', 'jobsohio-com' )
					     
					        )
					    
			   )
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-postcategoriesmenu-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-postcategoriesmenu-style';
	}
}

siteorigin_widget_register('jobsohio-postcategoriesmenu', __FILE__, 'jobsohio_postcategoriesmenu_Widget');