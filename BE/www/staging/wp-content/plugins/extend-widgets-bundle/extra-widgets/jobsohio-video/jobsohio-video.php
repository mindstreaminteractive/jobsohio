<?php

/*
Widget Name: JobsOhio Video Lightbox
Description: dev-jobsohio.local video options
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_video_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-video') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_video_banner_img_src', 10, 2);

class jobsohio_video_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-video',
			__('JobsOhio Video Lightbox Widget', 'jobsohio_video-text-domain'),
			array(
				'description' => __('JobsOhio Video Lightbox', 'jobsohio-video-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					'videottitle' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Video Title', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ), 'videocaption' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Video Caption', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
			    'youtube' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Youtube Video URL', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
			   'image' => array (
					'type' => 'media',
					'label' => esc_html__( 'Video Poster Image', 'jobsohio-com' ),
					'library' => 'image',
					'fallback' => false,
				)
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-video-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-video-style';
	}
}

siteorigin_widget_register('jobsohio-video', __FILE__, 'jobsohio_video_Widget');