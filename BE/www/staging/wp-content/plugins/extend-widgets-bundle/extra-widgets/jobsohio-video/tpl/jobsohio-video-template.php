<?php

$videottitle = $instance['videottitle'];
$videocaption = $instance['videocaption'];
$youtube = $instance['youtube'];
$image = $instance['image'];

$large_image = wp_get_attachment_image_src($image, 'full');?>
<div class="videogallery"><a href="<?php  echo esc_url($youtube); ?>" data-poster="<?php echo wp_kses_post($large_image[0]);?>" data-sub-html="#video-<?php echo rand(); ?>"><img class="img-responsive" alt="<? echo esc_html($videottitle); ?>" src="<?php echo wp_kses_post($large_image[0]);?>"><div class="videogallery-play"><img alt="Play" src="/wp-content/uploads/2018/08/play.svg"></div></a></div><?php if ($videocaption) { ?><div class="unit-video-post-caption"><? echo esc_html($videocaption); ?></div><?php } ?>