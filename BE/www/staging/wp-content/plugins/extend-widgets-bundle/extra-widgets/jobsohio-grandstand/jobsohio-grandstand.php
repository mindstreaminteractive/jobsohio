<?php

/*
Widget Name: JobsOhio Grandstand  Block
Description: dev-jobsohio.local 2 column with large vertical image with title/text/button.
Author: Whiteboard Marketing
Author URI: https://whiteboard-mktg.com
*/

function jobsohio_grandstand_banner_img_src( $banner_url, $widget_meta ) {
	if( $widget_meta['ID'] == 'jobsohio-grandstand') {
		$banner_url = plugin_dir_url(__FILE__) . 'images/jobsohio.svg';
	}
	return $banner_url;
}
add_filter( 'siteorigin_widgets_widget_banner', 'jobsohio_grandstand_banner_img_src', 10, 2);

class jobsohio_grandstand_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'jobsohio-grandstand',
			__('JobsOhio Grandstand Block', 'jobsohio_grandstand-text-domain'),
			array(
				'description' => __('JobsOhio Grandstand Block:2 column with large vertical image with title/text/button', 'jobsohio-grandstand-text-domain'),
				'panels_groups' => array('jobsohio-com')
			),
			array(

			),
				array(
					'title' => array(
			        'type' => 'text',
			        'label' => esc_html__( 'Title', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ), 'lead' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Lead', 'jobsohio-com' ),
			        'description' => esc_html__( 'The first paragraph of the body set in larger type.

', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
					 'body' => array(
			        'type' => 'tinymce',
			        'label' => esc_html__( 'Body', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
			    ),
			
			   'image' => array (
					'type' => 'media',
					'label' => esc_html__( 'Image', 'jobsohio-com' ),
					'library' => 'image',
					'fallback' => false,
				),
			    'btntext' => array (
					'type' => 'text',
					'label' => esc_html__( 'Button Text', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
				),
				'btnurl' => array (
					'type' => 'link',
					'label' => esc_html__( 'Button URL', 'jobsohio-com' ),
					'default' => esc_html__('', 'jobsohio-com'),
				),
			   'imageposition' => array (
			   'type' => 'select',
					        'label' => esc_html__( 'Image Alignment', 'jobsohio-com' ),
					        'default' => '',
					        'options' => array(
					            'icon-left' => esc_html__( 'Left', 'jobsohio-com' ),
					            'icon-right' => esc_html__( 'Right', 'jobsohio-com' )
					     
					        )
					    )
			   
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'jobsohio-grandstand-template';
	}

	function get_style_name($instance) {
		return 'jobsohio-grandstand-style';
	}
}

siteorigin_widget_register('jobsohio-grandstand', __FILE__, 'jobsohio_grandstand_Widget');