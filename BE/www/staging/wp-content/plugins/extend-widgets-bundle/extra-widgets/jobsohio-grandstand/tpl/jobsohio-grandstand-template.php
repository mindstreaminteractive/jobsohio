<?php

$title = $instance['title'];
$lead = $instance['lead'];

$image = $instance['image'];
$body = $instance['body'];
$btntext = $instance['btntext'];
$btnurl = $instance['btnurl'];
$imageposition = $instance['imageposition'];

$large_image = wp_get_attachment_image_src($image, 'full');?>


<div class="constrain">
    <div class="container-fluid">
        <div class="unit-grandstand-main">
            <div class="unit-header">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="heading heading--grandstand"><? echo esc_html($title); ?></h2></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 nopad-xs <?php if ($imageposition=='icon-right') { echo "col-md-push-7"; } ?>"><img class="grandstand-image img-responsive" alt="<? echo esc_html($title); ?>" src="<?php echo wp_kses_post($large_image[0]);?>"></div>
                <div class="col-sm-7 <?php if ($imageposition=='icon-right') { echo "col-md-pull-5"; } ?>">
                    <div class="unit-content">
                        <div class="lead">
                            <p><? echo $lead; ?></p>
                        </div>
                        <div class="body">
                           <?php echo $body; ?>
                        </div>
                    </div>
                    <?php if ($btntext && $btnurl) { ?>
                    <div class="buttons">
                        <div class="button button--download"><a href="<?php echo esc_url($btnurl); ?>" target="_blank"><?php echo esc_html($btntext); ?></a></div>
                    </div><?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
